import { Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerApiUrlConfig } from '../../app.constants';
export declare class AuthServerProvider {
    private http;
    private injector;
    private serverUrl;
    private $localStorage;
    private $sessionStorage;
    constructor(http: HttpClient, injector: Injector, serverUrl: ServerApiUrlConfig);
    getToken(): any;
    login(credentials: any): Observable<any>;
    loginWithToken(jwt: any, rememberMe: any): Promise<any>;
    storeAuthenticationToken(jwt: any, rememberMe: any): void;
    logout(): Observable<any>;
}
