export declare class Account {
    activated: boolean;
    authorities: string[];
    email: string;
    firstName: string;
    langKey: string;
    lastName: string;
    login: string;
    imageUrl: string;
    constructor(activated: boolean, authorities: string[], email: string, firstName: string, langKey: string, lastName: string, login: string, imageUrl: string);
}
