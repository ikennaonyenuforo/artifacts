import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IUser } from './user.model';
import { ServerApiUrlConfig } from '../../app.constants';
export declare class UserService {
    private http;
    private serverUrl;
    resourceUrl: any;
    constructor(http: HttpClient, serverUrl: ServerApiUrlConfig);
    create(user: IUser): Observable<HttpResponse<IUser>>;
    update(user: IUser): Observable<HttpResponse<IUser>>;
    find(login: string): Observable<HttpResponse<IUser>>;
    query(req?: any): Observable<HttpResponse<IUser[]>>;
    delete(login: string): Observable<HttpResponse<any>>;
    authorities(): Observable<string[]>;
}
