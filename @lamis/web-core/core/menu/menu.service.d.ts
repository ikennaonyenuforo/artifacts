import { HttpClient } from '@angular/common/http';
import { ServerApiUrlConfig } from '../../app.constants';
import { IMenuItem } from '../../shared/model';
export declare class MenuService {
    private http;
    private serverUrl;
    constructor(http: HttpClient, serverUrl: ServerApiUrlConfig);
    getMenus(): import("rxjs").Observable<IMenuItem[]>;
}
