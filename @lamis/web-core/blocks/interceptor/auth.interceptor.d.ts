import { Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { ServerApiUrlConfig } from '../../app.constants';
export declare class AuthInterceptor implements HttpInterceptor {
    private injector;
    private serverUrl;
    private $localStorage;
    private $sessionStorage;
    constructor(injector: Injector, serverUrl: ServerApiUrlConfig);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
