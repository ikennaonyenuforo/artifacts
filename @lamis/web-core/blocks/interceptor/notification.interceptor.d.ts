import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotificationService } from '@alfresco/adf-core';
export declare class NotificationInterceptor implements HttpInterceptor {
    private notificationService;
    constructor(notificationService: NotificationService);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
