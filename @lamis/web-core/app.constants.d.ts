import { InjectionToken } from '@angular/core';
export declare const SERVER_API_URL_CONFIG: InjectionToken<ServerApiUrlConfig>;
export interface ServerApiUrlConfig {
    SERVER_API_URL: string;
}
