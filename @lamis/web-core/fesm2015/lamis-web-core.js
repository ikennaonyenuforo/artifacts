import { InjectionToken, Inject, ɵɵdefineInjectable, ɵɵinject, Injectable, Injector, INJECTOR, NgModule, LOCALE_ID, Component, TemplateRef, ViewContainerRef, Input, Directive, Pipe, ViewEncapsulation, ViewChild, EventEmitter, ElementRef, ChangeDetectorRef, Output, forwardRef, ComponentFactoryResolver, Renderer2, KeyValueDiffers, HostListener, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { __decorate, __param, __metadata } from 'tslib';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpParams, HttpClientModule } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject, Observable, of } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { JhiEventManager, JhiAlertService, NgJhipsterModule, JhiPaginationUtil } from 'ng-jhipster';
import { NotificationService, CardViewBoolItemModel, CardViewIntItemModel, CardViewFloatItemModel, CardViewDateItemModel, CardViewDatetimeItemModel, CardViewTextItemModel, CoreModule as CoreModule$1, CardViewBaseItemModel, CardViewUpdateService, CardItemTypeService, ObjectDataTableAdapter } from '@alfresco/adf-core';
import { Router, RouterModule } from '@angular/router';
import { DatePipe, CurrencyPipe, CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import get from 'lodash-es/get';
import { MatCardModule, MatTableDataSource, MatButtonModule, MatSelectModule, MatIconModule, MatTooltipModule, MatDatepickerModule, MatProgressBar, MatButton } from '@angular/material';
import { trigger, state, style, transition, animate, query, stagger, keyframes } from '@angular/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, FormControl, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import * as _moment from 'moment';
import { FormioComponent, MatFormioModule } from 'angular-material-formio';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';

const SERVER_API_URL_CONFIG = new InjectionToken('SERVER_API_URL_CONFIG');

let AccountService = class AccountService {
    constructor(http, apiUrlConfig) {
        this.http = http;
        this.apiUrlConfig = apiUrlConfig;
        this.authenticated = false;
        this.authenticationState = new Subject();
    }
    fetch() {
        return this.http.get(this.apiUrlConfig.SERVER_API_URL + 'api/account', { observe: 'response' });
    }
    save(account) {
        return this.http.post(this.apiUrlConfig.SERVER_API_URL + 'api/account', account, { observe: 'response' });
    }
    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    }
    hasAnyAuthority(authorities) {
        if (authorities === undefined || authorities.length === 0) {
            return true;
        }
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }
        for (let i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.includes(authorities[i])) {
                return true;
            }
        }
        return false;
    }
    hasAuthority(authority) {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }
        return this.identity().then(id => {
            return Promise.resolve(id.authorities && id.authorities.includes(authority));
        }, () => {
            return Promise.resolve(false);
        });
    }
    identity(force) {
        if (force) {
            this.userIdentity = undefined;
        }
        // check and see if we have retrieved the userIdentity data from the server.
        // if we have, reuse it by immediately resolving
        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }
        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.fetch()
            .toPromise()
            .then(response => {
            const account = response.body;
            if (account) {
                this.userIdentity = account;
                this.authenticated = true;
            }
            else {
                this.userIdentity = null;
                this.authenticated = false;
            }
            this.authenticationState.next(this.userIdentity);
            return this.userIdentity;
        })
            .catch(err => {
            this.userIdentity = null;
            this.authenticated = false;
            this.authenticationState.next(this.userIdentity);
            return null;
        });
    }
    isAuthenticated() {
        return this.authenticated;
    }
    isIdentityResolved() {
        return this.userIdentity !== undefined;
    }
    getAuthenticationState() {
        return this.authenticationState.asObservable();
    }
    getImageUrl() {
        return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
    }
};
AccountService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AccountService.ngInjectableDef = ɵɵdefineInjectable({ factory: function AccountService_Factory() { return new AccountService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: AccountService, providedIn: "root" });
AccountService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], AccountService);

let AuthServerProvider = class AuthServerProvider {
    constructor(http, injector, serverUrl) {
        this.http = http;
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    getToken() {
        return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
    }
    login(credentials) {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        let _this = this;
        return this.http.post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data, { observe: 'response' }).pipe(map(authenticateSuccess.bind(this)));
        function authenticateSuccess(resp) {
            const bearerToken = resp.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length);
                _this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    }
    loginWithToken(jwt, rememberMe) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe);
            return Promise.resolve(jwt);
        }
        else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    }
    storeAuthenticationToken(jwt, rememberMe) {
        if (rememberMe) {
            this.$localStorage.set('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.set('authenticationToken', jwt);
        }
    }
    logout() {
        return new Observable(observer => {
            this.$localStorage.remove('authenticationToken');
            this.$sessionStorage.remove('authenticationToken');
            observer.complete();
        });
    }
};
AuthServerProvider.ctorParameters = () => [
    { type: HttpClient },
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthServerProvider.ngInjectableDef = ɵɵdefineInjectable({ factory: function AuthServerProvider_Factory() { return new AuthServerProvider(ɵɵinject(HttpClient), ɵɵinject(INJECTOR), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: AuthServerProvider, providedIn: "root" });
AuthServerProvider = __decorate([
    Injectable({ providedIn: 'root' }),
    __param(2, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient,
        Injector, Object])
], AuthServerProvider);

let LoginService = class LoginService {
    constructor(accountService, authServerProvider) {
        this.accountService = accountService;
        this.authServerProvider = authServerProvider;
    }
    login(credentials, callback) {
        const cb = callback || function () {
        };
        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe(data => {
                this.accountService.identity(true).then(account => {
                    resolve(data);
                });
                return cb();
            }, err => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }
    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }
    logout() {
        this.authServerProvider.logout().subscribe();
        this.accountService.authenticate(null);
    }
};
LoginService.ctorParameters = () => [
    { type: AccountService },
    { type: AuthServerProvider }
];
LoginService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(ɵɵinject(AccountService), ɵɵinject(AuthServerProvider)); }, token: LoginService, providedIn: "root" });
LoginService = __decorate([
    Injectable({ providedIn: 'root' }),
    __metadata("design:paramtypes", [AccountService, AuthServerProvider])
], LoginService);

let AuthExpiredInterceptor = class AuthExpiredInterceptor {
    constructor(loginService) {
        this.loginService = loginService;
    }
    intercept(request, next) {
        return next.handle(request).pipe(tap((event) => { }, (err) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this.loginService.logout();
                }
            }
        }));
    }
};
AuthExpiredInterceptor.ctorParameters = () => [
    { type: LoginService }
];
AuthExpiredInterceptor = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [LoginService])
], AuthExpiredInterceptor);

let AuthInterceptor = class AuthInterceptor {
    constructor(injector, serverUrl) {
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    intercept(request, next) {
        if (!request || !request.url || (/^http/.test(request.url) && !(this.serverUrl.SERVER_API_URL && request.url.startsWith(this.serverUrl.SERVER_API_URL)))) {
            return next.handle(request);
        }
        const token = this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        return next.handle(request);
    }
};
AuthInterceptor.ctorParameters = () => [
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthInterceptor = __decorate([
    Injectable(),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [Injector, Object])
], AuthInterceptor);

let ErrorHandlerInterceptor = class ErrorHandlerInterceptor {
    constructor(injector) {
        this.injector = injector;
        this.eventManager = injector.get(JhiEventManager);
    }
    intercept(request, next) {
        return next.handle(request).pipe(tap(() => { }, (err) => {
            if (err instanceof HttpErrorResponse) {
                if (!(err.status === 401 && (err.message === '' || (err.url && err.url.includes('/api/account'))))) {
                    this.eventManager.broadcast({ name: 'app.httpError', content: err });
                }
            }
        }));
    }
};
ErrorHandlerInterceptor.ctorParameters = () => [
    { type: Injector }
];
ErrorHandlerInterceptor = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Injector])
], ErrorHandlerInterceptor);

let NotificationInterceptor = class NotificationInterceptor {
    constructor(notificationService) {
        this.notificationService = notificationService;
    }
    intercept(request, next) {
        return next.handle(request).pipe(tap((event) => {
            if (event instanceof HttpResponse) {
                const arr = event.headers.keys();
                let alert = null;
                arr.forEach(entry => {
                    if (entry.toLowerCase().endsWith('app-alert')) {
                        alert = event.headers.get(entry);
                    }
                });
                if (alert) {
                    if (typeof alert === 'string') {
                        this.notificationService.openSnackMessage(alert);
                    }
                }
            }
        }, (err) => { }));
    }
};
NotificationInterceptor.ctorParameters = () => [
    { type: NotificationService }
];
NotificationInterceptor = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [NotificationService])
], NotificationInterceptor);

let StateStorageService = class StateStorageService {
    constructor(injector) {
        this.injector = injector;
        this.$sessionStorage = injector.get(SessionStorageService);
    }
    getPreviousState() {
        return this.$sessionStorage.get('previousState');
    }
    resetPreviousState() {
        this.$sessionStorage.remove('previousState');
    }
    storePreviousState(previousStateName, previousStateParams) {
        const previousState = { name: previousStateName, params: previousStateParams };
        this.$sessionStorage.set('previousState', previousState);
    }
    getDestinationState() {
        return this.$sessionStorage.get('destinationState');
    }
    storeUrl(url) {
        this.$sessionStorage.set('previousUrl', url);
    }
    getUrl() {
        return this.$sessionStorage.get('previousUrl');
    }
    storeDestinationState(destinationState, destinationStateParams, fromState) {
        const destinationInfo = {
            destination: {
                name: destinationState.name,
                data: destinationState.data
            },
            params: destinationStateParams,
            from: {
                name: fromState.name
            }
        };
        this.$sessionStorage.set('destinationState', destinationInfo);
    }
};
StateStorageService.ctorParameters = () => [
    { type: Injector }
];
StateStorageService.ngInjectableDef = ɵɵdefineInjectable({ factory: function StateStorageService_Factory() { return new StateStorageService(ɵɵinject(INJECTOR)); }, token: StateStorageService, providedIn: "root" });
StateStorageService = __decorate([
    Injectable({ providedIn: 'root' }),
    __metadata("design:paramtypes", [Injector])
], StateStorageService);

let UserRouteAccessService = class UserRouteAccessService {
    constructor(router, accountService, stateStorageService) {
        this.router = router;
        this.accountService = accountService;
        this.stateStorageService = stateStorageService;
    }
    canActivate(route, state) {
        const authorities = route.data['authorities'];
        // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.
        return this.checkLogin(authorities, state.url);
    }
    checkLogin(authorities, url) {
        return this.accountService.identity().then(account => {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account) {
                const hasAnyAuthority = this.accountService.hasAnyAuthority(authorities);
                if (hasAnyAuthority) {
                    return true;
                }
                if (account.login !== 'anonymoususer') {
                    this.router.navigate(['sessions/accessdenied']);
                }
                return false;
            }
            this.stateStorageService.storeUrl(url);
            this.router.navigate(['sessions/login']);
            return false;
        });
    }
};
UserRouteAccessService.ctorParameters = () => [
    { type: Router },
    { type: AccountService },
    { type: StateStorageService }
];
UserRouteAccessService.ngInjectableDef = ɵɵdefineInjectable({ factory: function UserRouteAccessService_Factory() { return new UserRouteAccessService(ɵɵinject(Router), ɵɵinject(AccountService), ɵɵinject(StateStorageService)); }, token: UserRouteAccessService, providedIn: "root" });
UserRouteAccessService = __decorate([
    Injectable({ providedIn: 'root' }),
    __metadata("design:paramtypes", [Router,
        AccountService,
        StateStorageService])
], UserRouteAccessService);

class Account {
    constructor(activated, authorities, email, firstName, langKey, lastName, login, imageUrl) {
        this.activated = activated;
        this.authorities = authorities;
        this.email = email;
        this.firstName = firstName;
        this.langKey = langKey;
        this.lastName = lastName;
        this.login = login;
        this.imageUrl = imageUrl;
    }
}

class User {
    constructor(id, login, firstName, lastName, email, activated, langKey, authorities, createdBy, createdDate, lastModifiedBy, lastModifiedDate, password) {
        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;
        this.langKey = langKey;
        this.authorities = authorities;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.password = password;
        this.id = id ? id : null;
        this.login = login ? login : null;
        this.firstName = firstName ? firstName : null;
        this.lastName = lastName ? lastName : null;
        this.email = email ? email : null;
        this.activated = activated ? activated : false;
        this.langKey = langKey ? langKey : null;
        this.authorities = authorities ? authorities : null;
        this.createdBy = createdBy ? createdBy : null;
        this.createdDate = createdDate ? createdDate : null;
        this.lastModifiedBy = lastModifiedBy ? lastModifiedBy : null;
        this.lastModifiedDate = lastModifiedDate ? lastModifiedDate : null;
        this.password = password ? password : null;
    }
}

const createRequestOption = (req) => {
    let options = new HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach(val => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};

let UserService = class UserService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = serverUrl.SERVER_API_URL + 'api/users';
    }
    create(user) {
        return this.http.post(this.resourceUrl, user, { observe: 'response' });
    }
    update(user) {
        return this.http.put(this.resourceUrl, user, { observe: 'response' });
    }
    find(login) {
        return this.http.get(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }
    query(req) {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(login) {
        return this.http.delete(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }
    authorities() {
        return this.http.get(this.serverUrl + 'api/users/authorities');
    }
};
UserService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
UserService.ngInjectableDef = ɵɵdefineInjectable({ factory: function UserService_Factory() { return new UserService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: UserService, providedIn: "root" });
UserService = __decorate([
    Injectable({ providedIn: 'root' }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], UserService);

let MenuService = class MenuService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
    }
    getMenus() {
        return this.http.get(this.serverUrl.SERVER_API_URL + 'api/modules/menus', {});
    }
};
MenuService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
MenuService.ngInjectableDef = ɵɵdefineInjectable({ factory: function MenuService_Factory() { return new MenuService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: MenuService, providedIn: "root" });
MenuService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], MenuService);

let CoreModule = class CoreModule {
    constructor() {
        //registerLocaleData(locale);
    }
};
CoreModule = __decorate([
    NgModule({
        imports: [HttpClientModule],
        exports: [],
        declarations: [],
        providers: [
            Title,
            {
                provide: LOCALE_ID,
                useValue: 'en'
            },
            DatePipe
        ]
    }),
    __metadata("design:paramtypes", [])
], CoreModule);

let LgaService = class LgaService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/lgas';
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    findByState(id) {
        return this.http.get(`${this.resourceUrl}/state/${id}`, { observe: 'response' });
    }
};
LgaService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
LgaService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LgaService_Factory() { return new LgaService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: LgaService, providedIn: "root" });
LgaService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], LgaService);

let FacilityService = class FacilityService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/facilities';
    }
    create(facility) {
        return this.http
            .post(this.resourceUrl, facility, { observe: 'response' });
    }
    update(facility) {
        return this.http
            .put(this.resourceUrl, facility, { observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    findByLga(id) {
        return this.http.get(`${this.resourceUrl}/lga/${id}`, { observe: 'response' });
    }
};
FacilityService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
FacilityService.ngInjectableDef = ɵɵdefineInjectable({ factory: function FacilityService_Factory() { return new FacilityService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: FacilityService, providedIn: "root" });
FacilityService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], FacilityService);

let LoginAuthenticationService = class LoginAuthenticationService {
    constructor(router, loginService, $localStorage, $sessionStorage, eventManager, stateStorageService, injector) {
        this.router = router;
        this.loginService = loginService;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
        this.eventManager = eventManager;
        this.stateStorageService = stateStorageService;
        this.injector = injector;
        /*this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
        this.stateStorageService = this.injector.get(StateStorageService);
        this.eventManager = this.injector.get(JhiEventManager);*/
    }
    setRedirect(value) {
    }
    isEcmLoggedIn() {
        return false;
    }
    isBpmLoggedIn() {
        return false;
    }
    isOauth() {
        return false;
    }
    getRedirect() {
        return null;
    }
    login(username, password, rememberMe = false) {
        this.loginService
            .login({
            username: username,
            password: password,
            rememberMe: rememberMe
        })
            .then((data) => {
            if (this.router.url === '/account/register' || (/^\/account\/activate\//.test(this.router.url)) ||
                (/^\/account\/reset\//.test(this.router.url))) {
                this.router.navigate(['']);
            }
            this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is successful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
                this.stateStorageService.storeUrl('');
                this.router.navigate([redirect]);
            }
            else {
                this.router.navigate(['/dashboard']);
            }
        });
        return of(true);
    }
};
LoginAuthenticationService.ctorParameters = () => [
    { type: Router },
    { type: LoginService },
    { type: LocalStorageService },
    { type: SessionStorageService },
    { type: JhiEventManager },
    { type: StateStorageService },
    { type: Injector }
];
LoginAuthenticationService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [Router,
        LoginService,
        LocalStorageService,
        SessionStorageService,
        JhiEventManager,
        StateStorageService,
        Injector])
], LoginAuthenticationService);

let StateService = class StateService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/states';
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    getStates() {
        return this.http.get(`${this.resourceUrl}`, { observe: 'response' });
    }
};
StateService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
StateService.ngInjectableDef = ɵɵdefineInjectable({ factory: function StateService_Factory() { return new StateService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: StateService, providedIn: "root" });
StateService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], StateService);

function _window() {
    // return the global native browser window object
    return window;
}
let WindowRef = class WindowRef {
    get nativeWindow() {
        return _window();
    }
};
WindowRef.ngInjectableDef = ɵɵdefineInjectable({ factory: function WindowRef_Factory() { return new WindowRef(); }, token: WindowRef, providedIn: "root" });
WindowRef = __decorate([
    Injectable({ providedIn: 'root' })
], WindowRef);

const PROBLEM_BASE_URL = 'https://www.jhipster.tech/problem';
const EMAIL_ALREADY_USED_TYPE = PROBLEM_BASE_URL + '/email-already-used';
const LOGIN_ALREADY_USED_TYPE = PROBLEM_BASE_URL + '/login-already-used';
const EMAIL_NOT_FOUND_TYPE = PROBLEM_BASE_URL + '/email-not-found';

const ITEMS_PER_PAGE = 20;

const DATE_FORMAT = 'YYYY-MM-DD';
const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';

let AlertComponent = class AlertComponent {
    constructor(notification, alertService) {
        this.notification = notification;
        this.alertService = alertService;
    }
    ngOnInit() {
        this.alerts = this.alertService.get();
        this.alerts.forEach(alert => this.notification.openSnackMessage(alert.msg, 5000));
    }
    ngOnDestroy() {
        this.alerts = [];
    }
};
AlertComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiAlertService }
];
AlertComponent = __decorate([
    Component({
        selector: 'alert',
        template: ``
    }),
    __metadata("design:paramtypes", [NotificationService, JhiAlertService])
], AlertComponent);

let AlertErrorComponent = class AlertErrorComponent {
    constructor(notification, eventManager) {
        this.notification = notification;
        this.eventManager = eventManager;
        /* tslint:enable */
        this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', response => {
            let i;
            const httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    const arr = httpErrorResponse.headers.keys();
                    let errorHeader = null;
                    let entityKey = null;
                    arr.forEach(entry => {
                        if (entry.endsWith('app-error')) {
                            errorHeader = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.endsWith('app-params')) {
                            entityKey = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader) {
                        this.addErrorAlert(errorHeader, errorHeader, { entityName: entityKey });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        const fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            const fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            const fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                            this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                case 404:
                    this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    ngOnDestroy() {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
        }
    }
    addErrorAlert(message, key, data) {
        this.notification.openSnackMessage(message, 5000);
    }
};
AlertErrorComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiEventManager }
];
AlertErrorComponent = __decorate([
    Component({
        selector: 'alert-error',
        template: ``
    }),
    __metadata("design:paramtypes", [NotificationService, JhiEventManager])
], AlertErrorComponent);

/**
 * @whatItDoes Conditionally includes an HTML element if current user has any
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *jhiHasAnyAuthority="'ROLE_ADMIN'">...</some-element>
 *
 *     <some-element *jhiHasAnyAuthority="['ROLE_ADMIN', 'ROLE_USER']">...</some-element>
 * ```
 */
let HasAnyAuthorityDirective = class HasAnyAuthorityDirective {
    constructor(accountService, templateRef, viewContainerRef) {
        this.accountService = accountService;
        this.templateRef = templateRef;
        this.viewContainerRef = viewContainerRef;
    }
    set jhiHasAnyAuthority(value) {
        this.authorities = typeof value === 'string' ? [value] : value;
        this.updateView();
        // Get notified each time authentication state changes.
        this.accountService.getAuthenticationState().subscribe(identity => this.updateView());
    }
    updateView() {
        const hasAnyAuthority = this.accountService.hasAnyAuthority(this.authorities);
        this.viewContainerRef.clear();
        if (hasAnyAuthority) {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
};
HasAnyAuthorityDirective.ctorParameters = () => [
    { type: AccountService },
    { type: TemplateRef },
    { type: ViewContainerRef }
];
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], HasAnyAuthorityDirective.prototype, "jhiHasAnyAuthority", null);
HasAnyAuthorityDirective = __decorate([
    Directive({
        selector: '[jhiHasAnyAuthority]'
    }),
    __metadata("design:paramtypes", [AccountService,
        TemplateRef,
        ViewContainerRef])
], HasAnyAuthorityDirective);

let RelativeTimePipe = class RelativeTimePipe {
    transform(value) {
        if (!(value instanceof Date))
            value = new Date(value);
        let seconds = Math.floor(((new Date()).getTime() - value.getTime()) / 1000);
        let interval = Math.floor(seconds / 31536000);
        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    }
};
RelativeTimePipe = __decorate([
    Pipe({ name: 'relativeTime' })
], RelativeTimePipe);

let ExcerptPipe = class ExcerptPipe {
    transform(text, limit = 5) {
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    }
};
ExcerptPipe = __decorate([
    Pipe({ name: 'excerpt' })
], ExcerptPipe);

let KeysPipe = class KeysPipe {
    transform(value, args) {
        let keys = [];
        for (let enumMember in value) {
            if (!isNaN(parseInt(enumMember, 10))) {
                keys.push({ key: enumMember, value: value[enumMember] });
            }
        }
        console.log('Keys', keys);
        return keys;
    }
};
KeysPipe = __decorate([
    Pipe({ name: 'keys1' })
], KeysPipe);

let MapValuesPipe = class MapValuesPipe {
    transform(value, args) {
        let returnArray = [];
        value.forEach((entryVal, entryKey) => {
            returnArray.push({
                key: entryKey,
                val: entryVal
            });
        });
        return returnArray;
    }
};
MapValuesPipe = __decorate([
    Pipe({ name: 'mapValues' })
], MapValuesPipe);

let NairaPipe = class NairaPipe {
    constructor() {
        this.pipe = new CurrencyPipe('en');
    }
    transform(value, ...args) {
        return this.pipe.transform(value, '₦');
    }
};
NairaPipe = __decorate([
    Pipe({
        name: 'naira'
    })
], NairaPipe);

let CommonPipesModule = class CommonPipesModule {
};
CommonPipesModule = __decorate([
    NgModule({
        declarations: [
            RelativeTimePipe,
            ExcerptPipe,
            KeysPipe,
            MapValuesPipe,
            NairaPipe
        ],
        exports: [
            RelativeTimePipe,
            ExcerptPipe,
            KeysPipe,
            MapValuesPipe,
            NairaPipe
        ]
    })
], CommonPipesModule);

let LayoutTemplateService = class LayoutTemplateService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/templates';
    }
    getTemplate(templateId) {
        return this.http.get(`${this.resourceUrl}/${templateId}`, { observe: 'body' });
    }
};
LayoutTemplateService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
LayoutTemplateService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LayoutTemplateService_Factory() { return new LayoutTemplateService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: LayoutTemplateService, providedIn: "root" });
LayoutTemplateService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG)),
    __metadata("design:paramtypes", [HttpClient, Object])
], LayoutTemplateService);

var FieldType;
(function (FieldType) {
    FieldType["date"] = "date";
    FieldType["datetime"] = "datetime";
    FieldType["text"] = "text";
    FieldType["boolean"] = "boolean";
    FieldType["int"] = "int";
    FieldType["float"] = "float";
})(FieldType || (FieldType = {}));
let DetailsComponent = class DetailsComponent {
    constructor(templateService) {
        this.templateService = templateService;
    }
    ngOnInit() {
        this.templateService.getTemplate(this.template).subscribe((json) => {
            this.details = json.template;
        });
    }
    propertiesForDetail(detail) {
        let properties = [];
        for (const field of detail.fields) {
            const dataType = field.type;
            let item;
            switch (dataType) {
                case FieldType.boolean:
                    item = new CardViewBoolItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label
                    });
                    break;
                case FieldType.int:
                    item = new CardViewIntItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.float:
                    item = new CardViewFloatItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.date:
                    item = new CardViewDateItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy'
                    });
                    break;
                case FieldType.datetime:
                    item = new CardViewDatetimeItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy HH:mm'
                    });
                    break;
                default:
                    item = new CardViewTextItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
            }
            properties.push(item);
        }
        return properties;
    }
    getValueForKey(key) {
        return get(this.model, key);
    }
};
DetailsComponent.ctorParameters = () => [
    { type: LayoutTemplateService }
];
__decorate([
    Input(),
    __metadata("design:type", String)
], DetailsComponent.prototype, "template", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], DetailsComponent.prototype, "model", void 0);
DetailsComponent = __decorate([
    Component({
        selector: 'details-component',
        template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
        encapsulation: ViewEncapsulation.None,
        styles: [""]
    }),
    __metadata("design:paramtypes", [LayoutTemplateService])
], DetailsComponent);

let SharedCommonModule = class SharedCommonModule {
};
SharedCommonModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            CoreModule$1,
            NgJhipsterModule,
            CommonPipesModule,
            MatCardModule
        ],
        declarations: [
            AlertComponent,
            AlertErrorComponent,
            DetailsComponent
        ],
        exports: [
            AlertComponent,
            AlertErrorComponent,
            CommonPipesModule,
            DetailsComponent
        ]
    })
], SharedCommonModule);

const speedDialFabAnimations = [
    trigger('fabToggler', [
        state('inactive', style({
            transform: 'rotate(0deg)'
        })),
        state('active', style({
            transform: 'rotate(225deg)'
        })),
        transition('* <=> *', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    trigger('speedDialStagger', [
        transition('* => *', [
            query(':enter', style({ opacity: 0 }), { optional: true }),
            query(':enter', stagger('40ms', [
                animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)', keyframes([
                    style({ opacity: 0, transform: 'translateY(10px)' }),
                    style({ opacity: 1, transform: 'translateY(0)' }),
                ]))
            ]), { optional: true }),
            query(':leave', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)', keyframes([
                style({ opacity: 1 }),
                style({ opacity: 0 }),
            ])), { optional: true })
        ])
    ])
];

let SpeedDialFabComponent = class SpeedDialFabComponent {
    constructor() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
        this.buttonState = [];
    }
    ngOnInit() {
        this.links.forEach(link => this.buttonState.push(link));
    }
    showItems() {
        this.fabTogglerState = 'active';
        this.buttons = this.buttonState;
    }
    hideItems() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
    }
    onToggleFab() {
        this.buttons.length ? this.hideItems() : this.showItems();
    }
};
__decorate([
    Input(),
    __metadata("design:type", Array)
], SpeedDialFabComponent.prototype, "links", void 0);
SpeedDialFabComponent = __decorate([
    Component({
        selector: 'speed-dial',
        template: "<div class=\"fab-container\">\r\n    <button mat-fab class=\"fab-toggler\"\r\n            (click)=\"onToggleFab()\">\r\n        <mat-icon [@fabToggler]=\"{value: fabTogglerState}\">add</mat-icon>\r\n    </button>\r\n    <div [@speedDialStagger]=\"buttons.length\">\r\n        <ng-container *ngFor=\"let btn of buttons\">\r\n            <button mat-mini-fab\r\n                    *jhiHasAnyAuthority=\"btn.roles\"\r\n                    matTooltip=\"{{btn.tooltip}}\"\r\n                    [routerLink]=\"['.', btn.state, 'new']\"\r\n                    class=\"fab-secondary\"\r\n                    color=\"accent\">\r\n                <mat-icon>{{btn.icon}}</mat-icon>\r\n            </button>\r\n        </ng-container>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"fab-dismiss\"\r\n     *ngIf=\"fabTogglerState==='active'\"\r\n     (click)=\"onToggleFab()\">\r\n</div>\r\n",
        animations: speedDialFabAnimations,
        styles: [""]
    }),
    __metadata("design:paramtypes", [])
], SpeedDialFabComponent);

class CardViewHtmlTextItemModel extends CardViewBaseItemModel {
    constructor(obj) {
        super(obj);
        this.type = 'html-text';
    }
    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        }
        else {
            return this.value;
        }
    }
}

let CardViewHtmlTextItemComponent = class CardViewHtmlTextItemComponent {
    constructor() {
    }
    ngOnChanges() {
    }
};
__decorate([
    Input(),
    __metadata("design:type", CardViewHtmlTextItemModel)
], CardViewHtmlTextItemComponent.prototype, "property", void 0);
CardViewHtmlTextItemComponent = __decorate([
    Component({
        selector: 'tradcard-view-html-text',
        template: "<div class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <span>\r\n        <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n            <span [innerHtml]=\"property.displayValue\"></span>\r\n        </span>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    }),
    __metadata("design:paramtypes", [])
], CardViewHtmlTextItemComponent);

class CardViewNameItemModel extends CardViewBaseItemModel {
    constructor(obj) {
        super(obj);
        this.type = 'name';
    }
    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        }
        else {
            return this.getValue();
        }
    }
    getValue() {
        return `${!!this.value.title ? this.value.title + ' ' : ''}
        ${!!this.value.firstName ? this.value.firstName + ' ' : ''}
        ${!!this.value.middleName ? this.value.middleName + ' ' : ''}
        ${!!this.value.surname ? this.value.surname + ' ' : ''}`.trim();
    }
}

class Address {
    constructor(street1, street2, city, lga) {
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.lga = lga;
        this.street1 = street1 ? street1 : null;
        this.street2 = street2 ? street2 : null;
        this.city = city ? city : null;
        this.lga = lga ? lga : null;
    }
}
class PersonName {
    constructor(title, firstName, middleName, surname) {
        this.title = title;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surname = surname;
    }
}
class Phone {
    constructor(phone1, phone2) {
        this.phone1 = phone1;
        this.phone2 = phone2;
    }
}

let CardViewNameItemComponent = class CardViewNameItemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    ngOnChanges() {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
    }
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    isClickable() {
        return this.property.clickable;
    }
    hasIcon() {
        return !!this.property.icon;
    }
    hasErrors() {
        return this.errorMessages && this.errorMessages.length;
    }
    setEditMode(editStatus) {
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.titleInput) {
                this.titleInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.firstNameInput) {
                this.firstNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.middleNameInput) {
                this.middleNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.surnameInput) {
                this.surnameInput.nativeElement.click();
            }
        }, 0);
    }
    reset() {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
        this.setEditMode(false);
    }
    update() {
        console.log('Property', this.property);
        if (this.property.isValid(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname))) {
            this.cardViewUpdateService.update(this.property, new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
            this.property.value = new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
        }
    }
    get displayValue() {
        return this.property.displayValue;
    }
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
};
CardViewNameItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
__decorate([
    Input(),
    __metadata("design:type", CardViewNameItemModel)
], CardViewNameItemComponent.prototype, "property", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewNameItemComponent.prototype, "editable", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewNameItemComponent.prototype, "displayEmpty", void 0);
__decorate([
    ViewChild('titleInput', { static: true }),
    __metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "titleInput", void 0);
__decorate([
    ViewChild('firstNameInput', { static: true }),
    __metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "firstNameInput", void 0);
__decorate([
    ViewChild('middleNameInput', { static: true }),
    __metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "middleNameInput", void 0);
__decorate([
    ViewChild('titleInput', { static: true }),
    __metadata("design:type", Object)
], CardViewNameItemComponent.prototype, "surnameInput", void 0);
CardViewNameItemComponent = __decorate([
    Component({
        selector: 'card-view-name-item',
        template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #titleInput\r\n                               matInput\r\n                               [placeholder]=\"'Title'\"\r\n                               [(ngModel)]=\"editedTitle\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #firstNameInput\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'First name'\"\r\n                               [(ngModel)]=\"editedFirstName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #middleNameInput\r\n                               matInput\r\n                               [placeholder]=\"'Middle name'\"\r\n                               [(ngModel)]=\"editedMiddleName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #surnameInput\r\n                               matInput\r\n                               [placeholder]=\"'Surname'\"\r\n                               [(ngModel)]=\"editedSurname\"\r\n                               [attr.data-automation-id]=\"'card-textitem-surnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
    }),
    __metadata("design:paramtypes", [CardViewUpdateService])
], CardViewNameItemComponent);

class CardViewAddressItemModel extends CardViewBaseItemModel {
    constructor(obj) {
        super(obj);
        this.type = 'address';
    }
    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        }
        else {
            return this.getValue();
        }
    }
    getValue() {
        return `${this.value.street1}${!!this.value.street2 ? ', ' + this.value.street2 : ''}
            ${!!this.value.city ? ', ' + this.value.city : ''}${!!this.value.lga ? ', ' + this.value.lga.name : ''}`
            .trim();
    }
}

let CardViewAddressItemComponent = class CardViewAddressItemComponent {
    constructor(cardViewUpdateService, stateService, lgaService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.stateService = stateService;
        this.lgaService = lgaService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    ngOnChanges() {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
    }
    ngOnInit() {
    }
    onChange(event) {
        this.lgaService.findByState(event.value.id).subscribe(res => this.lgas = res.body);
    }
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    isClickable() {
        return this.property.clickable;
    }
    hasIcon() {
        return !!this.property.icon;
    }
    hasErrors() {
        return this.errorMessages && this.errorMessages.length;
    }
    setEditMode(editStatus) {
        if (editStatus) {
            this.stateService.getStates().subscribe(res => this.states = res.body);
        }
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.cityInput) {
                this.cityInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.street1Input) {
                this.street1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.street2Input) {
                this.street2Input.nativeElement.click();
            }
        }, 0);
    }
    reset() {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
        this.setEditMode(false);
    }
    update() {
        if (this.property.isValid(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga))) {
            this.cardViewUpdateService.update(this.property, new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
            this.property.value = new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
        }
    }
    get displayValue() {
        return this.property.displayValue;
    }
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
    entityCompare(s1, s2) {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    }
};
CardViewAddressItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService },
    { type: StateService },
    { type: LgaService }
];
__decorate([
    Input(),
    __metadata("design:type", CardViewAddressItemModel)
], CardViewAddressItemComponent.prototype, "property", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewAddressItemComponent.prototype, "editable", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewAddressItemComponent.prototype, "displayEmpty", void 0);
__decorate([
    ViewChild('cityInput', { static: true }),
    __metadata("design:type", Object)
], CardViewAddressItemComponent.prototype, "cityInput", void 0);
__decorate([
    ViewChild('street1Input', { static: true }),
    __metadata("design:type", Object)
], CardViewAddressItemComponent.prototype, "street1Input", void 0);
__decorate([
    ViewChild('street2Input', { static: true }),
    __metadata("design:type", Object)
], CardViewAddressItemComponent.prototype, "street2Input", void 0);
CardViewAddressItemComponent = __decorate([
    Component({
        selector: 'card-view-address-item',
        template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #street1Input\r\n                               matInput\r\n                               [placeholder]=\"'Street Line 1'\"\r\n                               [(ngModel)]=\"editedStreet1\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #street2Input\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'Street Line 2'\"\r\n                               [(ngModel)]=\"editedStreet2\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #cityInput\r\n                               matInput\r\n                               [placeholder]=\"'City'\"\r\n                               [(ngModel)]=\"editedCity\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field>\r\n                        <mat-select (selectionChange)=\"onChange($event)\"\r\n                                    placeholder=\"State\"\r\n                                    [(ngModel)]=\"state\"\r\n                                    [compareWith]=\"entityCompare\"\r\n                                    data-automation-class=\"select-box\">\r\n                            <mat-option *ngFor=\"let state of states \" [value]=\"state\">\r\n                                {{ state.name }}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field>\r\n                        <mat-select [(ngModel)]=\"editedLga\"\r\n                                    placeholder=\"LGA\"\r\n                                    [compareWith]=\"entityCompare\"\r\n                                    data-automation-class=\"select-box\">\r\n                            <mat-option *ngFor=\"let lga of lgas \" [value]=\"lga\">\r\n                                {{ lga.name }}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
    }),
    __metadata("design:paramtypes", [CardViewUpdateService,
        StateService,
        LgaService])
], CardViewAddressItemComponent);

/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class CardViewFixedKeyValuePairsItemModel extends CardViewBaseItemModel {
    constructor(obj) {
        super(obj);
        this.type = 'fixedkeyvaluepairs';
    }
    get displayValue() {
        return this.value;
    }
}

/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let CardViewFixedKeyvaluepairsitemComponent = class CardViewFixedKeyvaluepairsitemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    ngOnChanges() {
        this.values = this.property.value || [];
        this.matTableValues = new MatTableDataSource(this.values);
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    add() {
        this.values.push({ name: '', value: '' });
    }
    remove(index) {
        this.values.splice(index, 1);
        this.save(true);
    }
    onBlur(value) {
        if (value.length) {
            this.save();
        }
    }
    save(remove) {
        const validValues = this.values.filter(i => i.name.length && i.value.length);
        if (remove || validValues.length) {
            this.cardViewUpdateService.update(this.property, validValues);
            this.property.value = validValues;
        }
    }
};
CardViewFixedKeyvaluepairsitemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
__decorate([
    Input(),
    __metadata("design:type", CardViewFixedKeyValuePairsItemModel)
], CardViewFixedKeyvaluepairsitemComponent.prototype, "property", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewFixedKeyvaluepairsitemComponent.prototype, "editable", void 0);
CardViewFixedKeyvaluepairsitemComponent = __decorate([
    Component({
        selector: 'card-view-keyvaluepair',
        template: "<div [attr.data-automation-id]=\"'card-key-value-pairs-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n\r\n    <div *ngIf=\"!isEditable()\" class=\"card-view__key-value-pairs__read-only\">\r\n        <mat-table #table [dataSource]=\"matTableValues\" class=\"mat-elevation-z8\">\r\n            <ng-container matColumnDef=\"name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.name}}</mat-cell>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"value\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.value}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"['name', 'value']\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: ['name', 'value'];\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n\r\n\r\n    <div class=\"card-view__key-value-pairs\" *ngIf=\"isEditable() && values && values.length\">\r\n        <div class=\"card-view__key-value-pairs__row\">\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</div>\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</div>\r\n        </div>\r\n\r\n        <div class=\"card-view__key-value-pairs__row\" *ngFor=\"let item of values; let i = index\">\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           [disabled]=\"true\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-name-input-' + i\"\r\n                           [(ngModel)]=\"values[i].name\">\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           (blur)=\"onBlur(item.value)\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-value-input-' + i\"\r\n                           [(ngModel)]=\"values[i].value\">\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [".card-view__key-value-pairs__col{display:inline-block;width:39%}.card-view__key-value-pairs__col .mat-form-field{width:100%}.card-view__key-value-pairs__read-only .mat-table{box-shadow:none}.card-view__key-value-pairs__read-only .mat-header-row,.card-view__key-value-pairs__read-only .mat-row{padding:0}"]
    }),
    __metadata("design:paramtypes", [CardViewUpdateService])
], CardViewFixedKeyvaluepairsitemComponent);

class CardViewPhoneItemModel extends CardViewBaseItemModel {
    constructor(obj) {
        super(obj);
        this.type = 'phone';
    }
    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        }
        else {
            return this.getValue();
        }
    }
    getValue() {
        return `${!!this.value.phone1 ? this.value.phone1 : ''}
        ${!!this.value.phone2 ? ', ' + this.value.phone2 : ''}`.trim();
    }
}

let CardViewPhoneItemComponent = class CardViewPhoneItemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    ngOnChanges() {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
    }
    ngOnInit() {
    }
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    isClickable() {
        return this.property.clickable;
    }
    hasIcon() {
        return !!this.property.icon;
    }
    hasErrors() {
        return this.errorMessages && this.errorMessages.length;
    }
    setEditMode(editStatus) {
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.phone1Input) {
                this.phone1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.phone2Input) {
                this.phone2Input.nativeElement.click();
            }
        }, 0);
    }
    reset() {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
        this.setEditMode(false);
    }
    update() {
        if (this.property.isValid(new Phone(this.editedPhone1, this.editedPhone2))) {
            this.cardViewUpdateService.update(this.property, new Phone(this.editedPhone1, this.editedPhone2));
            this.property.value = new Phone(this.editedPhone1, this.editedPhone2);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new Phone(this.editedPhone1, this.editedPhone2));
        }
    }
    get displayValue() {
        return this.property.displayValue;
    }
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
};
CardViewPhoneItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
__decorate([
    Input(),
    __metadata("design:type", CardViewPhoneItemModel)
], CardViewPhoneItemComponent.prototype, "property", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewPhoneItemComponent.prototype, "editable", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], CardViewPhoneItemComponent.prototype, "displayEmpty", void 0);
__decorate([
    ViewChild('phone1Input', { static: true }),
    __metadata("design:type", Object)
], CardViewPhoneItemComponent.prototype, "phone1Input", void 0);
__decorate([
    ViewChild('phone2Input', { static: true }),
    __metadata("design:type", Object)
], CardViewPhoneItemComponent.prototype, "phone2Input", void 0);
CardViewPhoneItemComponent = __decorate([
    Component({
        selector: 'card-view-phone-item',
        template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #phonr1Input\r\n                               matInput\r\n                               [placeholder]=\"'Phone 1'\"\r\n                               [(ngModel)]=\"editedPhone1\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #phone2Input\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'Phone 2'\"\r\n                               [(ngModel)]=\"editedPhone2\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
    }),
    __metadata("design:paramtypes", [CardViewUpdateService])
], CardViewPhoneItemComponent);

function components() {
    return [
        CardViewHtmlTextItemComponent,
        CardViewNameItemComponent,
        CardViewAddressItemComponent,
        CardViewFixedKeyvaluepairsitemComponent,
        CardViewPhoneItemComponent
    ];
}
let CardViewModule = class CardViewModule {
    constructor(cardItemTypeService) {
        this.cardItemTypeService = cardItemTypeService;
        cardItemTypeService.setComponentTypeResolver('html-text', () => CardViewHtmlTextItemComponent);
        cardItemTypeService.setComponentTypeResolver('phone', () => CardViewPhoneItemComponent);
        cardItemTypeService.setComponentTypeResolver('name', () => CardViewNameItemComponent);
        cardItemTypeService.setComponentTypeResolver('address', () => CardViewAddressItemComponent);
        cardItemTypeService.setComponentTypeResolver('fixedkeyvaluepairs', () => CardViewFixedKeyvaluepairsitemComponent);
    }
};
CardViewModule.ctorParameters = () => [
    { type: CardItemTypeService }
];
CardViewModule = __decorate([
    NgModule({
        imports: [
            CoreModule$1,
            CommonModule,
            FormsModule,
            FlexLayoutModule
        ],
        declarations: components(),
        exports: components(),
        entryComponents: components(),
        providers: [
            CardItemTypeService
        ]
    }),
    __metadata("design:paramtypes", [CardItemTypeService])
], CardViewModule);

const moment = _moment;
const LOCALE_CONFIG = new InjectionToken('daterangepicker.config');
/**
 *  DefaultLocaleConfig
 */
const DefaultLocaleConfig = {
    direction: 'ltr',
    separator: ' - ',
    weekLabel: 'W',
    applyLabel: 'Apply',
    cancelLabel: 'Cancel',
    customRangeLabel: 'Custom range',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek()
};

let LocaleService = class LocaleService {
    constructor(_config) {
        this._config = _config;
    }
    get config() {
        if (!this._config) {
            return DefaultLocaleConfig;
        }
        return Object.assign({}, DefaultLocaleConfig, this._config);
    }
};
LocaleService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
];
LocaleService = __decorate([
    Injectable(),
    __param(0, Inject(LOCALE_CONFIG)),
    __metadata("design:paramtypes", [Object])
], LocaleService);

var DateRangePicker_1;
const moment$1 = _moment;
var SideEnum;
(function (SideEnum) {
    SideEnum["left"] = "left";
    SideEnum["right"] = "right";
})(SideEnum || (SideEnum = {}));
let DateRangePicker = DateRangePicker_1 = class DateRangePicker {
    constructor(el, _ref, _localeService) {
        this.el = el;
        this._ref = _ref;
        this._localeService = _localeService;
        this._old = { start: null, end: null };
        this.calendarVariables = { left: {}, right: {} };
        this.timepickerVariables = { left: {}, right: {} };
        this.daterangepicker = { start: new FormControl(), end: new FormControl() };
        this.applyBtn = { disabled: false };
        this.startDate = moment$1().startOf('day');
        this.endDate = moment$1().endOf('day');
        this.dateLimit = null;
        // used in template for compile time support of enum values.
        this.sideEnum = SideEnum;
        this.minDate = null;
        this.maxDate = null;
        this.autoApply = false;
        this.singleDatePicker = false;
        this.showDropdowns = false;
        this.showWeekNumbers = false;
        this.showISOWeekNumbers = false;
        this.linkedCalendars = false;
        this.autoUpdateInput = true;
        this.alwaysShowCalendars = false;
        this.maxSpan = false;
        // timepicker variables
        this.timePicker = false;
        this.timePicker24Hour = false;
        this.timePickerIncrement = 1;
        this.timePickerSeconds = false;
        // end of timepicker variables
        this.showClearButton = false;
        this.firstMonthDayClass = null;
        this.lastMonthDayClass = null;
        this.emptyWeekRowClass = null;
        this.firstDayOfNextMonthClass = null;
        this.lastDayOfPreviousMonthClass = null;
        this._locale = {};
        // custom ranges
        this._ranges = {};
        this.showCancel = false;
        this.keepCalendarOpeningWithRange = false;
        this.showRangeLabelOnInput = false;
        this.rangesArray = [];
        // some state information
        this.isShown = false;
        this.inline = true;
        this.leftCalendar = {};
        this.rightCalendar = {};
        this.showCalInRanges = false;
        this.options = {}; // should get some opt from user
        this.chosenDate = new EventEmitter();
        this.rangeClicked = new EventEmitter();
        this.datesUpdated = new EventEmitter();
        this.cancel = new EventEmitter();
        this.delete = new EventEmitter();
    }
    set locale(value) {
        this._locale = Object.assign({}, this._localeService.config, value);
    }
    get locale() {
        return this._locale;
    }
    set ranges(value) {
        this._ranges = value;
        this.renderRanges();
    }
    get ranges() {
        return this._ranges;
    }
    ngOnInit() {
        this._buildLocale();
        const daysOfWeek = [...this.locale.daysOfWeek];
        if (this.locale.firstDay != 0) {
            let iterator = this.locale.firstDay;
            while (iterator > 0) {
                daysOfWeek.push(daysOfWeek.shift());
                iterator--;
            }
        }
        this.locale.daysOfWeek = daysOfWeek;
        if (this.inline) {
            this._old.start = this.startDate.clone();
            this._old.end = this.endDate.clone();
        }
        this.updateMonthsInView();
        this.renderCalendar(SideEnum.left);
        this.renderCalendar(SideEnum.right);
        this.renderRanges();
    }
    renderRanges() {
        this.rangesArray = [];
        let start, end;
        if (typeof this.ranges === 'object') {
            for (const range in this.ranges) {
                if (typeof this.ranges[range][0] === 'string') {
                    start = moment$1(this.ranges[range][0], this.locale.format);
                }
                else {
                    start = moment$1(this.ranges[range][0]);
                }
                if (typeof this.ranges[range][1] === 'string') {
                    end = moment$1(this.ranges[range][1], this.locale.format);
                }
                else {
                    end = moment$1(this.ranges[range][1]);
                }
                // If the start or end date exceed those allowed by the minDate or maxSpan
                // options, shorten the range to the allowable period.
                if (this.minDate && start.isBefore(this.minDate)) {
                    start = this.minDate.clone();
                }
                let maxDate = this.maxDate;
                if (this.maxSpan && maxDate && start.clone().add(this.maxSpan).isAfter(maxDate)) {
                    maxDate = start.clone().add(this.maxSpan);
                }
                if (maxDate && end.isAfter(maxDate)) {
                    end = maxDate.clone();
                }
                // If the end of the range is before the minimum or the start of the range is
                // after the maximum, don't display this range option at all.
                if ((this.minDate && end.isBefore(this.minDate, this.timePicker ? 'minute' : 'day'))
                    || (maxDate && start.isAfter(maxDate, this.timePicker ? 'minute' : 'day'))) {
                    continue;
                }
                //Support unicode chars in the range names.
                let elem = document.createElement('textarea');
                elem.innerHTML = range;
                let rangeHtml = elem.value;
                this.ranges[rangeHtml] = [start, end];
            }
            for (const range in this.ranges) {
                this.rangesArray.push(range);
            }
            if (this.showCustomRangeLabel) {
                this.rangesArray.push(this.locale.customRangeLabel);
            }
            this.showCalInRanges = (!this.rangesArray.length) || this.alwaysShowCalendars;
            if (!this.timePicker) {
                this.startDate = this.startDate.startOf('day');
                this.endDate = this.endDate.endOf('day');
            }
            // can't be used together for now
            if (this.timePicker && this.autoApply) {
                this.autoApply = false;
            }
        }
    }
    renderTimePicker(side) {
        let disabled;
        let time;
        let padded;
        let i;
        if (side == SideEnum.right && !this.endDate) {
            return;
        }
        let selected, minDate;
        let maxDate = this.maxDate;
        if (side === SideEnum.left) {
            selected = this.startDate.clone();
            minDate = this.minDate;
        }
        else if (side === SideEnum.right) {
            selected = this.endDate.clone();
            minDate = this.startDate;
        }
        const start = this.timePicker24Hour ? 0 : 1;
        const end = this.timePicker24Hour ? 23 : 12;
        this.timepickerVariables[side] = {
            hours: [],
            minutes: [],
            minutesLabel: [],
            seconds: [],
            secondsLabel: [],
            disabledHours: [],
            disabledMinutes: [],
            disabledSeconds: [],
            selectedHour: 0,
            selectedMinute: 0,
            selectedSecond: 0,
        };
        // generate hours
        for (let i = start; i <= end; i++) {
            let i_in_24 = i;
            if (!this.timePicker24Hour) {
                i_in_24 = selected.hour() >= 12 ? (i == 12 ? 12 : i + 12) : (i == 12 ? 0 : i);
            }
            let time = selected.clone().hour(i_in_24);
            let disabled = false;
            if (minDate && time.minute(59).isBefore(minDate)) {
                disabled = true;
            }
            if (maxDate && time.minute(0).isAfter(maxDate)) {
                disabled = true;
            }
            this.timepickerVariables[side].hours.push(i);
            if (i_in_24 == selected.hour() && !disabled) {
                this.timepickerVariables[side].selectedHour = i;
            }
            else if (disabled) {
                this.timepickerVariables[side].disabledHours.push(i);
            }
        }
        // generate minutes
        for (i = 0; i < 60; i += this.timePickerIncrement) {
            padded = i < 10 ? '0' + i : i;
            time = selected.clone().minute(i);
            disabled = minDate && time.second(59).isBefore(minDate);
            if (maxDate && time.second(0).isAfter(maxDate)) {
                disabled = true;
            }
            this.timepickerVariables[side].minutes.push(i);
            this.timepickerVariables[side].minutesLabel.push(padded);
            if (selected.minute() == i && !disabled) {
                this.timepickerVariables[side].selectedMinute = i;
            }
            else if (disabled) {
                this.timepickerVariables[side].disabledMinutes.push(i);
            }
        }
        // generate seconds
        if (this.timePickerSeconds) {
            for (i = 0; i < 60; i++) {
                padded = i < 10 ? '0' + i : i;
                time = selected.clone().second(i);
                disabled = minDate && time.isBefore(minDate);
                if (maxDate && time.isAfter(maxDate)) {
                    disabled = true;
                }
                this.timepickerVariables[side].seconds.push(i);
                this.timepickerVariables[side].secondsLabel.push(padded);
                if (selected.second() == i && !disabled) {
                    this.timepickerVariables[side].selectedSecond = i;
                }
                else if (disabled) {
                    this.timepickerVariables[side].disabledSeconds.push(i);
                }
            }
        }
        // generate AM/PM
        if (!this.timePicker24Hour) {
            let am_html = '';
            let pm_html = '';
            if (minDate && selected.clone().hour(12).minute(0).second(0).isBefore(minDate)) {
                this.timepickerVariables[side].amDisabled = true;
            }
            if (maxDate && selected.clone().hour(0).minute(0).second(0).isAfter(maxDate)) {
                this.timepickerVariables[side].pmDisabled = true;
            }
            if (selected.hour() >= 12) {
                this.timepickerVariables[side].ampmModel = 'PM';
            }
            else {
                this.timepickerVariables[side].ampmModel = 'AM';
            }
        }
        this.timepickerVariables[side].selected = selected;
    }
    renderCalendar(side) {
        let mainCalendar = (side === SideEnum.left) ? this.leftCalendar : this.rightCalendar;
        const month = mainCalendar.month.month();
        const year = mainCalendar.month.year();
        const hour = mainCalendar.month.hour();
        const minute = mainCalendar.month.minute();
        const second = mainCalendar.month.second();
        const daysInMonth = moment$1([year, month]).daysInMonth();
        const firstDay = moment$1([year, month, 1]);
        const lastDay = moment$1([year, month, daysInMonth]);
        const lastMonth = moment$1(firstDay).subtract(1, 'month').month();
        const lastYear = moment$1(firstDay).subtract(1, 'month').year();
        const daysInLastMonth = moment$1([lastYear, lastMonth]).daysInMonth();
        const dayOfWeek = firstDay.day();
        // initialize a 6 rows x 7 columns array for the calendar
        let calendar = [];
        calendar.firstDay = firstDay;
        calendar.lastDay = lastDay;
        for (let i = 0; i < 6; i++) {
            calendar[i] = [];
        }
        // populate the calendar with date objects
        let startDay = daysInLastMonth - dayOfWeek + this.locale.firstDay + 1;
        if (startDay > daysInLastMonth) {
            startDay -= 7;
        }
        if (dayOfWeek === this.locale.firstDay) {
            startDay = daysInLastMonth - 6;
        }
        let curDate = moment$1([lastYear, lastMonth, startDay, 12, minute, second]);
        for (let i = 0, col = 0, row = 0; i < 42; i++, col++, curDate = moment$1(curDate).add(24, 'hour')) {
            if (i > 0 && col % 7 === 0) {
                col = 0;
                row++;
            }
            calendar[row][col] = curDate.clone().hour(hour).minute(minute).second(second);
            curDate.hour(12);
            if (this.minDate && calendar[row][col].format('YYYY-MM-DD') === this.minDate.format('YYYY-MM-DD') &&
                calendar[row][col].isBefore(this.minDate) && side === 'left') {
                calendar[row][col] = this.minDate.clone();
            }
            if (this.maxDate && calendar[row][col].format('YYYY-MM-DD') === this.maxDate.format('YYYY-MM-DD') &&
                calendar[row][col].isAfter(this.maxDate) && side === 'right') {
                calendar[row][col] = this.maxDate.clone();
            }
        }
        // make the calendar object available to hoverDate/clickDate
        if (side === SideEnum.left) {
            this.leftCalendar.calendar = calendar;
        }
        else {
            this.rightCalendar.calendar = calendar;
        }
        //
        // Display the calendar
        //
        const minDate = side === 'left' ? this.minDate : this.startDate;
        let maxDate = this.maxDate;
        // adjust maxDate to reflect the dateLimit setting in order to
        // grey out end dates beyond the dateLimit
        if (this.endDate === null && this.dateLimit) {
            const maxLimit = this.startDate.clone().add(this.dateLimit, 'day').endOf('day');
            if (!maxDate || maxLimit.isBefore(maxDate)) {
                maxDate = maxLimit;
            }
        }
        this.calendarVariables[side] = {
            month: month,
            year: year,
            hour: hour,
            minute: minute,
            second: second,
            daysInMonth: daysInMonth,
            firstDay: firstDay,
            lastDay: lastDay,
            lastMonth: lastMonth,
            lastYear: lastYear,
            daysInLastMonth: daysInLastMonth,
            dayOfWeek: dayOfWeek,
            // other vars
            calRows: Array.from(Array(6).keys()),
            calCols: Array.from(Array(7).keys()),
            classes: {},
            minDate: minDate,
            maxDate: maxDate,
            calendar: calendar
        };
        if (this.showDropdowns) {
            const currentMonth = calendar[1][1].month();
            const currentYear = calendar[1][1].year();
            const maxYear = (maxDate && maxDate.year()) || (currentYear + 5);
            const minYear = (minDate && minDate.year()) || (currentYear - 50);
            const inMinYear = currentYear === minYear;
            const inMaxYear = currentYear === maxYear;
            const years = [];
            for (let y = minYear; y <= maxYear; y++) {
                years.push(y);
            }
            this.calendarVariables[side].dropdowns = {
                currentMonth: currentMonth,
                currentYear: currentYear,
                maxYear: maxYear,
                minYear: minYear,
                inMinYear: inMinYear,
                inMaxYear: inMaxYear,
                monthArrays: Array.from(Array(12).keys()),
                yearArrays: years
            };
        }
        this._buildCells(calendar, side);
    }
    setStartDate(startDate) {
        if (typeof startDate === 'string') {
            this.startDate = moment$1(startDate, this.locale.format);
        }
        if (typeof startDate === 'object') {
            this.startDate = moment$1(startDate);
        }
        if (!this.timePicker) {
            this.startDate = this.startDate.startOf('day');
        }
        if (this.timePicker && this.timePickerIncrement) {
            this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
        }
        if (this.minDate && this.startDate.isBefore(this.minDate)) {
            this.startDate = this.minDate.clone();
            if (this.timePicker && this.timePickerIncrement) {
                this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
            }
        }
        if (this.maxDate && this.startDate.isAfter(this.maxDate)) {
            this.startDate = this.maxDate.clone();
            if (this.timePicker && this.timePickerIncrement) {
                this.startDate.minute(Math.floor(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
            }
        }
        if (!this.isShown) {
            this.updateElement();
        }
        this.updateMonthsInView();
    }
    setEndDate(endDate) {
        if (typeof endDate === 'string') {
            this.endDate = moment$1(endDate, this.locale.format);
        }
        if (typeof endDate === 'object') {
            this.endDate = moment$1(endDate);
        }
        if (!this.timePicker) {
            this.endDate = this.endDate.add(1, 'd').startOf('day').subtract(1, 'second');
        }
        if (this.timePicker && this.timePickerIncrement) {
            this.endDate.minute(Math.round(this.endDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
        }
        if (this.endDate.isBefore(this.startDate)) {
            this.endDate = this.startDate.clone();
        }
        if (this.maxDate && this.endDate.isAfter(this.maxDate)) {
            this.endDate = this.maxDate.clone();
        }
        if (this.dateLimit && this.startDate.clone().add(this.dateLimit, 'day').isBefore(this.endDate)) {
            this.endDate = this.startDate.clone().add(this.dateLimit, 'day');
        }
        if (!this.isShown) {
            // this.updateElement();
        }
        this.updateMonthsInView();
    }
    isInvalidDate(date) {
        return false;
    }
    isCustomDate(date) {
        return false;
    }
    updateView() {
        if (this.timePicker) {
            this.renderTimePicker(SideEnum.left);
            this.renderTimePicker(SideEnum.right);
        }
        this.updateMonthsInView();
        this.updateCalendars();
    }
    updateMonthsInView() {
        if (this.endDate) {
            // if both dates are visible already, do nothing
            if (!this.singleDatePicker && this.leftCalendar.month && this.rightCalendar.month &&
                ((this.startDate && this.leftCalendar && this.startDate.format('YYYY-MM') === this.leftCalendar.month.format('YYYY-MM')) ||
                    (this.startDate && this.rightCalendar && this.startDate.format('YYYY-MM') === this.rightCalendar.month.format('YYYY-MM')))
                &&
                    (this.endDate.format('YYYY-MM') === this.leftCalendar.month.format('YYYY-MM') ||
                        this.endDate.format('YYYY-MM') === this.rightCalendar.month.format('YYYY-MM'))) {
                return;
            }
            if (this.startDate) {
                this.leftCalendar.month = this.startDate.clone().date(2);
                if (!this.linkedCalendars && (this.endDate.month() !== this.startDate.month() ||
                    this.endDate.year() !== this.startDate.year())) {
                    this.rightCalendar.month = this.endDate.clone().date(2);
                }
                else {
                    this.rightCalendar.month = this.startDate.clone().date(2).add(1, 'month');
                }
            }
        }
        else {
            if (this.leftCalendar.month.format('YYYY-MM') !== this.startDate.format('YYYY-MM') &&
                this.rightCalendar.month.format('YYYY-MM') !== this.startDate.format('YYYY-MM')) {
                this.leftCalendar.month = this.startDate.clone().date(2);
                this.rightCalendar.month = this.startDate.clone().date(2).add(1, 'month');
            }
        }
        if (this.maxDate && this.linkedCalendars && !this.singleDatePicker && this.rightCalendar.month > this.maxDate) {
            this.rightCalendar.month = this.maxDate.clone().date(2);
            this.leftCalendar.month = this.maxDate.clone().date(2).subtract(1, 'month');
        }
    }
    /**
     *  This is responsible for updating the calendars
     */
    updateCalendars() {
        this.renderCalendar(SideEnum.left);
        this.renderCalendar(SideEnum.right);
        if (this.endDate === null) {
            return;
        }
        this.calculateChosenLabel();
    }
    updateElement() {
        if (!this.singleDatePicker && this.autoUpdateInput) {
            if (this.startDate && this.endDate) {
                // if we use ranges and should show range label on inpu
                if (this.rangesArray.length && this.showRangeLabelOnInput === true && this.chosenRange &&
                    this.locale.customRangeLabel !== this.chosenRange) {
                    this.chosenLabel = this.chosenRange;
                }
                else {
                    this.chosenLabel = this.startDate.format(this.locale.format) +
                        this.locale.separator + this.endDate.format(this.locale.format);
                }
            }
        }
        else if (this.autoUpdateInput) {
            this.chosenLabel = this.startDate.format(this.locale.format);
        }
    }
    remove() {
        this.isShown = false;
    }
    /**
     * this should calculate the label
     */
    calculateChosenLabel() {
        if (!this.locale || !this.locale.separator) {
            this._buildLocale();
        }
        let customRange = true;
        let i = 0;
        if (this.rangesArray.length > 0) {
            for (const range in this.ranges) {
                if (this.timePicker) {
                    let format = this.timePickerSeconds ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD HH:mm";
                    //ignore times when comparing dates if time picker seconds is not enabled
                    if (this.startDate.format(format) == this.ranges[range][0].format(format) && this.endDate.format(format) == this.ranges[range][1].format(format)) {
                        customRange = false;
                        this.chosenRange = this.rangesArray[i];
                        break;
                    }
                }
                else {
                    //ignore times when comparing dates if time picker is not enabled
                    if (this.startDate.format('YYYY-MM-DD') == this.ranges[range][0].format('YYYY-MM-DD') && this.endDate.format('YYYY-MM-DD') == this.ranges[range][1].format('YYYY-MM-DD')) {
                        customRange = false;
                        this.chosenRange = this.rangesArray[i];
                        break;
                    }
                }
                i++;
            }
            if (customRange) {
                if (this.showCustomRangeLabel) {
                    this.chosenRange = this.locale.customRangeLabel;
                }
                else {
                    this.chosenRange = null;
                }
                // if custom label: show calenar
                this.showCalInRanges = true;
            }
        }
        this.updateElement();
    }
    clickApply(e) {
        if (!this.singleDatePicker && this.startDate && !this.endDate) {
            this.endDate = this.startDate.clone();
            this.calculateChosenLabel();
        }
        if (this.isInvalidDate && this.startDate && this.endDate) {
            // get if there are invalid date between range
            let d = this.startDate.clone();
            while (d.isBefore(this.endDate)) {
                if (this.isInvalidDate(d)) {
                    this.endDate = d.subtract(1, 'days');
                    this.calculateChosenLabel();
                    break;
                }
                d.add(1, 'days');
            }
        }
        if (this.chosenLabel) {
            this.chosenDate.emit({ chosenLabel: this.chosenLabel, startDate: this.startDate, endDate: this.endDate });
        }
        this.datesUpdated.emit({ startDate: this.startDate, endDate: this.endDate });
        this.hide();
    }
    clickCancel(e) {
        this.startDate = this._old.start;
        this.endDate = this._old.end;
        if (this.inline) {
            this.updateView();
        }
        this.hide();
        this.cancel.emit(null);
    }
    /**
     * called when month is changed
     * @param monthEvent get value in event.target.value
     * @param side left or right
     */
    monthChanged(monthEvent, side) {
        const year = this.calendarVariables[side].dropdowns.currentYear;
        const month = parseInt(monthEvent.target.value, 10);
        this.monthOrYearChanged(month, year, side);
    }
    /**
     * called when year is changed
     * @param yearEvent get value in event.target.value
     * @param side left or right
     */
    yearChanged(yearEvent, side) {
        const month = this.calendarVariables[side].dropdowns.currentMonth;
        const year = parseInt(yearEvent.target.value, 10);
        this.monthOrYearChanged(month, year, side);
    }
    /**
     * called when time is changed
     * @param timeEvent  an event
     * @param side left or right
     */
    timeChanged(timeEvent, side) {
        let hour = parseInt(this.timepickerVariables[side].selectedHour, 10);
        let minute = parseInt(this.timepickerVariables[side].selectedMinute, 10);
        let second = this.timePickerSeconds ? parseInt(this.timepickerVariables[side].selectedSecond, 10) : 0;
        if (!this.timePicker24Hour) {
            let ampm = this.timepickerVariables[side].ampmModel;
            if (ampm === 'PM' && hour < 12)
                hour += 12;
            if (ampm === 'AM' && hour === 12)
                hour = 0;
        }
        if (side === SideEnum.left) {
            let start = this.startDate.clone();
            start.hour(hour);
            start.minute(minute);
            start.second(second);
            this.setStartDate(start);
            if (this.singleDatePicker) {
                this.endDate = this.startDate.clone();
            }
            else if (this.endDate && this.endDate.format('YYYY-MM-DD') == start.format('YYYY-MM-DD') && this.endDate.isBefore(start)) {
                this.setEndDate(start.clone());
            }
        }
        else if (this.endDate) {
            let end = this.endDate.clone();
            end.hour(hour);
            end.minute(minute);
            end.second(second);
            this.setEndDate(end);
        }
        //update the calendars so all clickable dates reflect the new time component
        this.updateCalendars();
        //re-render the time pickers because changing one selection can affect what's enabled in another
        this.renderTimePicker(SideEnum.left);
        this.renderTimePicker(SideEnum.right);
    }
    /**
     *  call when month or year changed
     * @param month month number 0 -11
     * @param year year eg: 1995
     * @param side left or right
     */
    monthOrYearChanged(month, year, side) {
        const isLeft = side === SideEnum.left;
        if (!isLeft) {
            if (year < this.startDate.year() || (year === this.startDate.year() && month < this.startDate.month())) {
                month = this.startDate.month();
                year = this.startDate.year();
            }
        }
        if (this.minDate) {
            if (year < this.minDate.year() || (year === this.minDate.year() && month < this.minDate.month())) {
                month = this.minDate.month();
                year = this.minDate.year();
            }
        }
        if (this.maxDate) {
            if (year > this.maxDate.year() || (year === this.maxDate.year() && month > this.maxDate.month())) {
                month = this.maxDate.month();
                year = this.maxDate.year();
            }
        }
        this.calendarVariables[side].dropdowns.currentYear = year;
        this.calendarVariables[side].dropdowns.currentMonth = month;
        if (isLeft) {
            this.leftCalendar.month.month(month).year(year);
            if (this.linkedCalendars) {
                this.rightCalendar.month = this.leftCalendar.month.clone().add(1, 'month');
            }
        }
        else {
            this.rightCalendar.month.month(month).year(year);
            if (this.linkedCalendars) {
                this.leftCalendar.month = this.rightCalendar.month.clone().subtract(1, 'month');
            }
        }
        this.updateCalendars();
    }
    /**
     * Click on previous month
     * @param side left or right calendar
     */
    clickPrev(side) {
        if (side === SideEnum.left) {
            this.leftCalendar.month.subtract(1, 'month');
            if (this.linkedCalendars) {
                this.rightCalendar.month.subtract(1, 'month');
            }
        }
        else {
            this.rightCalendar.month.subtract(1, 'month');
        }
        this.updateCalendars();
    }
    /**
     * Click on next month
     * @param side left or right calendar
     */
    clickNext(side) {
        if (side === SideEnum.left) {
            this.leftCalendar.month.add(1, 'month');
        }
        else {
            this.rightCalendar.month.add(1, 'month');
            if (this.linkedCalendars) {
                this.leftCalendar.month.add(1, 'month');
            }
        }
        this.updateCalendars();
    }
    /**
     * When selecting a date
     * @param e event: get value by e.target.value
     * @param side left or right
     * @param row row position of the current date clicked
     * @param col col position of the current date clicked
     */
    clickDate(e, side, row, col) {
        if (e.target.tagName === 'TD') {
            if (!e.target.classList.contains('available')) {
                return;
            }
        }
        else if (e.target.tagName === 'SPAN') {
            if (!e.target.parentElement.classList.contains('available')) {
                return;
            }
        }
        if (this.rangesArray.length) {
            this.chosenRange = this.locale.customRangeLabel;
        }
        let date = side === SideEnum.left ? this.leftCalendar.calendar[row][col] : this.rightCalendar.calendar[row][col];
        if (this.endDate || date.isBefore(this.startDate, 'day')) { // picking start
            if (this.timePicker) {
                date = this._getDateWithTime(date, SideEnum.left);
            }
            this.endDate = null;
            this.setStartDate(date.clone());
        }
        else if (!this.endDate && date.isBefore(this.startDate)) {
            // special case: clicking the same date for start/end,
            // but the time of the end date is before the start date
            this.setEndDate(this.startDate.clone());
        }
        else { // picking end
            if (this.timePicker) {
                date = this._getDateWithTime(date, SideEnum.right);
            }
            this.setEndDate(date.clone());
            if (this.autoApply) {
                this.calculateChosenLabel();
                this.clickApply();
            }
        }
        if (this.singleDatePicker) {
            this.setEndDate(this.startDate);
            this.updateElement();
            if (this.autoApply) {
                this.clickApply();
            }
        }
        this.updateView();
        // This is to cancel the blur event handler if the mouse was in one of the inputs
        e.stopPropagation();
    }
    /**
     *  Click on the custom range
     * @param e: Event
     * @param label
     */
    clickRange(e, label) {
        this.chosenRange = label;
        if (label == this.locale.customRangeLabel) {
            this.isShown = true; // show calendars
            this.showCalInRanges = true;
        }
        else {
            let dates = this.ranges[label];
            this.startDate = dates[0].clone();
            this.endDate = dates[1].clone();
            if (this.showRangeLabelOnInput && label !== this.locale.customRangeLabel) {
                this.chosenLabel = label;
            }
            else {
                this.calculateChosenLabel();
            }
            this.showCalInRanges = (!this.rangesArray.length) || this.alwaysShowCalendars;
            if (!this.timePicker) {
                this.startDate.startOf('day');
                this.endDate.endOf('day');
            }
            if (!this.alwaysShowCalendars) {
                this.isShown = false; // hide calendars
            }
            this.rangeClicked.emit({ label: label, dates: dates });
            if (!this.keepCalendarOpeningWithRange) {
                this.clickApply();
            }
            else {
                this.leftCalendar.month.month(dates[0].month());
                this.leftCalendar.month.year(dates[0].year());
                this.rightCalendar.month.month(dates[1].month());
                this.rightCalendar.month.year(dates[1].year());
                this.updateCalendars();
                if (this.timePicker) {
                    this.renderTimePicker(SideEnum.left);
                    this.renderTimePicker(SideEnum.right);
                }
            }
        }
    }
    ;
    show(e) {
        if (this.isShown) {
            return;
        }
        this._old.start = this.startDate.clone();
        this._old.end = this.endDate.clone();
        this.isShown = true;
        this.updateView();
    }
    hide(e) {
        if (!this.isShown) {
            return;
        }
        // incomplete date selection, revert to last values
        if (!this.endDate) {
            if (this._old.start) {
                this.startDate = this._old.start.clone();
            }
            if (this._old.end) {
                this.endDate = this._old.end.clone();
            }
        }
        // if a new date range was selected, invoke the user callback function
        if (!this.startDate.isSame(this._old.start) || !this.endDate.isSame(this._old.end)) {
            // this.callback(this.startDate, this.endDate, this.chosenLabel);
        }
        // if picker is attached to a text input, update it
        this.updateElement();
        this.isShown = false;
        this._ref.detectChanges();
    }
    /**
     * handle click on all element in the component, usefull for outside of click
     * @param e event
     */
    handleInternalClick(e) {
        e.stopPropagation();
    }
    /**
     * update the locale options
     * @param locale
     */
    updateLocale(locale) {
        for (const key in locale) {
            if (locale.hasOwnProperty(key)) {
                this.locale[key] = locale[key];
            }
        }
    }
    /**
     *  clear the daterange picker
     */
    clear() {
        this.startDate = moment$1().startOf('day');
        this.endDate = moment$1().endOf('day');
        this.chosenDate.emit({ chosenLabel: '', startDate: null, endDate: null });
        this.datesUpdated.emit({ startDate: null, endDate: null });
        this.hide();
        this.delete.emit(null);
    }
    /**
     * Find out if the selected range should be disabled if it doesn't
     * fit into minDate and maxDate limitations.
     */
    disableRange(range) {
        if (range === this.locale.customRangeLabel) {
            return false;
        }
        const rangeMarkers = this.ranges[range];
        const areBothBefore = rangeMarkers.every(date => {
            if (!this.minDate) {
                return false;
            }
            return date.isBefore(this.minDate);
        });
        const areBothAfter = rangeMarkers.every(date => {
            if (!this.maxDate) {
                return false;
            }
            return date.isAfter(this.maxDate);
        });
        return (areBothBefore || areBothAfter);
    }
    /**
     *
     * @param date the date to add time
     * @param side left or right
     */
    _getDateWithTime(date, side) {
        let hour = parseInt(this.timepickerVariables[side].selectedHour, 10);
        if (!this.timePicker24Hour) {
            let ampm = this.timepickerVariables[side].ampmModel;
            if (ampm === 'PM' && hour < 12)
                hour += 12;
            if (ampm === 'AM' && hour === 12)
                hour = 0;
        }
        let minute = parseInt(this.timepickerVariables[side].selectedMinute, 10);
        let second = this.timePickerSeconds ? parseInt(this.timepickerVariables[side].selectedSecond, 10) : 0;
        return date.clone().hour(hour).minute(minute).second(second);
    }
    /**
     *  build the locale config
     */
    _buildLocale() {
        this.locale = Object.assign({}, this._localeService.config, this.locale);
        if (!this.locale.format) {
            if (this.timePicker) {
                this.locale.format = moment$1.localeData().longDateFormat('lll');
            }
            else {
                this.locale.format = moment$1.localeData().longDateFormat('L');
            }
        }
    }
    _buildCells(calendar, side) {
        for (let row = 0; row < 6; row++) {
            this.calendarVariables[side].classes[row] = {};
            const rowClasses = ['row'];
            if (this.emptyWeekRowClass && !this.hasCurrentMonthDays(this.calendarVariables[side].month, calendar[row])) {
                rowClasses.push(this.emptyWeekRowClass);
            }
            for (let col = 0; col < 7; col++) {
                const classes = ['column cell'];
                // highlight today's date
                if (calendar[row][col].isSame(new Date(), 'day')) {
                    classes.push('today');
                }
                // highlight weekends
                if (calendar[row][col].isoWeekday() > 5) {
                    classes.push('weekend');
                }
                // grey out the dates in other months displayed at beginning and end of this calendar
                if (calendar[row][col].month() !== calendar[1][1].month()) {
                    classes.push('off');
                    // mark the last day of the previous month in this calendar
                    if (this.lastDayOfPreviousMonthClass && (calendar[row][col].month() < calendar[1][1].month() || calendar[1][1].month() === 0) && calendar[row][col].date() === this.calendarVariables[side].daysInLastMonth) {
                        classes.push(this.lastDayOfPreviousMonthClass);
                    }
                    // mark the first day of the next month in this calendar
                    if (this.firstDayOfNextMonthClass && (calendar[row][col].month() > calendar[1][1].month() || calendar[row][col].month() === 0) && calendar[row][col].date() === 1) {
                        classes.push(this.firstDayOfNextMonthClass);
                    }
                }
                // mark the first day of the current month with a custom class
                if (this.firstMonthDayClass && calendar[row][col].month() === calendar[1][1].month() && calendar[row][col].date() === calendar.firstDay.date()) {
                    classes.push(this.firstMonthDayClass);
                }
                // mark the last day of the current month with a custom class
                if (this.lastMonthDayClass && calendar[row][col].month() === calendar[1][1].month() && calendar[row][col].date() === calendar.lastDay.date()) {
                    classes.push(this.lastMonthDayClass);
                }
                // don't allow selection of dates before the minimum date
                if (this.minDate && calendar[row][col].isBefore(this.minDate, 'day')) {
                    classes.push('off', 'disabled');
                }
                // don't allow selection of dates after the maximum date
                if (this.calendarVariables[side].maxDate && calendar[row][col].isAfter(this.calendarVariables[side].maxDate, 'day')) {
                    classes.push('off', 'disabled');
                }
                // don't allow selection of date if a custom function decides it's invalid
                if (this.isInvalidDate(calendar[row][col])) {
                    classes.push('off', 'disabled');
                }
                // highlight the currently selected start date
                if (this.startDate && calendar[row][col].format('YYYY-MM-DD') === this.startDate.format('YYYY-MM-DD')) {
                    classes.push('active', 'start-date');
                }
                // highlight the currently selected end date
                if (this.endDate != null && calendar[row][col].format('YYYY-MM-DD') === this.endDate.format('YYYY-MM-DD')) {
                    classes.push('active', 'end-date');
                }
                // highlight dates in-between the selected dates
                if (this.endDate != null && calendar[row][col] > this.startDate && calendar[row][col] < this.endDate) {
                    classes.push('in-range');
                }
                // apply custom classes for this date
                const isCustom = this.isCustomDate(calendar[row][col]);
                if (isCustom !== false) {
                    if (typeof isCustom === 'string') {
                        classes.push(isCustom);
                    }
                    else {
                        Array.prototype.push.apply(classes, isCustom);
                    }
                }
                // store classes var
                let cname = '', disabled = false;
                for (let i = 0; i < classes.length; i++) {
                    cname += classes[i] + ' ';
                    if (classes[i] === 'disabled') {
                        disabled = true;
                    }
                }
                if (!disabled) {
                    cname += 'available';
                }
                this.calendarVariables[side].classes[row][col] = cname.replace(/^\s+|\s+$/g, '');
            }
            this.calendarVariables[side].classes[row].classList = rowClasses.join(' ');
        }
    }
    /**
     * Find out if the current calendar row has current month days
     * (as opposed to consisting of only previous/next month days)
     */
    hasCurrentMonthDays(currentMonth, row) {
        for (let day = 0; day < 7; day++) {
            if (row[day].month() === currentMonth) {
                return true;
            }
        }
        return false;
    }
};
DateRangePicker.ctorParameters = () => [
    { type: ElementRef },
    { type: ChangeDetectorRef },
    { type: LocaleService }
];
__decorate([
    Input(),
    __metadata("design:type", Number)
], DateRangePicker.prototype, "dateLimit", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], DateRangePicker.prototype, "minDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], DateRangePicker.prototype, "maxDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "autoApply", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "singleDatePicker", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showDropdowns", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showWeekNumbers", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showISOWeekNumbers", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "linkedCalendars", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "autoUpdateInput", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "alwaysShowCalendars", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "maxSpan", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "timePicker", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "timePicker24Hour", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], DateRangePicker.prototype, "timePickerIncrement", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "timePickerSeconds", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showClearButton", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "firstMonthDayClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "lastMonthDayClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "emptyWeekRowClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "firstDayOfNextMonthClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "lastDayOfPreviousMonthClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], DateRangePicker.prototype, "locale", null);
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], DateRangePicker.prototype, "ranges", null);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showCustomRangeLabel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showCancel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "keepCalendarOpeningWithRange", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePicker.prototype, "showRangeLabelOnInput", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "drops", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePicker.prototype, "opens", void 0);
__decorate([
    Output('chosenDate'),
    __metadata("design:type", EventEmitter)
], DateRangePicker.prototype, "chosenDate", void 0);
__decorate([
    Output('rangeClicked'),
    __metadata("design:type", EventEmitter)
], DateRangePicker.prototype, "rangeClicked", void 0);
__decorate([
    Output('datesUpdated'),
    __metadata("design:type", EventEmitter)
], DateRangePicker.prototype, "datesUpdated", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], DateRangePicker.prototype, "cancel", void 0);
__decorate([
    Output('clear'),
    __metadata("design:type", EventEmitter)
], DateRangePicker.prototype, "delete", void 0);
__decorate([
    ViewChild('pickerContainer', { static: true }),
    __metadata("design:type", ElementRef)
], DateRangePicker.prototype, "pickerContainer", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DateRangePicker.prototype, "isInvalidDate", null);
__decorate([
    Input(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DateRangePicker.prototype, "isCustomDate", null);
DateRangePicker = DateRangePicker_1 = __decorate([
    Component({
        selector: 'date-range-picker',
        template: "<div class=\"md-drppicker\" #pickerContainer\r\n     [ngClass]=\"{\r\n    ltr: locale.direction === 'ltr',\r\n    rtl: this.locale.direction === 'rtl',\r\n    'shown': isShown || inline,\r\n    'hidden': !isShown && !inline,\r\n    'inline': inline,\r\n    'double': !singleDatePicker && showCalInRanges,\r\n    'show-ranges': rangesArray.length\r\n}\" [class]=\"'drops-' + drops + '-' + opens\">\r\n    <mat-card class=\"m-0\">\r\n        <mat-card-content fxLayout=\"row wrap\">\r\n            <div fxFlex=\"20\" fxLayoutAlign=\"space-around\" fxFlex.lt-sm=\"100\" fxFlex.lt-md=\"30\" class=\"ranges\">\r\n                <ul>\r\n                    <li *ngFor=\"let range of rangesArray\">\r\n                        <button mat-button type=\"button\"\r\n                                (click)=\"clickRange($event, range)\"\r\n                                [disabled]=\"disableRange(range)\"\r\n                                [ngClass]=\"{'active': range === chosenRange}\">{{range}}</button>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n            <div fxLayout=\"row wrap\" fxLayoutAlign=\"space-around\" fxFlex.lt-sm=\"100\" fxFlex=\"80\" fxFlex.lt-md=\"70\">\r\n                <div fxFlex=\"100\" fxFlex.gt-sm=\"50\" *ngIf=\"showCalInRanges\">\r\n                    <div class=\"calendar-table\">\r\n                        <div class=\"table-condensed\" *ngIf=\"calendarVariables\">\r\n                            <div class=\"row\">\r\n                                <div class=\"column cell\" *ngIf=\"showWeekNumbers || showISOWeekNumbers\"></div>\r\n                                <ng-container\r\n                                        *ngIf=\"!calendarVariables.left.minDate || calendarVariables.left.minDate.isBefore(calendarVariables.left.calendar.firstDay) && (!this.linkedCalendars || true)\">\r\n                                    <div (click)=\"clickPrev(sideEnum.left)\" class=\"column cell prev available\">\r\n                                    </div>\r\n                                </ng-container>\r\n                                <ng-container\r\n                                        *ngIf=\"!(!calendarVariables.left.minDate || calendarVariables.left.minDate.isBefore(calendarVariables.left.calendar.firstDay) && (!this.linkedCalendars || true))\">\r\n                                    <div class=\"column cell\"></div>\r\n                                </ng-container>\r\n                                <div class=\"column5 cell month drp-animate\">\r\n                                    <ng-container *ngIf=\"showDropdowns && calendarVariables.left.dropdowns\">\r\n                                        <div class=\"dropdowns\">\r\n                                            {{this.locale.monthNames[calendarVariables?.left?.calendar[1][1].month()]}}\r\n                                            <select class=\"monthselect\"\r\n                                                    (change)=\"monthChanged($event, sideEnum.left)\">\r\n                                                <option\r\n                                                        [disabled]=\"(calendarVariables.left.dropdowns.inMinYear && m < calendarVariables.left.minDate.month()) || (calendarVariables.left.dropdowns.inMaxYear && m > calendarVariables.left.maxDate.month())\"\r\n                                                        *ngFor=\"let m of calendarVariables.left.dropdowns.monthArrays\"\r\n                                                        [value]=\"m\"\r\n                                                        [selected]=\"calendarVariables.left.dropdowns.currentMonth == m\">\r\n                                                    {{locale.monthNames[m]}}\r\n                                                </option>\r\n                                            </select>\r\n                                        </div>\r\n                                        <div class=\"dropdowns\">\r\n                                            {{ calendarVariables?.left?.calendar[1][1].format(\" YYYY\")}}\r\n                                            <select class=\"yearselect\"\r\n                                                    (change)=\"yearChanged($event, sideEnum.left)\">\r\n                                                <option *ngFor=\"let y of calendarVariables.left.dropdowns.yearArrays\"\r\n                                                        [selected]=\"y === calendarVariables.left.dropdowns.currentYear\">\r\n                                                    {{y}}\r\n                                                </option>\r\n                                            </select>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                    <ng-container *ngIf=\"!showDropdowns || !calendarVariables.left.dropdowns\">\r\n                                        {{this.locale.monthNames[calendarVariables?.left?.calendar[1][1].month()]}}  {{ calendarVariables?.left?.calendar[1][1].format(\" YYYY\")}}\r\n                                    </ng-container>\r\n                                </div>\r\n                                <ng-container\r\n                                        *ngIf=\"(!calendarVariables.left.maxDate || calendarVariables.left.maxDate.isAfter(calendarVariables.left.calendar.lastDay)) && (!linkedCalendars || singleDatePicker )\">\r\n                                    <div class=\"column next available\" (click)=\"clickNext(sideEnum.left)\">\r\n                                    </div>\r\n                                </ng-container>\r\n                                <ng-container\r\n                                        *ngIf=\"!((!calendarVariables.left.maxDate || calendarVariables.left.maxDate.isAfter(calendarVariables.left.calendar.lastDay)) && (!linkedCalendars || singleDatePicker ))\">\r\n                                    <div class=\"column cell\"></div>\r\n                                </ng-container>\r\n                            </div>\r\n                            <div class='row week-days'>\r\n                                <div *ngIf=\"showWeekNumbers || showISOWeekNumbers\" class=\"column week\">\r\n                                    <span>{{this.locale.weekLabel}}</span></div>\r\n                                <div class=\"column cell\" *ngFor=\"let dayofweek of locale.daysOfWeek\">\r\n                                    <span>{{dayofweek}}</span></div>\r\n                            </div>\r\n                            <div class=\"drp-animate\">\r\n                                <div class=\"row\" *ngFor=\"let row of calendarVariables.left.calRows\"\r\n                                     [class]=\"calendarVariables.left.classes[row].classList\">\r\n                                    <!-- add week number -->\r\n                                    <div class=\"column week\" *ngIf=\"showWeekNumbers\">\r\n                                        <span>{{calendarVariables.left.calendar[row][0].week()}}</span>\r\n                                    </div>\r\n                                    <div class=\"column week\" *ngIf=\"showISOWeekNumbers\">\r\n                                        <span>{{calendarVariables.left.calendar[row][0].isoWeek()}}</span>\r\n                                    </div>\r\n                                    <!-- cal -->\r\n                                    <div class=\"column\" *ngFor=\"let col of calendarVariables.left.calCols\"\r\n                                         [class]=\"calendarVariables.left.classes[row][col]\"\r\n                                         (click)=\"clickDate($event, sideEnum.left, row, col)\">\r\n                                        <span>{{calendarVariables.left.calendar[row][col].date()}}</span>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"calendar-time\" *ngIf=\"timePicker\">\r\n                        <div class=\"select\">\r\n                            <select class=\"hourselect select-item\" [disabled]=\"!endDate\"\r\n                                    [(ngModel)]=\"timepickerVariables.left.selectedHour\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.left)\">\r\n                                <option *ngFor=\"let i of timepickerVariables.left.hours\"\r\n                                        [value]=\"i\"\r\n                                        [disabled]=\"timepickerVariables.left.disabledHours.indexOf(i) > -1\">{{i}}</option>\r\n                            </select>\r\n                        </div>\r\n                        <div class=\"select\">\r\n                            <select class=\"select-item minuteselect\" [disabled]=\"!endDate\"\r\n                                    [(ngModel)]=\"timepickerVariables.left.selectedMinute\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.left)\">\r\n                                <option *ngFor=\"let i of timepickerVariables.left.minutes; let index = index;\"\r\n                                        [value]=\"i\"\r\n                                        [disabled]=\"timepickerVariables.left.disabledMinutes.indexOf(i) > -1\">{{timepickerVariables.left.minutesLabel[index]}}</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                        <div class=\"select\">\r\n                            <select class=\"select-item secondselect\" *ngIf=\"timePickerSeconds\" [disabled]=\"!endDate\"\r\n                                    [(ngModel)]=\"timepickerVariables.left.selectedSecond\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.left)\">\r\n                                <option *ngFor=\"let i of timepickerVariables.left.seconds; let index = index;\"\r\n                                        [value]=\"i\"\r\n                                        [disabled]=\"timepickerVariables.left.disabledSeconds.indexOf(i) > -1\">{{timepickerVariables.left.secondsLabel[index]}}</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                        <div class=\"select\">\r\n                            <select class=\"select-item ampmselect\" *ngIf=\"!timePicker24Hour\"\r\n                                    [(ngModel)]=\"timepickerVariables.left.ampmModel\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.left)\">\r\n                                <option value=\"AM\" [disabled]=\"timepickerVariables.left.amDisabled\">AM</option>\r\n                                <option value=\"PM\" [disabled]=\"timepickerVariables.left.pmDisabled\">PM</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div fxFlex=\"100\" fxFlex.gt-sm=\"50\" *ngIf=\"showCalInRanges && !singleDatePicker\">\r\n                    <div class=\"calendar-table\">\r\n                        <div class=\"table-condensed\" *ngIf=\"calendarVariables\">\r\n                            <div class=\"row\">\r\n                                <div class=\"column cell\" *ngIf=\"showWeekNumbers || showISOWeekNumbers\"></div>\r\n                                <ng-container\r\n                                        *ngIf=\"(!calendarVariables.right.minDate || calendarVariables.right.minDate.isBefore(calendarVariables.right.calendar.firstDay)) && (!this.linkedCalendars)\">\r\n                                    <div (click)=\"clickPrev(sideEnum.right)\" class=\"column cell prev available\">\r\n                                    </div>\r\n                                </ng-container>\r\n                                <ng-container\r\n                                        *ngIf=\"!((!calendarVariables.right.minDate || calendarVariables.right.minDate.isBefore(calendarVariables.right.calendar.firstDay)) && (!this.linkedCalendars))\">\r\n                                    <div class=\"column cell\"></div>\r\n                                </ng-container>\r\n                                <div class=\"column5 cell month\">\r\n                                    <ng-container *ngIf=\"showDropdowns && calendarVariables.right.dropdowns\">\r\n                                        <div class=\"dropdowns\">\r\n                                            {{this.locale.monthNames[calendarVariables?.right?.calendar[1][1].month()]}}\r\n                                            <select class=\"monthselect\" (change)=\"monthChanged($event, sideEnum.right)\">\r\n                                                <option\r\n                                                        [disabled]=\"(calendarVariables.right.dropdowns.inMinYear && m < calendarVariables.right.minDate.month()) || (calendarVariables.right.dropdowns.inMaxYear && m > calendarVariables.right.maxDate.month())\"\r\n                                                        *ngFor=\"let m of calendarVariables.right.dropdowns.monthArrays\"\r\n                                                        [value]=\"m\"\r\n                                                        [selected]=\"calendarVariables.right.dropdowns.currentMonth == m\">\r\n                                                    {{locale.monthNames[m]}}\r\n                                                </option>\r\n                                            </select>\r\n                                        </div>\r\n                                        <div class=\"dropdowns\">\r\n                                            {{ calendarVariables?.right?.calendar[1][1].format(\" YYYY\")}}\r\n                                            <select class=\"yearselect\" (change)=\"yearChanged($event, sideEnum.right)\">\r\n                                                <option *ngFor=\"let y of calendarVariables.right.dropdowns.yearArrays\"\r\n                                                        [selected]=\"y === calendarVariables.right.dropdowns.currentYear\">\r\n                                                    {{y}}\r\n                                                </option>\r\n                                            </select>\r\n                                        </div>\r\n                                    </ng-container>\r\n                                    <ng-container *ngIf=\"!showDropdowns || !calendarVariables.right.dropdowns\">\r\n                                        {{this.locale.monthNames[calendarVariables?.right?.calendar[1][1].month()]}}  {{ calendarVariables?.right?.calendar[1][1].format(\" YYYY\")}}\r\n                                    </ng-container>\r\n                                </div>\r\n                                <ng-container\r\n                                        *ngIf=\"!calendarVariables.right.maxDate || calendarVariables.right.maxDate.isAfter(calendarVariables.right.calendar.lastDay) && (!linkedCalendars || singleDatePicker || true)\">\r\n                                    <div class=\"column cell next available\" (click)=\"clickNext(sideEnum.right)\">\r\n                                    </div>\r\n                                </ng-container>\r\n                                <ng-container\r\n                                        *ngIf=\"!(!calendarVariables.right.maxDate || calendarVariables.right.maxDate.isAfter(calendarVariables.right.calendar.lastDay) && (!linkedCalendars || singleDatePicker || true))\">\r\n                                    <div class=\"column cell\"></div>\r\n                                </ng-container>\r\n                            </div>\r\n\r\n                            <div class=\"row\">\r\n                                <div *ngIf=\"showWeekNumbers || showISOWeekNumbers\" class=\"column cell week\">\r\n                                    <span>{{this.locale.weekLabel}}</span></div>\r\n                                <div class=\"column cell\" *ngFor=\"let dayofweek of locale.daysOfWeek\">\r\n                                    <span>{{dayofweek}}</span></div>\r\n                            </div>\r\n                            <div class=\"row\" *ngFor=\"let row of calendarVariables.right.calRows\"\r\n                                 [class]=\"calendarVariables.right.classes[row].classList\">\r\n                                <div class=\"column cell week\" *ngIf=\"showWeekNumbers\">\r\n                                    `<span>{{calendarVariables.right.calendar[row][0].week()}}</span>\r\n                                </div>\r\n                                <div class=\"column cell week\" *ngIf=\"showISOWeekNumbers\">\r\n                                    <span>{{calendarVariables.right.calendar[row][0].isoWeek()}}</span>\r\n                                </div>\r\n                                <div *ngFor=\"let col of calendarVariables.right.calCols\"\r\n                                     [class]=\"calendarVariables.right.classes[row][col]\"\r\n                                     (click)=\"clickDate($event, sideEnum.right, row, col)\">\r\n                                    <span>{{calendarVariables.right.calendar[row][col].date()}}</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"calendar-time\" *ngIf=\"timePicker\">\r\n                        <div class=\"select\">\r\n                            <select class=\"select-item hourselect\" [disabled]=\"!endDate\"\r\n                                    [(ngModel)]=\"timepickerVariables.right.selectedHour\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.right)\">\r\n                                <option *ngFor=\"let i of timepickerVariables.right.hours\"\r\n                                        [value]=\"i\"\r\n                                        [disabled]=\"timepickerVariables.right.disabledHours.indexOf(i) > -1\">{{i}}</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                        <div class=\"select\">\r\n                            <select class=\"select-item minuteselect\" [disabled]=\"!endDate\"\r\n                                    [(ngModel)]=\"timepickerVariables.right.selectedMinute\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.right)\">\r\n                                <option *ngFor=\"let i of timepickerVariables.right.minutes; let index = index;\"\r\n                                        [value]=\"i\"\r\n                                        [disabled]=\"timepickerVariables.right.disabledMinutes.indexOf(i) > -1\">{{timepickerVariables.right.minutesLabel[index]}}</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                        <div class=\"select\">\r\n                            <select *ngIf=\"timePickerSeconds\" class=\"select-item secondselect\" [disabled]=\"!endDate\"\r\n                                    [(ngModel)]=\"timepickerVariables.right.selectedSecond\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.right)\">\r\n                                <option *ngFor=\"let i of timepickerVariables.right.seconds; let index = index;\"\r\n                                        [value]=\"i\"\r\n                                        [disabled]=\"timepickerVariables.right.disabledSeconds.indexOf(i) > -1\">{{timepickerVariables.right.secondsLabel[index]}}</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                        <div class=\"select\">\r\n                            <select *ngIf=\"!timePicker24Hour\" class=\"select-item ampmselect\"\r\n                                    [(ngModel)]=\"timepickerVariables.right.ampmModel\"\r\n                                    (ngModelChange)=\"timeChanged($event, sideEnum.right)\">\r\n                                <option value=\"AM\" [disabled]=\"timepickerVariables.right.amDisabled\">AM</option>\r\n                                <option value=\"PM\" [disabled]=\"timepickerVariables.right.pmDisabled\">PM</option>\r\n                            </select>\r\n                            <span class=\"select-highlight\"></span>\r\n                            <span class=\"select-bar\"></span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div fxFlex=\"100\">\r\n                    <mat-card-actions fxLayoutAlign=\"end\"\r\n                                      *ngIf=\"!autoApply && ( !rangesArray.length || (showCalInRanges && !singleDatePicker))\">\r\n                        <button *ngIf=\"showClearButton\" mat-icon-button type=\"button\" color=\"warn\" (click)=\"clear()\"\r\n                                title=\"clear the date\">\r\n                            <mat-icon>delete</mat-icon>\r\n                        </button>\r\n                        <button mat-button *ngIf=\"showCancel\" type=\"button\"\r\n                                (click)=\"clickCancel($event)\">{{locale.cancelLabel}}</button>\r\n                        <button mat-raised-button color=\"primary\" [disabled]=\"applyBtn.disabled\" type=\"button\"\r\n                                (click)=\"clickApply($event)\">{{locale.applyLabel}}</button>\r\n                    </mat-card-actions>\r\n                </div>\r\n            </div>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</div>\r\n",
        host: {
            '(click)': 'handleInternalClick($event)',
        },
        encapsulation: ViewEncapsulation.None,
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => DateRangePicker_1),
                multi: true
            }],
        styles: [".md-drppicker{position:absolute;font-family:Roboto,sans-serif;color:inherit;border-radius:4px;width:600px!important;padding:0;margin-top:-10px;overflow:hidden;z-index:1000;font-size:14px;background-color:#fff;box-shadow:0 2px 4px 0 rgba(0,0,0,.16),0 2px 8px 0 rgba(0,0,0,.12)}.md-drppicker .row{display:flex;flex-direction:row;flex-wrap:wrap;width:100%}.md-drppicker .column{display:flex;flex-direction:column;flex-basis:100%;flex:1}.md-drppicker .column5{display:flex;flex-direction:row;flex-basis:100%;flex:5}.md-drppicker.double{width:auto}.md-drppicker.inline{position:relative;display:inline-block}.md-drppicker:after,.md-drppicker:before{position:absolute;display:inline-block;border-bottom-color:rgba(0,0,0,.2);content:''}.md-drppicker.openscenter:after,.md-drppicker.openscenter:before{left:0;right:0;width:0;margin-left:auto;margin-right:auto}.md-drppicker.single .calendar,.md-drppicker.single .ranges{float:none}.md-drppicker.shown{transform:scale(1);transition:.1s ease-in-out;transform-origin:0 0;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.md-drppicker.shown.drops-up-left{transform-origin:100% 100%}.md-drppicker.shown.drops-up-right{transform-origin:0 100%}.md-drppicker.shown.drops-down-left{transform-origin:100% 0}.md-drppicker.shown.drops-down-right{transform-origin:0 0}.md-drppicker.shown.drops-down-center{transform-origin:NaN}.md-drppicker.shown.drops-up-center{transform-origin:50%}.md-drppicker.shown .calendar{display:block}.md-drppicker.hidden{transition:.1s;transform:scale(0);transform-origin:0 0;cursor:default;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.md-drppicker.hidden.drops-up-left{transform-origin:100% 100%}.md-drppicker.hidden.drops-up-right{transform-origin:0 100%}.md-drppicker.hidden.drops-down-left{transform-origin:100% 0}.md-drppicker.hidden.drops-down-right{transform-origin:0 0}.md-drppicker.hidden.drops-down-center{transform-origin:NaN}.md-drppicker.hidden.drops-up-center{transform-origin:50%}.md-drppicker.hidden .calendar{display:none}.md-drppicker .calendar{margin:4px}.md-drppicker .calendar.single .calendar-table{border:none}.md-drppicker .calendar div{padding:0;white-space:nowrap;text-align:center;min-width:32px}.md-drppicker .calendar div span{pointer-events:none}.md-drppicker .calendar-table{border:1px solid #fff;padding:4px;border-radius:4px;background-color:#fff}.md-drppicker table{width:100%;margin:0}.md-drppicker div.header{color:#988c8c}.md-drppicker div.cell{text-align:center;border:1px solid transparent;white-space:nowrap;cursor:pointer;width:2em;margin:.25em 0;opacity:.8;transition:450ms cubic-bezier(.23,1,.32,1);border-radius:2em;transform:scale(1)}.md-drppicker div.cell.available.prev{display:block;background-image:url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgMy43IDYiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDMuNyA2IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGQ9Ik0zLjcsMC43TDEuNCwzbDIuMywyLjNMMyw2TDAsM2wzLTNMMy43LDAuN3oiLz4NCjwvZz4NCjwvc3ZnPg0K);background-repeat:no-repeat;background-size:.5em;background-position:center;opacity:.8;transition:background-color .2s;border-radius:2em}.md-drppicker div.cell.available.prev:hover{margin:0}.md-drppicker div.cell.available.next{transform:rotate(180deg);display:block;background-image:url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgMy43IDYiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDMuNyA2IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGQ9Ik0zLjcsMC43TDEuNCwzbDIuMywyLjNMMyw2TDAsM2wzLTNMMy43LDAuN3oiLz4NCjwvZz4NCjwvc3ZnPg0K);background-repeat:no-repeat;background-size:.5em;background-position:center;opacity:.8;transition:background-color .2s;border-radius:2em}.md-drppicker div.cell.available.next:hover{margin:0;transform:rotate(180deg)}.md-drppicker div.cell.available:hover{background-color:#eee;border-color:transparent;color:inherit;background-repeat:no-repeat;background-size:.5em;background-position:center;margin:.25em 0;opacity:.8;border-radius:2em;transform:scale(1);transition:450ms cubic-bezier(.23,1,.32,1)}.md-drppicker div.cell.week{font-size:80%;color:#ccc}.md-drppicker div.cell.off,.md-drppicker div.cell.off.end-date,.md-drppicker div.cell.off.in-range,.md-drppicker div.cell.off.start-date{background-color:#fff;border-color:transparent;color:#999}.md-drppicker div.cell.in-range{background-color:#dde2e4;border-color:transparent;color:#000;border-radius:0}.md-drppicker div.cell.start-date{border-radius:2em 0 0 2em}.md-drppicker div.cell.end-date{border-radius:0 2em 2em 0}.md-drppicker div.cell.start-date.end-date{border-radius:4px}.md-drppicker div.cell.active{transition:background .3s ease-out;background:rgba(0,0,0,.1)}.md-drppicker div.cell.active,.md-drppicker div.cell.active:hover{background-color:#3f51b5;border-color:transparent;color:#fff}.md-drppicker div.month{width:auto}.md-drppicker div.disabled,.md-drppicker option.disabled{color:#999;cursor:not-allowed;text-decoration:line-through}.md-drppicker .dropdowns{background-repeat:no-repeat;background-size:10px;background-position-y:center;background-position-x:right;width:50px;background-image:url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDI1NSAyNTUiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDI1NSAyNTU7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8ZyBpZD0iYXJyb3ctZHJvcC1kb3duIj4KCQk8cG9seWdvbiBwb2ludHM9IjAsNjMuNzUgMTI3LjUsMTkxLjI1IDI1NSw2My43NSAgICIgZmlsbD0iIzk4OGM4YyIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=)}.md-drppicker .dropdowns select{display:inline-block;background-color:rgba(255,255,255,.9);width:100%;padding:5px;border:1px solid #f2f2f2;border-radius:2px;height:3rem}.md-drppicker .dropdowns select.ampmselect,.md-drppicker .dropdowns select.hourselect,.md-drppicker .dropdowns select.minuteselect,.md-drppicker .dropdowns select.secondselect{width:50px;margin:0 auto;background:#eee;border:1px solid #eee;padding:2px;outline:0;font-size:12px}.md-drppicker .dropdowns select.monthselect,.md-drppicker .dropdowns select.yearselect{font-size:12px;height:auto;cursor:pointer;opacity:0;position:absolute;top:0;left:0;margin:0;padding:0}.md-drppicker div.month>div{position:relative;display:inline-block}.md-drppicker .calendar-time{text-align:center;margin:4px auto 0;line-height:30px;position:relative}.md-drppicker .calendar-time .select{display:inline}.md-drppicker .calendar-time .select .select-item{display:inline-block;width:auto;position:relative;font-family:inherit;background-color:transparent;padding:10px 10px 10px 0;font-size:18px;border-radius:0;border:none;border-bottom:1px solid rgba(0,0,0,.12)}.md-drppicker .calendar-time .select .select-item:after{position:absolute;top:18px;right:10px;width:0;height:0;padding:0;content:'';border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid rgba(0,0,0,.12);pointer-events:none}.md-drppicker .calendar-time .select .select-item:focus{outline:0}.md-drppicker .calendar-time .select .select-item .select-label{color:rgba(0,0,0,.26);font-size:16px;font-weight:400;position:absolute;pointer-events:none;left:0;top:10px;transition:.2s}.md-drppicker .calendar-time select.disabled{color:#ccc;cursor:not-allowed}.md-drppicker .label-input{border:1px solid #ccc;border-radius:4px;color:#555;height:30px;line-height:30px;display:block;vertical-align:middle;margin:0 auto 5px;padding:0 0 0 28px;width:100%}.md-drppicker .label-input.active{border:1px solid #08c;border-radius:4px}.md-drppicker .md-drppicker_input{position:relative;padding:0 30px 0 0}.md-drppicker .md-drppicker_input i,.md-drppicker .md-drppicker_input svg{position:absolute;left:8px;top:8px}.md-drppicker.rtl .label-input{padding-right:28px;padding-left:6px}.md-drppicker.rtl .md-drppicker_input i,.md-drppicker.rtl .md-drppicker_input svg{left:auto;right:8px}.md-drppicker .ranges ul{list-style:none;margin:0 auto;padding:0;width:100%}.md-drppicker .ranges ul li{font-size:12px}.md-drppicker .ranges ul li button{background:0 0;border:none;text-align:left;cursor:pointer}.md-drppicker .ranges ul li button.active{background-color:#3f51b5;color:#fff}.md-drppicker .ranges ul li button[disabled]{opacity:.3}.md-drppicker .ranges ul li button:active{background:0 0}.md-drppicker .ranges ul li:hover{background-color:#eee}.md-drppicker .show-calendar .ranges{margin-top:8px}.md-drppicker [hidden]{display:none}@media (min-width:680px){.md-drppicker{min-width:600px}.md-drppicker.single .calendar.left{clear:none}.md-drppicker.ltr{direction:ltr;text-align:left}.md-drppicker.ltr .calendar.left{clear:left}.md-drppicker.ltr .calendar.left .calendar-table{border-right:none;border-top-right-radius:0;border-bottom-right-radius:0;padding-right:12px}.md-drppicker.ltr .calendar.right{margin-left:0}.md-drppicker.ltr .calendar.right .calendar-table{border-left:none;border-top-left-radius:0;border-bottom-left-radius:0}.md-drppicker.ltr .left .md-drppicker_input,.md-drppicker.ltr .right .md-drppicker_input{padding-right:35px}.md-drppicker.ltr .calendar,.md-drppicker.ltr .ranges{float:left}.md-drppicker.rtl{direction:rtl;text-align:right}.md-drppicker.rtl .calendar.left{clear:right;margin-left:0}.md-drppicker.rtl .calendar.left .calendar-table{border-left:none;border-top-left-radius:0;border-bottom-left-radius:0}.md-drppicker.rtl .calendar.right{margin-right:0}.md-drppicker.rtl .calendar.right .calendar-table{border-right:none;border-top-right-radius:0;border-bottom-right-radius:0}.md-drppicker.rtl .calendar.left .calendar-table,.md-drppicker.rtl .left .md-drppicker_input{padding-left:12px}.md-drppicker.rtl .calendar,.md-drppicker.rtl .ranges{text-align:right;float:right}.drp-animate{transform:translate(0);transition:transform .2s,opacity .2s}.drp-animate.drp-picker-site-this{transition-timing-function:linear}.drp-animate.drp-animate-right{transform:translateX(10%);opacity:0}.drp-animate.drp-animate-left{transform:translateX(-10%);opacity:0}}@media (min-width:540px){.md-drppicker{min-width:400px}}"]
    }),
    __metadata("design:paramtypes", [ElementRef,
        ChangeDetectorRef,
        LocaleService])
], DateRangePicker);

var DateRangePickerDirective_1;
const moment$2 = _moment;
let DateRangePickerDirective = DateRangePickerDirective_1 = class DateRangePickerDirective {
    constructor(viewContainerRef, _changeDetectorRef, _componentFactoryResolver, _el, _renderer, differs, _localeService) {
        this.viewContainerRef = viewContainerRef;
        this._changeDetectorRef = _changeDetectorRef;
        this._componentFactoryResolver = _componentFactoryResolver;
        this._el = _el;
        this._renderer = _renderer;
        this.differs = differs;
        this._localeService = _localeService;
        this._onChange = Function.prototype;
        this._onTouched = Function.prototype;
        this._validatorChange = Function.prototype;
        this.dateLimit = null;
        this.showCancel = false;
        // timepicker variables
        this.timePicker = false;
        this.timePicker24Hour = false;
        this.timePickerIncrement = 1;
        this.timePickerSeconds = false;
        this._locale = {};
        this._endKey = 'endDate';
        this._startKey = 'startDate';
        this.notForChangesProperty = [
            'locale',
            'endKey',
            'startKey'
        ];
        this.onChange = new EventEmitter();
        this.rangeClicked = new EventEmitter();
        this.datesUpdated = new EventEmitter();
        this.drops = 'down';
        this.opens = 'right';
        const componentFactory = this._componentFactoryResolver.resolveComponentFactory(DateRangePicker);
        viewContainerRef.clear();
        const componentRef = viewContainerRef.createComponent(componentFactory);
        this.picker = componentRef.instance;
        this.picker.inline = false; // set inline to false for all directive usage
    }
    set locale(value) {
        this._locale = Object.assign({}, this._localeService.config, value);
    }
    get locale() {
        return this._locale;
    }
    set startKey(value) {
        if (value !== null) {
            this._startKey = value;
        }
        else {
            this._startKey = 'startDate';
        }
    }
    ;
    set endKey(value) {
        if (value !== null) {
            this._endKey = value;
        }
        else {
            this._endKey = 'endDate';
        }
    }
    ;
    get value() {
        return this._value || null;
    }
    set value(val) {
        this._value = val;
        this._onChange(val);
        this._changeDetectorRef.markForCheck();
    }
    ngOnInit() {
        this.picker.rangeClicked.asObservable().subscribe((range) => {
            this.rangeClicked.emit(range);
        });
        this.picker.datesUpdated.asObservable().subscribe((range) => {
            this.datesUpdated.emit(range);
        });
        this.picker.chosenDate.asObservable().subscribe((change) => {
            if (change) {
                const value = {};
                value[this._startKey] = change.startDate;
                value[this._endKey] = change.endDate;
                this.value = value;
                this.onChange.emit(value);
                if (typeof change.chosenLabel === 'string') {
                    this._el.nativeElement.value = change.chosenLabel;
                }
            }
        });
        this.picker.firstMonthDayClass = this.firstMonthDayClass;
        this.picker.lastMonthDayClass = this.lastMonthDayClass;
        this.picker.emptyWeekRowClass = this.emptyWeekRowClass;
        this.picker.firstDayOfNextMonthClass = this.firstDayOfNextMonthClass;
        this.picker.lastDayOfPreviousMonthClass = this.lastDayOfPreviousMonthClass;
        this.picker.drops = this.drops;
        this.picker.opens = this.opens;
        this.localeDiffer = this.differs.find(this.locale).create();
    }
    ngOnChanges(changes) {
        for (let change in changes) {
            if (changes.hasOwnProperty(change)) {
                if (this.notForChangesProperty.indexOf(change) === -1) {
                    this.picker[change] = changes[change].currentValue;
                }
            }
        }
    }
    ngDoCheck() {
        if (this.localeDiffer) {
            const changes = this.localeDiffer.diff(this.locale);
            if (changes) {
                this.picker.updateLocale(this.locale);
            }
        }
    }
    onBlur() {
        this._onTouched();
    }
    open(event) {
        this.picker.show(event);
        setTimeout(() => {
            this.setPosition();
        });
    }
    hide(e) {
        this.picker.hide(e);
    }
    toggle(e) {
        if (this.picker.isShown) {
            this.hide(e);
        }
        else {
            this.open(e);
        }
    }
    clear() {
        this.picker.clear();
    }
    writeValue(value) {
        this.value = value;
        this.setValue(value);
    }
    registerOnChange(fn) {
        this._onChange = fn;
    }
    registerOnTouched(fn) {
        this._onTouched = fn;
    }
    setValue(val) {
        if (val) {
            if (val[this._startKey]) {
                this.picker.setStartDate(val[this._startKey]);
            }
            if (val[this._endKey]) {
                this.picker.setEndDate(val[this._endKey]);
            }
            this.picker.calculateChosenLabel();
            if (this.picker.chosenLabel) {
                this._el.nativeElement.value = this.picker.chosenLabel;
            }
        }
        else {
            this.picker.clear();
        }
    }
    /**
     * Set position of the calendar
     */
    setPosition() {
        let style;
        let containerTop;
        const container = this.picker.pickerContainer.nativeElement;
        const element = this._el.nativeElement;
        if (this.drops && this.drops == 'up') {
            containerTop = (element.offsetTop - container.clientHeight) + 'px';
        }
        else {
            containerTop = 'auto';
        }
        if (this.opens == 'left') {
            style = {
                top: containerTop,
                left: (element.offsetLeft - container.clientWidth + element.clientWidth) + 'px',
                right: 'auto'
            };
        }
        else if (this.opens == 'center') {
            style = {
                top: containerTop,
                left: (element.offsetLeft + element.clientWidth / 2
                    - container.clientWidth / 2) + 'px',
                right: 'auto'
            };
        }
        else {
            style = {
                top: containerTop,
                left: element.offsetLeft + 'px',
                right: 'auto'
            };
        }
        if (style) {
            this._renderer.setStyle(container, 'top', style.top);
            this._renderer.setStyle(container, 'left', style.left);
            this._renderer.setStyle(container, 'right', style.right);
        }
    }
    /**
     * For click outside of the calendar's container
     * @param event event object
     * @param targetElement target element object
     */
    outsideClick(event, targetElement) {
        if (!targetElement) {
            return;
        }
        if (targetElement.classList.contains('ngx-daterangepicker-action')) {
            return;
        }
        const clickedInside = this._el.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.hide();
        }
    }
};
DateRangePickerDirective.ctorParameters = () => [
    { type: ViewContainerRef },
    { type: ChangeDetectorRef },
    { type: ComponentFactoryResolver },
    { type: ElementRef },
    { type: Renderer2 },
    { type: KeyValueDiffers },
    { type: LocaleService }
];
__decorate([
    Input(),
    __metadata("design:type", Object)
], DateRangePickerDirective.prototype, "minDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], DateRangePickerDirective.prototype, "maxDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "autoApply", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "alwaysShowCalendars", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "autoUpdateInput", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showCustomRangeLabel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "linkedCalendars", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], DateRangePickerDirective.prototype, "dateLimit", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "singleDatePicker", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showWeekNumbers", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showISOWeekNumbers", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showDropdowns", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function)
], DateRangePickerDirective.prototype, "isInvalidDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Function)
], DateRangePickerDirective.prototype, "isCustomDate", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showClearButton", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], DateRangePickerDirective.prototype, "ranges", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "opens", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "drops", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "lastMonthDayClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "emptyWeekRowClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "firstDayOfNextMonthClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "lastDayOfPreviousMonthClass", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "keepCalendarOpeningWithRange", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showRangeLabelOnInput", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "showCancel", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "timePicker", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "timePicker24Hour", void 0);
__decorate([
    Input(),
    __metadata("design:type", Number)
], DateRangePickerDirective.prototype, "timePickerIncrement", void 0);
__decorate([
    Input(),
    __metadata("design:type", Boolean)
], DateRangePickerDirective.prototype, "timePickerSeconds", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], DateRangePickerDirective.prototype, "locale", null);
__decorate([
    Input(),
    __metadata("design:type", String)
], DateRangePickerDirective.prototype, "_endKey", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], DateRangePickerDirective.prototype, "startKey", null);
__decorate([
    Input(),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], DateRangePickerDirective.prototype, "endKey", null);
__decorate([
    Output('change'),
    __metadata("design:type", EventEmitter)
], DateRangePickerDirective.prototype, "onChange", void 0);
__decorate([
    Output('rangeClicked'),
    __metadata("design:type", EventEmitter)
], DateRangePickerDirective.prototype, "rangeClicked", void 0);
__decorate([
    Output('datesUpdated'),
    __metadata("design:type", EventEmitter)
], DateRangePickerDirective.prototype, "datesUpdated", void 0);
__decorate([
    HostListener('document:click', ['$event', '$event.target']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, HTMLElement]),
    __metadata("design:returntype", void 0)
], DateRangePickerDirective.prototype, "outsideClick", null);
DateRangePickerDirective = DateRangePickerDirective_1 = __decorate([
    Directive({
        selector: 'input[dateRangePicker]',
        host: {
            '(keyup.esc)': 'hide()',
            '(blur)': 'onBlur()',
            '(click)': 'open()'
        },
        providers: [
            {
                provide: NG_VALUE_ACCESSOR,
                useExisting: forwardRef(() => DateRangePickerDirective_1), multi: true
            }
        ]
    }),
    __metadata("design:paramtypes", [ViewContainerRef,
        ChangeDetectorRef,
        ComponentFactoryResolver,
        ElementRef,
        Renderer2,
        KeyValueDiffers,
        LocaleService])
], DateRangePickerDirective);

let JsonFormComponent = class JsonFormComponent {
    constructor(templateService) {
        this.templateService = templateService;
        this.dataEvent = new EventEmitter(true);
        this.form = {};
        this.isValid = false;
    }
    ngOnInit() {
        this.templateService.getTemplate(this.template).subscribe((json) => {
            this.form = json.template;
        });
    }
    reset() {
        this.model = this._defaultValue;
    }
    change(event) {
        this.isValid = event.isValid;
        if (this.isValid) {
            this.dataEvent.emit(event.data);
        }
    }
    ngOnChanges(changes) {
        if (changes['model']) {
            this.model = changes['model'];
            this._defaultValue = changes['model'];
            this.formio.submission = this.model;
        }
    }
};
JsonFormComponent.ctorParameters = () => [
    { type: LayoutTemplateService }
];
__decorate([
    ViewChild(FormioComponent, { static: true }),
    __metadata("design:type", FormioComponent)
], JsonFormComponent.prototype, "formio", void 0);
__decorate([
    Input(),
    __metadata("design:type", String)
], JsonFormComponent.prototype, "template", void 0);
__decorate([
    Input(),
    __metadata("design:type", Object)
], JsonFormComponent.prototype, "model", void 0);
__decorate([
    Output(),
    __metadata("design:type", EventEmitter)
], JsonFormComponent.prototype, "dataEvent", void 0);
JsonFormComponent = __decorate([
    Component({
        selector: 'json-form',
        template: `
        <mat-formio [form]="form"
                    [submission]="model"
                    [options]=""
                    (change)="change($event)">
        </mat-formio>
    `
    }),
    __metadata("design:paramtypes", [LayoutTemplateService])
], JsonFormComponent);

let JsonFormModule = class JsonFormModule {
};
JsonFormModule = __decorate([
    NgModule({
        imports: [MatFormioModule],
        declarations: [JsonFormComponent],
        exports: [JsonFormComponent, MatFormioModule],
        entryComponents: [JsonFormComponent]
    })
], JsonFormModule);

let LamisSharedModule = class LamisSharedModule {
};
LamisSharedModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MatButtonModule,
            MatSelectModule,
            MatIconModule,
            MatTooltipModule,
            RouterModule,
            SharedCommonModule,
            CardViewModule,
            FlexLayoutModule,
            MatDatepickerModule,
            MatCardModule,
            JsonFormModule
        ],
        declarations: [
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            DateRangePicker,
            DateRangePickerDirective
        ],
        entryComponents: [
            DateRangePicker
        ],
        exports: [
            JsonFormModule,
            SharedCommonModule,
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            CardViewModule,
            DateRangePicker,
            DateRangePickerDirective
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
], LamisSharedModule);

var FacilityType;
(function (FacilityType) {
    FacilityType[FacilityType["DOCTOR_OFFICE"] = 0] = "DOCTOR_OFFICE";
    FacilityType[FacilityType["PRIMARY_CARE"] = 1] = "PRIMARY_CARE";
    FacilityType[FacilityType["CLINIC"] = 2] = "CLINIC";
    FacilityType[FacilityType["HOSPITAL"] = 3] = "HOSPITAL";
    FacilityType[FacilityType["SPECIALIZED"] = 4] = "SPECIALIZED";
    FacilityType[FacilityType["NURSING_HOME"] = 5] = "NURSING_HOME";
    FacilityType[FacilityType["HOSPICE"] = 6] = "HOSPICE";
    FacilityType[FacilityType["RURAL"] = 7] = "RURAL";
})(FacilityType || (FacilityType = {}));
var PublicLevel;
(function (PublicLevel) {
    PublicLevel[PublicLevel["PRIVATE"] = 0] = "PRIVATE";
    PublicLevel[PublicLevel["PUBLIC"] = 1] = "PUBLIC";
    PublicLevel[PublicLevel["MIXED"] = 2] = "MIXED";
})(PublicLevel || (PublicLevel = {}));

class LGA {
    constructor(id, name, state) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.state = state ? state : null;
    }
}

class State {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.id = id ? id : null;
        this.name = name ? name : null;
    }
}

class Aggregate {
    constructor(field, key) {
        this.field = field;
        this.key = key;
    }
}
function remove(array, element) {
    return array.filter(e => e.id !== element.id);
}
function clear(array) {
    array.length = 0;
    return array;
}
function contains(array, element) {
    return array.filter(e => e.id === element.id).length > 0;
}
function entityCompare(e1, e2) {
    return e1 && e2 ? e1.id == e2.id : e1 === e2;
}
function enumCompare(e1, e2) {
    return (e1 !== undefined && e2 !== undefined) ? e1.valueOf() == e2.valueOf() : e1 === e2;
}
function replace(array, element) {
    let result = remove(array, element);
    result.push(element);
    return result;
}

const source = 'http://' + window.location.host + '/websocket';
const ɵ0 = () => {
    return new SockJS('' + source);
};
const RxStompConfig = {
    // Which server?
    brokerURL: '' + source,
    webSocketFactory: ɵ0,
    // Headers
    // Typical keys: login, passcode, host
    connectHeaders: {
        login: 'guest',
        passcode: 'guest'
    },
    // How often to heartbeat?
    // Interval in milliseconds, set to 0 to disable
    heartbeatIncoming: 0,
    // Typical value 0 - disabled
    heartbeatOutgoing: 20000,
};

var LamisCoreModule_1;
let LamisCoreModule = LamisCoreModule_1 = class LamisCoreModule {
    static forRoot(serverApiUrlConfig, config) {
        return {
            ngModule: LamisCoreModule_1,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                { provide: LOCALE_CONFIG, useValue: config || {} },
                { provide: LocaleService, useClass: LocaleService, deps: [LOCALE_CONFIG] },
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    }
};
LamisCoreModule = LamisCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            LamisSharedModule
        ],
        exports: [
            LamisSharedModule
        ],
        providers: []
    })
], LamisCoreModule);

class BaseAggregatingComponent {
    constructor(router, activatedRoute, notification, media) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.notification = notification;
        this.media = media;
        this.filter = [];
        this.totalItems = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.selectedAggregates = [];
        this.sortOrder = 'asc';
        this.sortBy = 'id';
    }
    ngOnInit() {
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            if (data['pagingParams']) {
                this.page = data['pagingParams'].page;
                this.previousPage = data['pagingParams'].page;
                this.currentSearch = data['pagingParams'].query;
                this.reverse = data['pagingParams'].ascending;
                this.predicate = data['pagingParams'].predicate;
                if (data['pagingParams'].filter) {
                    this.filter = typeof data['pagingParams'].filter === 'string' ?
                        [data['pagingParams'].filter] : data['pagingParams'].filter;
                }
            }
        });
        this.transition();
    }
    searchEntities(keyword) {
        this.selectedAggregates = [];
        this.currentSearch = keyword;
        this.transition();
    }
    entitySelected(event) {
        let row = event.obj;
        this.router.navigate(['..', this.getPath(), row.id, 'view'], { relativeTo: this.activatedRoute });
    }
    getSort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }
    sort(sortEvent) {
        this.sortBy = sortEvent.key;
        this.sortOrder = sortEvent.direction;
        this.predicate = this.sortBy;
        this.reverse = this.sortOrder === 'asc';
        this.transition();
    }
    changed() {
        this.previousPage = 1;
        this.page = 1;
        this.transition();
    }
    changeLinks(event) {
        this.page = event;
        this.loadPage(event);
    }
    loadPage(page) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        let params = {};
        if ((this.page) || (this.currentSearch)) {
            params.page = this.page;
        }
        if (this.currentSearch) {
            params.query = this.currentSearch;
        }
        if (this.predicate && this.predicate !== 'id') {
            params.sort = this.predicate + ',' + (this.reverse ? 'asc' : 'desc');
        }
        if (this.filter) {
            params.filter = this.filter;
        }
        this.router.navigate(['..', this.getPath()], { relativeTo: this.activatedRoute, queryParams: params });
        this.loadAll();
    }
    loadAll() {
        const aggs = [];
        if (this.filter) {
            if (this.selectedAggregates.length == 0) {
                this.filter.forEach((filter) => {
                    let parts = filter.split(':');
                    this.selectedAggregates.push({ field: parts[0], key: parts[1] });
                });
            }
        }
        if (this.selectedAggregates) {
            this.selectedAggregates.forEach((agg) => {
                aggs.push(agg);
            });
        }
        this.getService().search({
            query: this.currentSearch,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.getSort(),
            aggs: this.selectedAggregates
        }).subscribe((res) => this.onSuccess(res.body, res.headers), (res) => this.onError(res.statusText));
    }
    onSuccess(data, headers) {
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.rawEntities = data;
        this.entities = new ObjectDataTableAdapter(data);
        let result = this.buildMap(JSON.parse(headers.get('aggregates')));
        let map = new Map();
        result.forEach((entryVal, entryKey) => {
            let aggs = [];
            if (this.selectedAggregates && this.selectedAggregates.length !== 0) {
                entryVal.forEach((entry) => {
                    let selectedAggregate = {};
                    this.selectedAggregates.filter((e) => {
                        if (e.key == entry.key) {
                            selectedAggregate = e;
                        }
                    });
                    if (entry.field == selectedAggregate.field && entry.key == selectedAggregate.key) {
                        aggs.push({
                            field: entry.field,
                            key: entry.key,
                            count: entry.count,
                            selected: true
                        });
                    }
                    else {
                        aggs.push({
                            field: entry.field,
                            key: entry.key,
                            count: entry.count,
                            selected: false
                        });
                    }
                });
            }
            else {
                entryVal.forEach((entry) => {
                    aggs.push({
                        field: entry.field,
                        key: entry.key,
                        count: entry.count,
                        selected: false
                    });
                });
            }
            map.set(entryKey, aggs);
        });
        this.aggregates = map;
        this.querySuccess();
    }
    onError(error) {
        this.notification.openSnackMessage(error.message);
    }
    addFilter(field, val) {
        this.aggregateSelected(field, val, true);
    }
    removeFilter(field, val) {
        this.aggregateSelected(field, val, false);
    }
    aggregateSelected(field, val, state) {
        const selection = { 'field': field, 'key': val };
        if (state) {
            this.selectedAggregates.push(selection);
        }
        else {
            let found = false;
            this.selectedAggregates.filter(e => {
                if (e.field === selection.field && e.key === selection.key) {
                    found = true;
                }
            });
            if (found) {
                this.selectedAggregates = this.selectedAggregates
                    .filter(e => !(e.field === selection.field && e.key === selection.key));
            }
        }
        this.filter = [];
        this.selectedAggregates.forEach((agg) => {
            this.filter.push(agg.field + ':' + agg.key);
        });
        this.transition();
    }
    buildMap(obj) {
        let map = new Map();
        Object.keys(obj).forEach(key => {
            map.set(key, obj[key]);
        });
        return map;
    }
}

class BaseEntityEditComponent {
    constructor(notification, route) {
        this.notification = notification;
        this.route = route;
        this.error = false;
    }
    ngOnInit() {
        this.isSaving = false;
        this.route.data.subscribe(({ entity }) => {
            this.entity = !!entity && entity.body ? entity.body : entity;
        });
        if (this.entity === undefined) {
            this.entity = this.createEntity();
        }
    }
    previousState() {
        window.history.back();
    }
    save(valid) {
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
        this.isSaving = true;
        if (this.entity.id !== undefined) {
            this.subscribeToSaveResponse(this.getService().update(this.entity));
        }
        else {
            this.subscribeToSaveResponse(this.getService().create(this.entity));
        }
    }
    subscribeToSaveResponse(result) {
        result.subscribe((res) => this.onSaveSuccess(res.body), (res) => {
            this.onSaveError();
            this.onError(res.message);
        });
    }
    onSaveSuccess(result) {
        this.isSaving = false;
        this.previousState();
    }
    onSaveError() {
        this.isSaving = false;
        this.error = true;
        this.submitButton.disabled = true;
        this.progressBar.mode = 'determinate';
    }
    onError(errorMessage) {
        this.notification.openSnackMessage(errorMessage);
    }
    entityCompare(s1, s2) {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    }
}
__decorate([
    ViewChild(MatProgressBar, { static: true }),
    __metadata("design:type", MatProgressBar)
], BaseEntityEditComponent.prototype, "progressBar", void 0);
__decorate([
    ViewChild(MatButton, { static: true }),
    __metadata("design:type", MatButton)
], BaseEntityEditComponent.prototype, "submitButton", void 0);

let PagingParamsResolve = class PagingParamsResolve {
    constructor(injector) {
        this.injector = injector;
        this.paginationUtil = this.injector.get(JhiPaginationUtil);
    }
    resolve(route, state) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const query = route.queryParams['query'] ? route.queryParams['query'] : '';
        const filter = route.queryParams['filter'] ? route.queryParams['filter'] : '';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: +page,
            query: query,
            filter: filter,
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
};
PagingParamsResolve.ctorParameters = () => [
    { type: Injector }
];
PagingParamsResolve.ngInjectableDef = ɵɵdefineInjectable({ factory: function PagingParamsResolve_Factory() { return new PagingParamsResolve(ɵɵinject(INJECTOR)); }, token: PagingParamsResolve, providedIn: "root" });
PagingParamsResolve = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __metadata("design:paramtypes", [Injector])
], PagingParamsResolve);

class CardViewItemAddressValidator {
    constructor() {
        this.message = '';
    }
    isValid(value) {
        let streetError = false;
        let cityError = false;
        let lgaError = false;
        if (!value.street1 || value.street1.length < 3) {
            streetError = true;
        }
        if (!value.city || value.city.length < 3) {
            cityError = true;
        }
        if (!!value.lga) {
            lgaError = true;
        }
        if (streetError && cityError && lgaError) {
            this.message = 'Street, city and LGA are all required';
        }
        else if (streetError && cityError) {
            this.message = 'Street and city are required';
        }
        else if (streetError && lgaError) {
            this.message = 'Street and LGA are required';
        }
        else if (lgaError && cityError) {
            this.message = 'City and LGA are required';
        }
        else if (streetError) {
            this.message = 'Street is required';
        }
        else if (cityError) {
            this.message = 'City is required';
        }
        else if (lgaError) {
            this.message = 'LGA is required';
        }
        return !(streetError || cityError || lgaError);
    }
}

class CardViewItemNameValidator {
    constructor() {
        this.message = '';
    }
    isValid(value) {
        let nameError = false;
        let surnameError = false;
        if (!value.firstName || value.firstName.length < 3) {
            nameError = true;
        }
        if (!value.surname || value.surname.length < 3) {
            surnameError = true;
        }
        if (nameError && surnameError) {
            this.message = 'Both first name and surname are required';
        }
        else if (nameError) {
            this.message = 'First name is required';
        }
        else if (surnameError) {
            this.message = 'Surname is required';
        }
        return !(nameError || surnameError);
    }
}

/*
 * Public API Surface of LAMIS Web Core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { Account, AccountService, Address, Aggregate, AlertComponent, AlertErrorComponent, AuthExpiredInterceptor, AuthInterceptor, AuthServerProvider, BaseAggregatingComponent, BaseEntityEditComponent, CardViewAddressItemComponent, CardViewAddressItemModel, CardViewFixedKeyValuePairsItemModel, CardViewFixedKeyvaluepairsitemComponent, CardViewHtmlTextItemComponent, CardViewHtmlTextItemModel, CardViewItemAddressValidator, CardViewItemNameValidator, CardViewModule, CardViewNameItemComponent, CardViewNameItemModel, CardViewPhoneItemComponent, CardViewPhoneItemModel, CommonPipesModule, CoreModule, DATE_FORMAT, DATE_TIME_FORMAT, DateRangePicker, DateRangePickerDirective, DefaultLocaleConfig, DetailsComponent, EMAIL_ALREADY_USED_TYPE, EMAIL_NOT_FOUND_TYPE, ErrorHandlerInterceptor, ExcerptPipe, FacilityService, FacilityType, FieldType, HasAnyAuthorityDirective, ITEMS_PER_PAGE, JsonFormComponent, JsonFormModule, KeysPipe, LGA, LOCALE_CONFIG, LOGIN_ALREADY_USED_TYPE, LamisCoreModule, LamisSharedModule, LgaService, LocaleService, LoginAuthenticationService, LoginService, MapValuesPipe, MenuService, NairaPipe, NotificationInterceptor, PROBLEM_BASE_URL, PagingParamsResolve, PersonName, Phone, PublicLevel, RelativeTimePipe, SERVER_API_URL_CONFIG, SharedCommonModule, SideEnum, SpeedDialFabComponent, State, StateService, StateStorageService, User, UserRouteAccessService, UserService, WindowRef, clear, components, contains, createRequestOption, entityCompare, enumCompare, remove, replace, speedDialFabAnimations, LayoutTemplateService as ɵa, RxStompConfig as ɵb };
//# sourceMappingURL=lamis-web-core.js.map
