import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../app.constants";
let MenuService = class MenuService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
    }
    getMenus() {
        return this.http.get(this.serverUrl.SERVER_API_URL + 'api/modules/menus', {});
    }
};
MenuService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
MenuService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function MenuService_Factory() { return new MenuService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: MenuService, providedIn: "root" });
MenuService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG)),
    tslib_1.__metadata("design:paramtypes", [HttpClient, Object])
], MenuService);
export { MenuService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiY29yZS9tZW51L21lbnUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxxQkFBcUIsRUFBc0IsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQU1oRixJQUFhLFdBQVcsR0FBeEIsTUFBYSxXQUFXO0lBRXZCLFlBQW9CLElBQWdCLEVBQXlDLFNBQTZCO1FBQXRGLFNBQUksR0FBSixJQUFJLENBQVk7UUFBeUMsY0FBUyxHQUFULFNBQVMsQ0FBb0I7SUFDMUcsQ0FBQztJQUVNLFFBQVE7UUFDZCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFjLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLG1CQUFtQixFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzVGLENBQUM7Q0FDRCxDQUFBOztZQU4wQixVQUFVOzRDQUFHLE1BQU0sU0FBQyxxQkFBcUI7OztBQUZ2RCxXQUFXO0lBSHZCLFVBQVUsQ0FBQztRQUNYLFVBQVUsRUFBRSxNQUFNO0tBQ2xCLENBQUM7SUFHc0MsbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7NkNBQTFDLFVBQVU7R0FGeEIsV0FBVyxDQVF2QjtTQVJZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkxfQ09ORklHLCBTZXJ2ZXJBcGlVcmxDb25maWcgfSBmcm9tICcuLi8uLi9hcHAuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgSU1lbnVJdGVtIH0gZnJvbSAnLi4vLi4vc2hhcmVkL21vZGVsJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuXHRwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE1lbnVTZXJ2aWNlIHtcclxuXHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LCBASW5qZWN0KFNFUlZFUl9BUElfVVJMX0NPTkZJRykgcHJpdmF0ZSBzZXJ2ZXJVcmw6IFNlcnZlckFwaVVybENvbmZpZykge1xyXG5cdH1cclxuXHJcblx0cHVibGljIGdldE1lbnVzKCkge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQ8SU1lbnVJdGVtW10+KHRoaXMuc2VydmVyVXJsLlNFUlZFUl9BUElfVVJMICsgJ2FwaS9tb2R1bGVzL21lbnVzJywge30pO1xyXG5cdH1cclxufVxyXG4iXX0=