import * as tslib_1 from "tslib";
import { LOCALE_ID, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
let CoreModule = class CoreModule {
    constructor() {
        //registerLocaleData(locale);
    }
};
CoreModule = tslib_1.__decorate([
    NgModule({
        imports: [HttpClientModule],
        exports: [],
        declarations: [],
        providers: [
            Title,
            {
                provide: LOCALE_ID,
                useValue: 'en'
            },
            DatePipe
        ]
    }),
    tslib_1.__metadata("design:paramtypes", [])
], CoreModule);
export { CoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJjb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBZWxELElBQWEsVUFBVSxHQUF2QixNQUFhLFVBQVU7SUFDbkI7UUFDSSw2QkFBNkI7SUFDakMsQ0FBQztDQUNKLENBQUE7QUFKWSxVQUFVO0lBYnRCLFFBQVEsQ0FBQztRQUNOLE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO1FBQzNCLE9BQU8sRUFBRSxFQUFFO1FBQ1gsWUFBWSxFQUFFLEVBQUU7UUFDaEIsU0FBUyxFQUFFO1lBQ1AsS0FBSztZQUNMO2dCQUNJLE9BQU8sRUFBRSxTQUFTO2dCQUNsQixRQUFRLEVBQUUsSUFBSTthQUNqQjtZQUNELFFBQVE7U0FDWDtLQUNKLENBQUM7O0dBQ1csVUFBVSxDQUl0QjtTQUpZLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBMT0NBTEVfSUQsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgVGl0bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbSHR0cENsaWVudE1vZHVsZV0sXHJcbiAgICBleHBvcnRzOiBbXSxcclxuICAgIGRlY2xhcmF0aW9uczogW10sXHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBUaXRsZSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHByb3ZpZGU6IExPQ0FMRV9JRCxcclxuICAgICAgICAgICAgdXNlVmFsdWU6ICdlbidcclxuICAgICAgICB9LFxyXG4gICAgICAgIERhdGVQaXBlXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3JlTW9kdWxlIHtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIC8vcmVnaXN0ZXJMb2NhbGVEYXRhKGxvY2FsZSk7XHJcbiAgICB9XHJcbn1cclxuIl19