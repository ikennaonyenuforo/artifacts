import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { SessionStorageService } from 'ngx-store';
import * as i0 from "@angular/core";
let StateStorageService = class StateStorageService {
    constructor(injector) {
        this.injector = injector;
        this.$sessionStorage = injector.get(SessionStorageService);
    }
    getPreviousState() {
        return this.$sessionStorage.get('previousState');
    }
    resetPreviousState() {
        this.$sessionStorage.remove('previousState');
    }
    storePreviousState(previousStateName, previousStateParams) {
        const previousState = { name: previousStateName, params: previousStateParams };
        this.$sessionStorage.set('previousState', previousState);
    }
    getDestinationState() {
        return this.$sessionStorage.get('destinationState');
    }
    storeUrl(url) {
        this.$sessionStorage.set('previousUrl', url);
    }
    getUrl() {
        return this.$sessionStorage.get('previousUrl');
    }
    storeDestinationState(destinationState, destinationStateParams, fromState) {
        const destinationInfo = {
            destination: {
                name: destinationState.name,
                data: destinationState.data
            },
            params: destinationStateParams,
            from: {
                name: fromState.name
            }
        };
        this.$sessionStorage.set('destinationState', destinationInfo);
    }
};
StateStorageService.ctorParameters = () => [
    { type: Injector }
];
StateStorageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function StateStorageService_Factory() { return new StateStorageService(i0.ɵɵinject(i0.INJECTOR)); }, token: StateStorageService, providedIn: "root" });
StateStorageService = tslib_1.__decorate([
    Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [Injector])
], StateStorageService);
export { StateStorageService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtc3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiY29yZS9hdXRoL3N0YXRlLXN0b3JhZ2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckQsT0FBTyxFQUF1QixxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQzs7QUFHdkUsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFHNUIsWUFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQyxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGtCQUFrQixDQUFDLGlCQUFzQixFQUFFLG1CQUF3QjtRQUMvRCxNQUFNLGFBQWEsR0FBRyxFQUFDLElBQUksRUFBRSxpQkFBaUIsRUFBRSxNQUFNLEVBQUUsbUJBQW1CLEVBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVELG1CQUFtQjtRQUNmLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQsUUFBUSxDQUFDLEdBQVc7UUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxNQUFNO1FBQ0YsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQscUJBQXFCLENBQUMsZ0JBQXFCLEVBQUUsc0JBQTJCLEVBQUUsU0FBYztRQUNwRixNQUFNLGVBQWUsR0FBRztZQUNwQixXQUFXLEVBQUU7Z0JBQ1QsSUFBSSxFQUFFLGdCQUFnQixDQUFDLElBQUk7Z0JBQzNCLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJO2FBQzlCO1lBQ0QsTUFBTSxFQUFFLHNCQUFzQjtZQUM5QixJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJO2FBQ3ZCO1NBQ0osQ0FBQztRQUNGLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7Q0FDSixDQUFBOztZQTFDaUMsUUFBUTs7O0FBSDdCLG1CQUFtQjtJQUQvQixVQUFVLENBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDLENBQUM7NkNBSUMsUUFBUTtHQUg3QixtQkFBbUIsQ0E2Qy9CO1NBN0NZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IExvY2FsU3RvcmFnZVNlcnZpY2UsIFNlc3Npb25TdG9yYWdlU2VydmljZSB9IGZyb20gJ25neC1zdG9yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIFN0YXRlU3RvcmFnZVNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSAkc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGluamVjdG9yOiBJbmplY3Rvcikge1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlID0gaW5qZWN0b3IuZ2V0KFNlc3Npb25TdG9yYWdlU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UHJldmlvdXNTdGF0ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kc2Vzc2lvblN0b3JhZ2UuZ2V0KCdwcmV2aW91c1N0YXRlJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXRQcmV2aW91c1N0YXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnJlbW92ZSgncHJldmlvdXNTdGF0ZScpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlUHJldmlvdXNTdGF0ZShwcmV2aW91c1N0YXRlTmFtZTogYW55LCBwcmV2aW91c1N0YXRlUGFyYW1zOiBhbnkpIHtcclxuICAgICAgICBjb25zdCBwcmV2aW91c1N0YXRlID0ge25hbWU6IHByZXZpb3VzU3RhdGVOYW1lLCBwYXJhbXM6IHByZXZpb3VzU3RhdGVQYXJhbXN9O1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnNldCgncHJldmlvdXNTdGF0ZScsIHByZXZpb3VzU3RhdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERlc3RpbmF0aW9uU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJHNlc3Npb25TdG9yYWdlLmdldCgnZGVzdGluYXRpb25TdGF0ZScpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlVXJsKHVybDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy4kc2Vzc2lvblN0b3JhZ2Uuc2V0KCdwcmV2aW91c1VybCcsIHVybCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXJsKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRzZXNzaW9uU3RvcmFnZS5nZXQoJ3ByZXZpb3VzVXJsJyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RvcmVEZXN0aW5hdGlvblN0YXRlKGRlc3RpbmF0aW9uU3RhdGU6IGFueSwgZGVzdGluYXRpb25TdGF0ZVBhcmFtczogYW55LCBmcm9tU3RhdGU6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGRlc3RpbmF0aW9uSW5mbyA9IHtcclxuICAgICAgICAgICAgZGVzdGluYXRpb246IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IGRlc3RpbmF0aW9uU3RhdGUubmFtZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IGRlc3RpbmF0aW9uU3RhdGUuZGF0YVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwYXJhbXM6IGRlc3RpbmF0aW9uU3RhdGVQYXJhbXMsXHJcbiAgICAgICAgICAgIGZyb206IHtcclxuICAgICAgICAgICAgICAgIG5hbWU6IGZyb21TdGF0ZS5uYW1lXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnNldCgnZGVzdGluYXRpb25TdGF0ZScsIGRlc3RpbmF0aW9uSW5mbyk7XHJcbiAgICB9XHJcbn1cclxuIl19