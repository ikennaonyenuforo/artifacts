import * as tslib_1 from "tslib";
import { Inject, Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../app.constants";
let AuthServerProvider = class AuthServerProvider {
    constructor(http, injector, serverUrl) {
        this.http = http;
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    getToken() {
        return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
    }
    login(credentials) {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        let _this = this;
        return this.http.post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data, { observe: 'response' }).pipe(map(authenticateSuccess.bind(this)));
        function authenticateSuccess(resp) {
            const bearerToken = resp.headers.get('Authorization');
            if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
                const jwt = bearerToken.slice(7, bearerToken.length);
                _this.storeAuthenticationToken(jwt, credentials.rememberMe);
                return jwt;
            }
        }
    }
    loginWithToken(jwt, rememberMe) {
        if (jwt) {
            this.storeAuthenticationToken(jwt, rememberMe);
            return Promise.resolve(jwt);
        }
        else {
            return Promise.reject('auth-jwt-service Promise reject'); // Put appropriate error message here
        }
    }
    storeAuthenticationToken(jwt, rememberMe) {
        if (rememberMe) {
            this.$localStorage.set('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.set('authenticationToken', jwt);
        }
    }
    logout() {
        return new Observable(observer => {
            this.$localStorage.remove('authenticationToken');
            this.$sessionStorage.remove('authenticationToken');
            observer.complete();
        });
    }
};
AuthServerProvider.ctorParameters = () => [
    { type: HttpClient },
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthServerProvider.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthServerProvider_Factory() { return new AuthServerProvider(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i0.INJECTOR), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: AuthServerProvider, providedIn: "root" });
AuthServerProvider = tslib_1.__decorate([
    Injectable({ providedIn: 'root' }),
    tslib_1.__param(2, Inject(SERVER_API_URL_CONFIG)),
    tslib_1.__metadata("design:paramtypes", [HttpClient,
        Injector, Object])
], AuthServerProvider);
export { AuthServerProvider };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1qd3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvYXV0aC9hdXRoLWp3dC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUN2RSxPQUFPLEVBQUUscUJBQXFCLEVBQXNCLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFHaEYsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFJM0IsWUFBb0IsSUFBZ0IsRUFDaEIsUUFBa0IsRUFDYSxTQUE2QjtRQUY1RCxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDYSxjQUFTLEdBQVQsU0FBUyxDQUFvQjtRQUM1RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRCxRQUFRO1FBQ0osT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDNUcsQ0FBQztJQUVELEtBQUssQ0FBQyxXQUFnQjtRQUNsQixNQUFNLElBQUksR0FBRztZQUNULFFBQVEsRUFBRSxXQUFXLENBQUMsUUFBUTtZQUM5QixRQUFRLEVBQUUsV0FBVyxDQUFDLFFBQVE7WUFDOUIsVUFBVSxFQUFFLFdBQVcsQ0FBQyxVQUFVO1NBQ3JDLENBQUM7UUFDRixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFakosU0FBUyxtQkFBbUIsQ0FBQyxJQUFTO1lBQ2xDLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3RELElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLFNBQVMsRUFBRTtnQkFDdEQsTUFBTSxHQUFHLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNyRCxLQUFLLENBQUMsd0JBQXdCLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDNUQsT0FBTyxHQUFHLENBQUM7YUFDZDtRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQsY0FBYyxDQUFDLEdBQVEsRUFBRSxVQUFlO1FBQ3BDLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUMvQyxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDL0I7YUFBTTtZQUNILE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUMscUNBQXFDO1NBQ2xHO0lBQ0wsQ0FBQztJQUVELHdCQUF3QixDQUFDLEdBQVEsRUFBRSxVQUFlO1FBQzlDLElBQUksVUFBVSxFQUFFO1lBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDdEQ7YUFBTTtZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0wsQ0FBQztJQUVELE1BQU07UUFDRixPQUFPLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNuRCxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0osQ0FBQTs7WUF0RDZCLFVBQVU7WUFDTixRQUFROzRDQUN6QixNQUFNLFNBQUMscUJBQXFCOzs7QUFOaEMsa0JBQWtCO0lBRDlCLFVBQVUsQ0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsQ0FBQztJQU9oQixtQkFBQSxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQTs2Q0FGaEIsVUFBVTtRQUNOLFFBQVE7R0FMN0Isa0JBQWtCLENBMEQ5QjtTQTFEWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBMb2NhbFN0b3JhZ2VTZXJ2aWNlLCBTZXNzaW9uU3RvcmFnZVNlcnZpY2UgfSBmcm9tICduZ3gtc3RvcmUnO1xyXG5pbXBvcnQgeyBTRVJWRVJfQVBJX1VSTF9DT05GSUcsIFNlcnZlckFwaVVybENvbmZpZyB9IGZyb20gJy4uLy4uL2FwcC5jb25zdGFudHMnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBBdXRoU2VydmVyUHJvdmlkZXIge1xyXG4gICAgcHJpdmF0ZSAkbG9jYWxTdG9yYWdlOiBMb2NhbFN0b3JhZ2VTZXJ2aWNlO1xyXG4gICAgcHJpdmF0ZSAkc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcixcclxuICAgICAgICAgICAgICAgIEBJbmplY3QoU0VSVkVSX0FQSV9VUkxfQ09ORklHKSBwcml2YXRlIHNlcnZlclVybDogU2VydmVyQXBpVXJsQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy4kbG9jYWxTdG9yYWdlID0gdGhpcy5pbmplY3Rvci5nZXQoTG9jYWxTdG9yYWdlU2VydmljZSk7XHJcbiAgICAgICAgdGhpcy4kc2Vzc2lvblN0b3JhZ2UgPSB0aGlzLmluamVjdG9yLmdldChTZXNzaW9uU3RvcmFnZVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRva2VuKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRsb2NhbFN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJykgfHwgdGhpcy4kc2Vzc2lvblN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW4oY3JlZGVudGlhbHM6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgICAgICAgdXNlcm5hbWU6IGNyZWRlbnRpYWxzLnVzZXJuYW1lLFxyXG4gICAgICAgICAgICBwYXNzd29yZDogY3JlZGVudGlhbHMucGFzc3dvcmQsXHJcbiAgICAgICAgICAgIHJlbWVtYmVyTWU6IGNyZWRlbnRpYWxzLnJlbWVtYmVyTWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIGxldCBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHRoaXMuc2VydmVyVXJsLlNFUlZFUl9BUElfVVJMICsgJ2FwaS9hdXRoZW50aWNhdGUnLCBkYXRhLCB7b2JzZXJ2ZTogJ3Jlc3BvbnNlJ30pLnBpcGUobWFwKGF1dGhlbnRpY2F0ZVN1Y2Nlc3MuYmluZCh0aGlzKSkpO1xyXG5cclxuICAgICAgICBmdW5jdGlvbiBhdXRoZW50aWNhdGVTdWNjZXNzKHJlc3A6IGFueSkge1xyXG4gICAgICAgICAgICBjb25zdCBiZWFyZXJUb2tlbiA9IHJlc3AuaGVhZGVycy5nZXQoJ0F1dGhvcml6YXRpb24nKTtcclxuICAgICAgICAgICAgaWYgKGJlYXJlclRva2VuICYmIGJlYXJlclRva2VuLnNsaWNlKDAsIDcpID09PSAnQmVhcmVyICcpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGp3dCA9IGJlYXJlclRva2VuLnNsaWNlKDcsIGJlYXJlclRva2VuLmxlbmd0aCk7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5zdG9yZUF1dGhlbnRpY2F0aW9uVG9rZW4oand0LCBjcmVkZW50aWFscy5yZW1lbWJlck1lKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBqd3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW5XaXRoVG9rZW4oand0OiBhbnksIHJlbWVtYmVyTWU6IGFueSkge1xyXG4gICAgICAgIGlmIChqd3QpIHtcclxuICAgICAgICAgICAgdGhpcy5zdG9yZUF1dGhlbnRpY2F0aW9uVG9rZW4oand0LCByZW1lbWJlck1lKTtcclxuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShqd3QpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdCgnYXV0aC1qd3Qtc2VydmljZSBQcm9taXNlIHJlamVjdCcpOyAvLyBQdXQgYXBwcm9wcmlhdGUgZXJyb3IgbWVzc2FnZSBoZXJlXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlQXV0aGVudGljYXRpb25Ub2tlbihqd3Q6IGFueSwgcmVtZW1iZXJNZTogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlbWVtYmVyTWUpIHtcclxuICAgICAgICAgICAgdGhpcy4kbG9jYWxTdG9yYWdlLnNldCgnYXV0aGVudGljYXRpb25Ub2tlbicsIGp3dCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy4kc2Vzc2lvblN0b3JhZ2Uuc2V0KCdhdXRoZW50aWNhdGlvblRva2VuJywgand0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbG9nb3V0KCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy4kbG9jYWxTdG9yYWdlLnJlbW92ZSgnYXV0aGVudGljYXRpb25Ub2tlbicpO1xyXG4gICAgICAgICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZS5yZW1vdmUoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=