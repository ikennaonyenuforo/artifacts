import * as tslib_1 from "tslib";
var LamisCoreModule_1;
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SERVER_API_URL_CONFIG } from './app.constants';
import { RxStompConfig } from './services/rx-stomp.config';
import { LamisSharedModule } from './shared/lamis-shared.module';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { LOCALE_CONFIG } from './shared/util/date-range-picker/date-range-picker.config';
import { LocaleService } from './shared/util/date-range-picker/locales.service';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
let LamisCoreModule = LamisCoreModule_1 = class LamisCoreModule {
    static forRoot(serverApiUrlConfig, config) {
        return {
            ngModule: LamisCoreModule_1,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                { provide: LOCALE_CONFIG, useValue: config || {} },
                { provide: LocaleService, useClass: LocaleService, deps: [LOCALE_CONFIG] },
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    }
};
LamisCoreModule = LamisCoreModule_1 = tslib_1.__decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            LamisSharedModule
        ],
        exports: [
            LamisSharedModule
        ],
        providers: []
    })
], LamisCoreModule);
export { LamisCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMuY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJsYW1pcy5jb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUscUJBQXFCLEVBQXNCLE1BQU0saUJBQWlCLENBQUM7QUFDNUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzNELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN4RixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN4RixPQUFPLEVBQUUsYUFBYSxFQUFnQixNQUFNLDBEQUEwRCxDQUFDO0FBQ3ZHLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxpREFBaUQsQ0FBQztBQUNoRixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFhcEcsSUFBYSxlQUFlLHVCQUE1QixNQUFhLGVBQWU7SUFDeEIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBc0MsRUFBRSxNQUFxQjtRQUN4RSxPQUFPO1lBQ0gsUUFBUSxFQUFFLGlCQUFlO1lBQ3pCLFNBQVMsRUFBRTtnQkFDUCxzQkFBc0I7Z0JBQ3RCLGVBQWU7Z0JBQ2YsdUJBQXVCO2dCQUN2Qix1QkFBdUI7Z0JBQ3ZCO29CQUNJLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLFFBQVEsRUFBRSxrQkFBa0I7aUJBQy9CO2dCQUNELEVBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRLEVBQUUsTUFBTSxJQUFJLEVBQUUsRUFBQztnQkFDaEQsRUFBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLENBQUMsYUFBYSxDQUFDLEVBQUM7Z0JBQ3hFO29CQUNJLE9BQU8sRUFBRSx1QkFBdUI7b0JBQ2hDLFFBQVEsRUFBRSxhQUFhO2lCQUMxQjtnQkFDRDtvQkFDSSxPQUFPLEVBQUUsY0FBYztvQkFDdkIsVUFBVSxFQUFFLHFCQUFxQjtvQkFDakMsSUFBSSxFQUFFLENBQUMsdUJBQXVCLENBQUM7aUJBQ2xDO2FBQ0o7U0FDSixDQUFDO0lBQ04sQ0FBQztDQUNKLENBQUE7QUEzQlksZUFBZTtJQVgzQixRQUFRLENBQUM7UUFDTixZQUFZLEVBQUUsRUFBRTtRQUNoQixPQUFPLEVBQUU7WUFDTCxZQUFZO1lBQ1osaUJBQWlCO1NBQ3BCO1FBQ0QsT0FBTyxFQUFFO1lBQ0wsaUJBQWlCO1NBQ3BCO1FBQ0QsU0FBUyxFQUFFLEVBQUU7S0FDaEIsQ0FBQztHQUNXLGVBQWUsQ0EyQjNCO1NBM0JZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBTRVJWRVJfQVBJX1VSTF9DT05GSUcsIFNlcnZlckFwaVVybENvbmZpZyB9IGZyb20gJy4vYXBwLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFJ4U3RvbXBDb25maWcgfSBmcm9tICcuL3NlcnZpY2VzL3J4LXN0b21wLmNvbmZpZyc7XHJcbmltcG9ydCB7IExhbWlzU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi9zaGFyZWQvbGFtaXMtc2hhcmVkLm1vZHVsZSc7XHJcbmltcG9ydCB7IEF1dGhFeHBpcmVkSW50ZXJjZXB0b3IgfSBmcm9tICcuL2Jsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLWV4cGlyZWQuaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBBdXRoSW50ZXJjZXB0b3IgfSBmcm9tICcuL2Jsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uSW50ZXJjZXB0b3IgfSBmcm9tICcuL2Jsb2Nrcy9pbnRlcmNlcHRvci9ub3RpZmljYXRpb24uaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBFcnJvckhhbmRsZXJJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2Vycm9yaGFuZGxlci5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IExPQ0FMRV9DT05GSUcsIExvY2FsZUNvbmZpZyB9IGZyb20gJy4vc2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvZGF0ZS1yYW5nZS1waWNrZXIuY29uZmlnJztcclxuaW1wb3J0IHsgTG9jYWxlU2VydmljZSB9IGZyb20gJy4vc2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvbG9jYWxlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZVJ4U3RvbXBDb25maWcsIFJ4U3RvbXBTZXJ2aWNlLCByeFN0b21wU2VydmljZUZhY3RvcnkgfSBmcm9tICdAc3RvbXAvbmcyLXN0b21wanMnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW10sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIExhbWlzU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIExhbWlzU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFtaXNDb3JlTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KHNlcnZlckFwaVVybENvbmZpZzogU2VydmVyQXBpVXJsQ29uZmlnLCBjb25maWc/OiBMb2NhbGVDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBuZ01vZHVsZTogTGFtaXNDb3JlTW9kdWxlLFxyXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICAgICAgICAgIEF1dGhFeHBpcmVkSW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgICAgICBBdXRoSW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgICAgICBFcnJvckhhbmRsZXJJbnRlcmNlcHRvcixcclxuICAgICAgICAgICAgICAgIE5vdGlmaWNhdGlvbkludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IFNFUlZFUl9BUElfVVJMX0NPTkZJRyxcclxuICAgICAgICAgICAgICAgICAgICB1c2VWYWx1ZTogc2VydmVyQXBpVXJsQ29uZmlnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge3Byb3ZpZGU6IExPQ0FMRV9DT05GSUcsIHVzZVZhbHVlOiBjb25maWcgfHwge319LFxyXG4gICAgICAgICAgICAgICAge3Byb3ZpZGU6IExvY2FsZVNlcnZpY2UsIHVzZUNsYXNzOiBMb2NhbGVTZXJ2aWNlLCBkZXBzOiBbTE9DQUxFX0NPTkZJR119LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IEluamVjdGFibGVSeFN0b21wQ29uZmlnLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZVZhbHVlOiBSeFN0b21wQ29uZmlnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IFJ4U3RvbXBTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhY3Rvcnk6IHJ4U3RvbXBTZXJ2aWNlRmFjdG9yeSxcclxuICAgICAgICAgICAgICAgICAgICBkZXBzOiBbSW5qZWN0YWJsZVJ4U3RvbXBDb25maWddXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==