import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
let AlertErrorComponent = class AlertErrorComponent {
    constructor(notification, eventManager) {
        this.notification = notification;
        this.eventManager = eventManager;
        /* tslint:enable */
        this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', response => {
            let i;
            const httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    const arr = httpErrorResponse.headers.keys();
                    let errorHeader = null;
                    let entityKey = null;
                    arr.forEach(entry => {
                        if (entry.endsWith('app-error')) {
                            errorHeader = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.endsWith('app-params')) {
                            entityKey = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader) {
                        this.addErrorAlert(errorHeader, errorHeader, { entityName: entityKey });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        const fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            const fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            const fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                            this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                case 404:
                    this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    ngOnDestroy() {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
        }
    }
    addErrorAlert(message, key, data) {
        this.notification.openSnackMessage(message, 5000);
    }
};
AlertErrorComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiEventManager }
];
AlertErrorComponent = tslib_1.__decorate([
    Component({
        selector: 'alert-error',
        template: ``
    }),
    tslib_1.__metadata("design:paramtypes", [NotificationService, JhiEventManager])
], AlertErrorComponent);
export { AlertErrorComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtZXJyb3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRTlDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTXpELElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBRzVCLFlBQW9CLFlBQWlDLEVBQVUsWUFBNkI7UUFBeEUsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWlCO1FBQ3hGLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsc0JBQXNCLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLEVBQUU7WUFDN0UsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDM0MsUUFBUSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQzlCLDJDQUEyQztnQkFDM0MsS0FBSyxDQUFDO29CQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztvQkFDekUsTUFBTTtnQkFFVixLQUFLLEdBQUc7b0JBQ0osTUFBTSxHQUFHLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUM3QyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDckIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDaEIsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUM3QixXQUFXLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdEQ7NkJBQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFOzRCQUNyQyxTQUFTLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDcEQ7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxXQUFXLEVBQUU7d0JBQ2IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsV0FBVyxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsRUFBQyxDQUFDLENBQUM7cUJBQ3pFO3lCQUFNLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO3dCQUM5RSxNQUFNLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO3dCQUN4RCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3JDLE1BQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbEMsdUdBQXVHOzRCQUN2RyxNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ2xFLE1BQU0sU0FBUyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbkYsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLEdBQUcsR0FBRyxFQUFFLFFBQVEsR0FBRyxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUMsU0FBUyxFQUFDLENBQUMsQ0FBQzt5QkFDeEc7cUJBQ0o7eUJBQU0sSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7d0JBQzFFLElBQUksQ0FBQyxhQUFhLENBQ2QsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDL0IsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDL0IsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FDakMsQ0FBQztxQkFDTDt5QkFBTTt3QkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQztvQkFDRCxNQUFNO2dCQUVWLEtBQUssR0FBRztvQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO29CQUN2RCxNQUFNO2dCQUVWO29CQUNJLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO3dCQUNuRSxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDdkQ7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDL0M7YUFDUjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLElBQUksRUFBRTtZQUNuRixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztTQUMxRDtJQUNMLENBQUM7SUFFRCxhQUFhLENBQUMsT0FBTyxFQUFFLEdBQUksRUFBRSxJQUFLO1FBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3RELENBQUM7Q0FDSixDQUFBOztZQW5FcUMsbUJBQW1CO1lBQXdCLGVBQWU7O0FBSG5GLG1CQUFtQjtJQUovQixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsYUFBYTtRQUN2QixRQUFRLEVBQUUsRUFBRTtLQUNmLENBQUM7NkNBSW9DLG1CQUFtQixFQUF3QixlQUFlO0dBSG5GLG1CQUFtQixDQXNFL0I7U0F0RVksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSmhpRXZlbnRNYW5hZ2VyIH0gZnJvbSAnbmctamhpcHN0ZXInO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWxlcnQtZXJyb3InLFxyXG4gICAgdGVtcGxhdGU6IGBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBbGVydEVycm9yQ29tcG9uZW50IGltcGxlbWVudHMgT25EZXN0cm95IHtcclxuICAgIGNsZWFuSHR0cEVycm9yTGlzdGVuZXI6IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSwgcHJpdmF0ZSBldmVudE1hbmFnZXI6IEpoaUV2ZW50TWFuYWdlcikge1xyXG4gICAgICAgIC8qIHRzbGludDplbmFibGUgKi9cclxuICAgICAgICB0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIgPSBldmVudE1hbmFnZXIuc3Vic2NyaWJlKCdhcHAuaHR0cEVycm9yJywgcmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBsZXQgaTtcclxuICAgICAgICAgICAgY29uc3QgaHR0cEVycm9yUmVzcG9uc2UgPSByZXNwb25zZS5jb250ZW50O1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGh0dHBFcnJvclJlc3BvbnNlLnN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgLy8gY29ubmVjdGlvbiByZWZ1c2VkLCBzZXJ2ZXIgbm90IHJlYWNoYWJsZVxyXG4gICAgICAgICAgICAgICAgY2FzZSAwOlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydCgnU2VydmVyIG5vdCByZWFjaGFibGUnLCAnZXJyb3Iuc2VydmVyLm5vdC5yZWFjaGFibGUnKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgICAgICBjYXNlIDQwMDpcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhcnIgPSBodHRwRXJyb3JSZXNwb25zZS5oZWFkZXJzLmtleXMoKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZXJyb3JIZWFkZXIgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBlbnRpdHlLZXkgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIGFyci5mb3JFYWNoKGVudHJ5ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVudHJ5LmVuZHNXaXRoKCdhcHAtZXJyb3InKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JIZWFkZXIgPSBodHRwRXJyb3JSZXNwb25zZS5oZWFkZXJzLmdldChlbnRyeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZW50cnkuZW5kc1dpdGgoJ2FwcC1wYXJhbXMnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50aXR5S2V5ID0gaHR0cEVycm9yUmVzcG9uc2UuaGVhZGVycy5nZXQoZW50cnkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVycm9ySGVhZGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChlcnJvckhlYWRlciwgZXJyb3JIZWFkZXIsIHtlbnRpdHlOYW1lOiBlbnRpdHlLZXl9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5maWVsZEVycm9ycykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZEVycm9ycyA9IGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLmZpZWxkRXJyb3JzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgZmllbGRFcnJvcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkRXJyb3IgPSBmaWVsZEVycm9yc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnZlcnQgJ3NvbWV0aGluZ1sxNF0ub3RoZXJbNF0uaWQnIHRvICdzb21ldGhpbmdbXS5vdGhlcltdLmlkJyBzbyB0cmFuc2xhdGlvbnMgY2FuIGJlIHdyaXR0ZW4gdG8gaXRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbnZlcnRlZEZpZWxkID0gZmllbGRFcnJvci5maWVsZC5yZXBsYWNlKC9cXFtcXGQqXFxdL2csICdbXScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGROYW1lID0gY29udmVydGVkRmllbGQuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBjb252ZXJ0ZWRGaWVsZC5zbGljZSgxKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydCgnRXJyb3Igb24gZmllbGQgXCInICsgZmllbGROYW1lICsgJ1wiJywgJ2Vycm9yLicgKyBmaWVsZEVycm9yLm1lc3NhZ2UsIHtmaWVsZE5hbWV9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IgIT09ICcnICYmIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5wYXJhbXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgICAgICBjYXNlIDQwNDpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoJ05vdCBmb3VuZCcsICdlcnJvci51cmwubm90LmZvdW5kJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IgIT09ICcnICYmIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChodHRwRXJyb3JSZXNwb25zZS5lcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2xlYW5IdHRwRXJyb3JMaXN0ZW5lciAhPT0gdW5kZWZpbmVkICYmIHRoaXMuY2xlYW5IdHRwRXJyb3JMaXN0ZW5lciAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICB0aGlzLmV2ZW50TWFuYWdlci5kZXN0cm95KHRoaXMuY2xlYW5IdHRwRXJyb3JMaXN0ZW5lcik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFkZEVycm9yQWxlcnQobWVzc2FnZSwga2V5PywgZGF0YT8pIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbi5vcGVuU25hY2tNZXNzYWdlKG1lc3NhZ2UsIDUwMDApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==