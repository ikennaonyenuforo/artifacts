import * as tslib_1 from "tslib";
import { Component, Input, ViewEncapsulation } from '@angular/core';
import get from 'lodash-es/get';
import { LayoutTemplateService } from '../../services/layout.template.service';
import { CardViewBoolItemModel, CardViewDateItemModel, CardViewDatetimeItemModel, CardViewFloatItemModel, CardViewIntItemModel, CardViewTextItemModel } from '@alfresco/adf-core';
export var FieldType;
(function (FieldType) {
    FieldType["date"] = "date";
    FieldType["datetime"] = "datetime";
    FieldType["text"] = "text";
    FieldType["boolean"] = "boolean";
    FieldType["int"] = "int";
    FieldType["float"] = "float";
})(FieldType || (FieldType = {}));
let DetailsComponent = class DetailsComponent {
    constructor(templateService) {
        this.templateService = templateService;
    }
    ngOnInit() {
        this.templateService.getTemplate(this.template).subscribe((json) => {
            this.details = json.template;
        });
    }
    propertiesForDetail(detail) {
        let properties = [];
        for (const field of detail.fields) {
            const dataType = field.type;
            let item;
            switch (dataType) {
                case FieldType.boolean:
                    item = new CardViewBoolItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label
                    });
                    break;
                case FieldType.int:
                    item = new CardViewIntItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.float:
                    item = new CardViewFloatItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.date:
                    item = new CardViewDateItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy'
                    });
                    break;
                case FieldType.datetime:
                    item = new CardViewDatetimeItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy HH:mm'
                    });
                    break;
                default:
                    item = new CardViewTextItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
            }
            properties.push(item);
        }
        return properties;
    }
    getValueForKey(key) {
        return get(this.model, key);
    }
};
DetailsComponent.ctorParameters = () => [
    { type: LayoutTemplateService }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", String)
], DetailsComponent.prototype, "template", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], DetailsComponent.prototype, "model", void 0);
DetailsComponent = tslib_1.__decorate([
    Component({
        selector: 'details-component',
        template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
        encapsulation: ViewEncapsulation.None,
        styles: [""]
    }),
    tslib_1.__metadata("design:paramtypes", [LayoutTemplateService])
], DetailsComponent);
export { DetailsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvY29tcG9uZW50L2RldGFpbHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM1RSxPQUFPLEdBQUcsTUFBTSxlQUFlLENBQUM7QUFDaEMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDL0UsT0FBTyxFQUNILHFCQUFxQixFQUNyQixxQkFBcUIsRUFDckIseUJBQXlCLEVBQ3pCLHNCQUFzQixFQUN0QixvQkFBb0IsRUFFcEIscUJBQXFCLEVBQ3hCLE1BQU0sb0JBQW9CLENBQUM7QUFjNUIsTUFBTSxDQUFOLElBQVksU0FPWDtBQVBELFdBQVksU0FBUztJQUNqQiwwQkFBYSxDQUFBO0lBQ2Isa0NBQXFCLENBQUE7SUFDckIsMEJBQWEsQ0FBQTtJQUNiLGdDQUFtQixDQUFBO0lBQ25CLHdCQUFXLENBQUE7SUFDWCw0QkFBZSxDQUFBO0FBQ25CLENBQUMsRUFQVyxTQUFTLEtBQVQsU0FBUyxRQU9wQjtBQVFELElBQWEsZ0JBQWdCLEdBQTdCLE1BQWEsZ0JBQWdCO0lBU3pCLFlBQW9CLGVBQXNDO1FBQXRDLG9CQUFlLEdBQWYsZUFBZSxDQUF1QjtJQUMxRCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTtZQUNwRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDakMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sbUJBQW1CLENBQUMsTUFBYztRQUNyQyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDcEIsS0FBSyxNQUFNLEtBQUssSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFO1lBQy9CLE1BQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFDNUIsSUFBSSxJQUFrQixDQUFDO1lBQ3ZCLFFBQVEsUUFBUSxFQUFFO2dCQUNkLEtBQUssU0FBUyxDQUFDLE9BQU87b0JBQ2xCLElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDO3dCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO3dCQUNyQyxHQUFHLEVBQUUsRUFBRTt3QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7cUJBQ3JCLENBQUMsQ0FBQztvQkFDSCxNQUFNO2dCQUNWLEtBQUssU0FBUyxDQUFDLEdBQUc7b0JBQ2QsSUFBSSxHQUFHLElBQUksb0JBQW9CLENBQUM7d0JBQzVCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3JDLEdBQUcsRUFBRSxFQUFFO3dCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztxQkFDckIsQ0FBQyxDQUFDO29CQUNILE1BQU07Z0JBQ1YsS0FBSyxTQUFTLENBQUMsS0FBSztvQkFDaEIsSUFBSSxHQUFHLElBQUksc0JBQXNCLENBQUM7d0JBQzlCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3JDLEdBQUcsRUFBRSxFQUFFO3dCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztxQkFDckIsQ0FBQyxDQUFDO29CQUNILE1BQU07Z0JBQ1YsS0FBSyxTQUFTLENBQUMsSUFBSTtvQkFDZixJQUFJLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzt3QkFDckMsR0FBRyxFQUFFLEVBQUU7d0JBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3dCQUNsQixNQUFNLEVBQUUsY0FBYztxQkFDekIsQ0FBQyxDQUFDO29CQUNILE1BQU07Z0JBQ1YsS0FBSyxTQUFTLENBQUMsUUFBUTtvQkFDbkIsSUFBSSxHQUFHLElBQUkseUJBQXlCLENBQUM7d0JBQ2pDLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3JDLEdBQUcsRUFBRSxFQUFFO3dCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSzt3QkFDbEIsTUFBTSxFQUFFLG9CQUFvQjtxQkFDL0IsQ0FBQyxDQUFDO29CQUNILE1BQU07Z0JBQ1Y7b0JBQ0ksSUFBSSxHQUFHLElBQUkscUJBQXFCLENBQUM7d0JBQzdCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3JDLEdBQUcsRUFBRSxFQUFFO3dCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSztxQkFDckIsQ0FBQyxDQUFDO2FBQ1Y7WUFDRCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3pCO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQztJQUVPLGNBQWMsQ0FBQyxHQUFXO1FBQzlCLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDaEMsQ0FBQztDQUNKLENBQUE7O1lBbkV3QyxxQkFBcUI7O0FBUDFEO0lBREMsS0FBSyxFQUFFOztrREFDUztBQUdqQjtJQURDLEtBQUssRUFBRTs7K0NBQ0c7QUFMRixnQkFBZ0I7SUFONUIsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLG1CQUFtQjtRQUM3QixtZ0JBQXVDO1FBRXZDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztLQUN4QyxDQUFDOzZDQVV1QyxxQkFBcUI7R0FUakQsZ0JBQWdCLENBNEU1QjtTQTVFWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCBnZXQgZnJvbSAnbG9kYXNoLWVzL2dldCc7XHJcbmltcG9ydCB7IExheW91dFRlbXBsYXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xheW91dC50ZW1wbGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHtcclxuICAgIENhcmRWaWV3Qm9vbEl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3RGF0ZUl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3RGF0ZXRpbWVJdGVtTW9kZWwsXHJcbiAgICBDYXJkVmlld0Zsb2F0SXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdJbnRJdGVtTW9kZWwsXHJcbiAgICBDYXJkVmlld0l0ZW0sXHJcbiAgICBDYXJkVmlld1RleHRJdGVtTW9kZWxcclxufSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBEZXRhaWwge1xyXG4gICAgaGVhZGVyPzogc3RyaW5nO1xyXG4gICAgaGVhZGVyQ2xhc3M/OiBzdHJpbmc7XHJcbiAgICBmaWVsZHM6IEZpZWxkW107XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmllbGQge1xyXG4gICAgdHlwZTogRmllbGRUeXBlLFxyXG4gICAga2V5OiBzdHJpbmdcclxuICAgIGxhYmVsOiBzdHJpbmdcclxufVxyXG5cclxuZXhwb3J0IGVudW0gRmllbGRUeXBlIHtcclxuICAgIGRhdGUgPSAnZGF0ZScsXHJcbiAgICBkYXRldGltZSA9ICdkYXRldGltZScsXHJcbiAgICB0ZXh0ID0gJ3RleHQnLFxyXG4gICAgYm9vbGVhbiA9ICdib29sZWFuJyxcclxuICAgIGludCA9ICdpbnQnLFxyXG4gICAgZmxvYXQgPSAnZmxvYXQnXHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdkZXRhaWxzLWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGV0YWlscy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kZXRhaWxzLWNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEZXRhaWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpXHJcbiAgICB0ZW1wbGF0ZTogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBtb2RlbDogYW55O1xyXG5cclxuICAgIGRldGFpbHM6IERldGFpbFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdGVtcGxhdGVTZXJ2aWNlOiBMYXlvdXRUZW1wbGF0ZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnRlbXBsYXRlU2VydmljZS5nZXRUZW1wbGF0ZSh0aGlzLnRlbXBsYXRlKS5zdWJzY3JpYmUoKGpzb246IGFueSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRldGFpbHMgPSBqc29uLnRlbXBsYXRlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBwcm9wZXJ0aWVzRm9yRGV0YWlsKGRldGFpbDogRGV0YWlsKTogQXJyYXk8Q2FyZFZpZXdJdGVtPiB7XHJcbiAgICAgICAgbGV0IHByb3BlcnRpZXMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkIG9mIGRldGFpbC5maWVsZHMpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0YVR5cGUgPSBmaWVsZC50eXBlO1xyXG4gICAgICAgICAgICBsZXQgaXRlbTogQ2FyZFZpZXdJdGVtO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGRhdGFUeXBlKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEZpZWxkVHlwZS5ib29sZWFuOlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdCb29sSXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEZpZWxkVHlwZS5pbnQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0ludEl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmZsb2F0OlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdGbG9hdEl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmRhdGU6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0RhdGVJdGVtTW9kZWwoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5nZXRWYWx1ZUZvcktleShmaWVsZC5rZXkpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogZmllbGQubGFiZWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdDogJ2RkIE1NTSwgeXl5eSdcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmRhdGV0aW1lOlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdEYXRldGltZUl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiAnZGQgTU1NLCB5eXl5IEhIOm1tJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpdGVtID0gbmV3IENhcmRWaWV3VGV4dEl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwcm9wZXJ0aWVzLnB1c2goaXRlbSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBwcm9wZXJ0aWVzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0VmFsdWVGb3JLZXkoa2V5OiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiBnZXQodGhpcy5tb2RlbCwga2V5KTtcclxuICAgIH1cclxufVxyXG4iXX0=