import * as tslib_1 from "tslib";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedCommonModule } from './shared-common.module';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { SpeedDialFabComponent } from './util/speed-dial-fab.component';
import { CardViewModule } from './util/card-view/card-view.module';
import { DateRangePicker } from './util/date-range-picker/date-range-picker';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatDatepickerModule, MatIconModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { DateRangePickerDirective } from './util/date-range-picker/date-range-picker.directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JsonFormModule } from './json-form/json-form.module';
let LamisSharedModule = class LamisSharedModule {
};
LamisSharedModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            MatButtonModule,
            MatSelectModule,
            MatIconModule,
            MatTooltipModule,
            RouterModule,
            SharedCommonModule,
            CardViewModule,
            FlexLayoutModule,
            MatDatepickerModule,
            MatCardModule,
            JsonFormModule
        ],
        declarations: [
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            DateRangePicker,
            DateRangePickerDirective
        ],
        entryComponents: [
            DateRangePicker
        ],
        exports: [
            JsonFormModule,
            SharedCommonModule,
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            CardViewModule,
            DateRangePicker,
            DateRangePickerDirective
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
], LamisSharedModule);
export { LamisSharedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMtc2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9sYW1pcy1zaGFyZWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNuRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDN0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQ04sZUFBZSxFQUNmLGFBQWEsRUFDYixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLGVBQWUsRUFDZixnQkFBZ0IsRUFDaEIsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDaEcsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBdUM5RCxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtDQUM3QixDQUFBO0FBRFksaUJBQWlCO0lBckM3QixRQUFRLENBQUM7UUFDVCxPQUFPLEVBQUU7WUFDUixZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtZQUNuQixlQUFlO1lBQ2YsZUFBZTtZQUNmLGFBQWE7WUFDYixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLGtCQUFrQjtZQUNsQixjQUFjO1lBQ2QsZ0JBQWdCO1lBQ2hCLG1CQUFtQjtZQUNuQixhQUFhO1lBQ2IsY0FBYztTQUNkO1FBQ0QsWUFBWSxFQUFFO1lBQ2Isd0JBQXdCO1lBQ3hCLHFCQUFxQjtZQUNyQixlQUFlO1lBQ2Ysd0JBQXdCO1NBQ3hCO1FBQ0QsZUFBZSxFQUFFO1lBQ2hCLGVBQWU7U0FDZjtRQUNELE9BQU8sRUFBRTtZQUNSLGNBQWM7WUFDZCxrQkFBa0I7WUFDbEIsd0JBQXdCO1lBQ3hCLHFCQUFxQjtZQUNyQixjQUFjO1lBQ2QsZUFBZTtZQUNmLHdCQUF3QjtTQUN4QjtRQUNELE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO0tBQ2pDLENBQUM7R0FDVyxpQkFBaUIsQ0FDN0I7U0FEWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDVVNUT01fRUxFTUVOVFNfU0NIRU1BLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRDb21tb25Nb2R1bGUgfSBmcm9tICcuL3NoYXJlZC1jb21tb24ubW9kdWxlJztcclxuaW1wb3J0IHsgSGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlIH0gZnJvbSAnLi9hdXRoL2hhcy1hbnktYXV0aG9yaXR5LmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFNwZWVkRGlhbEZhYkNvbXBvbmVudCB9IGZyb20gJy4vdXRpbC9zcGVlZC1kaWFsLWZhYi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld01vZHVsZSB9IGZyb20gJy4vdXRpbC9jYXJkLXZpZXcvY2FyZC12aWV3Lm1vZHVsZSc7XHJcbmltcG9ydCB7IERhdGVSYW5nZVBpY2tlciB9IGZyb20gJy4vdXRpbC9kYXRlLXJhbmdlLXBpY2tlci9kYXRlLXJhbmdlLXBpY2tlcic7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQge1xyXG5cdE1hdEJ1dHRvbk1vZHVsZSxcclxuXHRNYXRDYXJkTW9kdWxlLFxyXG5cdE1hdERhdGVwaWNrZXJNb2R1bGUsXHJcblx0TWF0SWNvbk1vZHVsZSxcclxuXHRNYXRTZWxlY3RNb2R1bGUsXHJcblx0TWF0VG9vbHRpcE1vZHVsZVxyXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRGF0ZVJhbmdlUGlja2VyRGlyZWN0aXZlIH0gZnJvbSAnLi91dGlsL2RhdGUtcmFuZ2UtcGlja2VyL2RhdGUtcmFuZ2UtcGlja2VyLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IEpzb25Gb3JtTW9kdWxlIH0gZnJvbSAnLi9qc29uLWZvcm0vanNvbi1mb3JtLm1vZHVsZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG5cdGltcG9ydHM6IFtcclxuXHRcdENvbW1vbk1vZHVsZSxcclxuXHRcdEZvcm1zTW9kdWxlLFxyXG5cdFx0UmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuXHRcdE1hdEJ1dHRvbk1vZHVsZSxcclxuXHRcdE1hdFNlbGVjdE1vZHVsZSxcclxuXHRcdE1hdEljb25Nb2R1bGUsXHJcblx0XHRNYXRUb29sdGlwTW9kdWxlLFxyXG5cdFx0Um91dGVyTW9kdWxlLFxyXG5cdFx0U2hhcmVkQ29tbW9uTW9kdWxlLFxyXG5cdFx0Q2FyZFZpZXdNb2R1bGUsXHJcblx0XHRGbGV4TGF5b3V0TW9kdWxlLFxyXG5cdFx0TWF0RGF0ZXBpY2tlck1vZHVsZSxcclxuXHRcdE1hdENhcmRNb2R1bGUsXHJcblx0XHRKc29uRm9ybU1vZHVsZVxyXG5cdF0sXHJcblx0ZGVjbGFyYXRpb25zOiBbXHJcblx0XHRIYXNBbnlBdXRob3JpdHlEaXJlY3RpdmUsXHJcblx0XHRTcGVlZERpYWxGYWJDb21wb25lbnQsXHJcblx0XHREYXRlUmFuZ2VQaWNrZXIsXHJcblx0XHREYXRlUmFuZ2VQaWNrZXJEaXJlY3RpdmVcclxuXHRdLFxyXG5cdGVudHJ5Q29tcG9uZW50czogW1xyXG5cdFx0RGF0ZVJhbmdlUGlja2VyXHJcblx0XSxcclxuXHRleHBvcnRzOiBbXHJcblx0XHRKc29uRm9ybU1vZHVsZSxcclxuXHRcdFNoYXJlZENvbW1vbk1vZHVsZSxcclxuXHRcdEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSxcclxuXHRcdFNwZWVkRGlhbEZhYkNvbXBvbmVudCxcclxuXHRcdENhcmRWaWV3TW9kdWxlLFxyXG5cdFx0RGF0ZVJhbmdlUGlja2VyLFxyXG5cdFx0RGF0ZVJhbmdlUGlja2VyRGlyZWN0aXZlXHJcblx0XSxcclxuXHRzY2hlbWFzOiBbQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIExhbWlzU2hhcmVkTW9kdWxlIHtcclxufVxyXG4iXX0=