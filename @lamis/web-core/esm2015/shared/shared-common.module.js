import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@alfresco/adf-core';
import { CommonPipesModule } from './pipes/common/common-pipes.module';
import { NgJhipsterModule } from 'ng-jhipster';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { DetailsComponent } from './component/details.component';
import { MatCardModule } from '@angular/material';
let SharedCommonModule = class SharedCommonModule {
};
SharedCommonModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            CoreModule,
            NgJhipsterModule,
            CommonPipesModule,
            MatCardModule
        ],
        declarations: [
            AlertComponent,
            AlertErrorComponent,
            DetailsComponent
        ],
        exports: [
            AlertComponent,
            AlertErrorComponent,
            CommonPipesModule,
            DetailsComponent
        ]
    })
], SharedCommonModule);
export { SharedCommonModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWNvbW1vbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvc2hhcmVkLWNvbW1vbi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNoRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN2RSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDL0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQXNCbEQsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7Q0FDOUIsQ0FBQTtBQURZLGtCQUFrQjtJQXBCOUIsUUFBUSxDQUFDO1FBQ1QsT0FBTyxFQUFFO1lBQ1IsWUFBWTtZQUNaLFVBQVU7WUFDVixnQkFBZ0I7WUFDaEIsaUJBQWlCO1lBQ2pCLGFBQWE7U0FDYjtRQUNELFlBQVksRUFBRTtZQUNiLGNBQWM7WUFDZCxtQkFBbUI7WUFDbkIsZ0JBQWdCO1NBQ2hCO1FBQ0QsT0FBTyxFQUFFO1lBQ1IsY0FBYztZQUNkLG1CQUFtQjtZQUNuQixpQkFBaUI7WUFDakIsZ0JBQWdCO1NBQ2hCO0tBQ0QsQ0FBQztHQUNXLGtCQUFrQixDQUM5QjtTQURZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDb3JlTW9kdWxlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uUGlwZXNNb2R1bGUgfSBmcm9tICcuL3BpcGVzL2NvbW1vbi9jb21tb24tcGlwZXMubW9kdWxlJztcclxuaW1wb3J0IHsgTmdKaGlwc3Rlck1vZHVsZSB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgQWxlcnRDb21wb25lbnQgfSBmcm9tICcuL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFsZXJ0RXJyb3JDb21wb25lbnQgfSBmcm9tICcuL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERldGFpbHNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudC9kZXRhaWxzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdENhcmRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG5cdGltcG9ydHM6IFtcclxuXHRcdENvbW1vbk1vZHVsZSxcclxuXHRcdENvcmVNb2R1bGUsXHJcblx0XHROZ0poaXBzdGVyTW9kdWxlLFxyXG5cdFx0Q29tbW9uUGlwZXNNb2R1bGUsXHJcblx0XHRNYXRDYXJkTW9kdWxlXHJcblx0XSxcclxuXHRkZWNsYXJhdGlvbnM6IFtcclxuXHRcdEFsZXJ0Q29tcG9uZW50LFxyXG5cdFx0QWxlcnRFcnJvckNvbXBvbmVudCxcclxuXHRcdERldGFpbHNDb21wb25lbnRcclxuXHRdLFxyXG5cdGV4cG9ydHM6IFtcclxuXHRcdEFsZXJ0Q29tcG9uZW50LFxyXG5cdFx0QWxlcnRFcnJvckNvbXBvbmVudCxcclxuXHRcdENvbW1vblBpcGVzTW9kdWxlLFxyXG5cdFx0RGV0YWlsc0NvbXBvbmVudFxyXG5cdF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNoYXJlZENvbW1vbk1vZHVsZSB7XHJcbn1cclxuIl19