export class CardViewItemNameValidator {
    constructor() {
        this.message = '';
    }
    isValid(value) {
        let nameError = false;
        let surnameError = false;
        if (!value.firstName || value.firstName.length < 3) {
            nameError = true;
        }
        if (!value.surname || value.surname.length < 3) {
            surnameError = true;
        }
        if (nameError && surnameError) {
            this.message = 'Both first name and surname are required';
        }
        else if (nameError) {
            this.message = 'First name is required';
        }
        else if (surnameError) {
            this.message = 'Surname is required';
        }
        return !(nameError || surnameError);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tbmFtZS52YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvdmFsaWRhdG9ycy9jYXJkLXZpZXctaXRlbS1uYW1lLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8seUJBQXlCO0lBQXRDO1FBQ0ksWUFBTyxHQUFHLEVBQUUsQ0FBQztJQXFCakIsQ0FBQztJQW5CRyxPQUFPLENBQUMsS0FBVTtRQUNkLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2hELFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDNUMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUNELElBQUksU0FBUyxJQUFJLFlBQVksRUFBRTtZQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLDBDQUEwQyxDQUFDO1NBQzdEO2FBQU0sSUFBSSxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyx3QkFBd0IsQ0FBQztTQUMzQzthQUFNLElBQUksWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcscUJBQXFCLENBQUE7U0FDdkM7UUFDRCxPQUFPLENBQUMsQ0FBQyxTQUFTLElBQUksWUFBWSxDQUFDLENBQUM7SUFDeEMsQ0FBQztDQUVKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0l0ZW1OYW1lVmFsaWRhdG9yIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIHtcclxuICAgIG1lc3NhZ2UgPSAnJztcclxuXHJcbiAgICBpc1ZhbGlkKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgbmFtZUVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IHN1cm5hbWVFcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIGlmICghdmFsdWUuZmlyc3ROYW1lIHx8IHZhbHVlLmZpcnN0TmFtZS5sZW5ndGggPCAzKSB7XHJcbiAgICAgICAgICAgIG5hbWVFcnJvciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdmFsdWUuc3VybmFtZSB8fCB2YWx1ZS5zdXJuYW1lLmxlbmd0aCA8IDMpIHtcclxuICAgICAgICAgICAgc3VybmFtZUVycm9yID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKG5hbWVFcnJvciAmJiBzdXJuYW1lRXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ0JvdGggZmlyc3QgbmFtZSBhbmQgc3VybmFtZSBhcmUgcmVxdWlyZWQnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobmFtZUVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdGaXJzdCBuYW1lIGlzIHJlcXVpcmVkJztcclxuICAgICAgICB9IGVsc2UgaWYgKHN1cm5hbWVFcnJvcikge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnU3VybmFtZSBpcyByZXF1aXJlZCdcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICEobmFtZUVycm9yIHx8IHN1cm5hbWVFcnJvcik7XHJcbiAgICB9XHJcblxyXG59Il19