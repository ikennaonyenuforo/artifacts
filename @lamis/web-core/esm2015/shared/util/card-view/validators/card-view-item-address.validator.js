export class CardViewItemAddressValidator {
    constructor() {
        this.message = '';
    }
    isValid(value) {
        let streetError = false;
        let cityError = false;
        let lgaError = false;
        if (!value.street1 || value.street1.length < 3) {
            streetError = true;
        }
        if (!value.city || value.city.length < 3) {
            cityError = true;
        }
        if (!!value.lga) {
            lgaError = true;
        }
        if (streetError && cityError && lgaError) {
            this.message = 'Street, city and LGA are all required';
        }
        else if (streetError && cityError) {
            this.message = 'Street and city are required';
        }
        else if (streetError && lgaError) {
            this.message = 'Street and LGA are required';
        }
        else if (lgaError && cityError) {
            this.message = 'City and LGA are required';
        }
        else if (streetError) {
            this.message = 'Street is required';
        }
        else if (cityError) {
            this.message = 'City is required';
        }
        else if (lgaError) {
            this.message = 'LGA is required';
        }
        return !(streetError || cityError || lgaError);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tYWRkcmVzcy52YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvdmFsaWRhdG9ycy9jYXJkLXZpZXctaXRlbS1hZGRyZXNzLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQSxNQUFNLE9BQU8sNEJBQTRCO0lBQXpDO1FBQ0ksWUFBTyxHQUFHLEVBQUUsQ0FBQztJQWlDakIsQ0FBQztJQS9CRyxPQUFPLENBQUMsS0FBVTtRQUNkLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM1QyxXQUFXLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3RDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFO1lBQ2IsUUFBUSxHQUFHLElBQUksQ0FBQztTQUNuQjtRQUNELElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxRQUFRLEVBQUU7WUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyx1Q0FBdUMsQ0FBQztTQUMxRDthQUFNLElBQUksV0FBVyxJQUFJLFNBQVMsRUFBRTtZQUNqQyxJQUFJLENBQUMsT0FBTyxHQUFHLDhCQUE4QixDQUFDO1NBQ2pEO2FBQU0sSUFBSSxXQUFXLElBQUksUUFBUSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsNkJBQTZCLENBQUM7U0FDaEQ7YUFBTSxJQUFJLFFBQVEsSUFBSSxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLE9BQU8sR0FBRywyQkFBMkIsQ0FBQztTQUM5QzthQUFNLElBQUksV0FBVyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQW9CLENBQUM7U0FDdkM7YUFBTSxJQUFJLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFBO1NBQ3BDO2FBQU0sSUFBSSxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQTtTQUNuQztRQUNELE9BQU8sQ0FBQyxDQUFDLFdBQVcsSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLENBQUM7SUFDbkQsQ0FBQztDQUVKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0l0ZW1BZGRyZXNzVmFsaWRhdG9yIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIHtcclxuICAgIG1lc3NhZ2UgPSAnJztcclxuXHJcbiAgICBpc1ZhbGlkKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgc3RyZWV0RXJyb3IgPSBmYWxzZTtcclxuICAgICAgICBsZXQgY2l0eUVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IGxnYUVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKCF2YWx1ZS5zdHJlZXQxIHx8IHZhbHVlLnN0cmVldDEubGVuZ3RoIDwgMykge1xyXG4gICAgICAgICAgICBzdHJlZXRFcnJvciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdmFsdWUuY2l0eSB8fCB2YWx1ZS5jaXR5Lmxlbmd0aCA8IDMpIHtcclxuICAgICAgICAgICAgY2l0eUVycm9yID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCEhdmFsdWUubGdhKSB7XHJcbiAgICAgICAgICAgIGxnYUVycm9yID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHN0cmVldEVycm9yICYmIGNpdHlFcnJvciAmJiBsZ2FFcnJvcikge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnU3RyZWV0LCBjaXR5IGFuZCBMR0EgYXJlIGFsbCByZXF1aXJlZCc7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzdHJlZXRFcnJvciAmJiBjaXR5RXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ1N0cmVldCBhbmQgY2l0eSBhcmUgcmVxdWlyZWQnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc3RyZWV0RXJyb3IgJiYgbGdhRXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ1N0cmVldCBhbmQgTEdBIGFyZSByZXF1aXJlZCc7XHJcbiAgICAgICAgfSBlbHNlIGlmIChsZ2FFcnJvciAmJiBjaXR5RXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ0NpdHkgYW5kIExHQSBhcmUgcmVxdWlyZWQnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc3RyZWV0RXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ1N0cmVldCBpcyByZXF1aXJlZCc7XHJcbiAgICAgICAgfSBlbHNlIGlmIChjaXR5RXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ0NpdHkgaXMgcmVxdWlyZWQnXHJcbiAgICAgICAgfSBlbHNlIGlmIChsZ2FFcnJvcikge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnTEdBIGlzIHJlcXVpcmVkJ1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gIShzdHJlZXRFcnJvciB8fCBjaXR5RXJyb3IgfHwgbGdhRXJyb3IpO1xyXG4gICAgfVxyXG5cclxufSJdfQ==