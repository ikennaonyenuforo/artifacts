/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as tslib_1 from "tslib";
import { MatTableDataSource } from '@angular/material';
import { Component, Input } from '@angular/core';
import { CardViewKeyValuePairsItemType, CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewFixedKeyValuePairsItemModel } from '../../models/card-view-fixed-keyvaluepairs.model';
let CardViewFixedKeyvaluepairsitemComponent = class CardViewFixedKeyvaluepairsitemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    ngOnChanges() {
        this.values = this.property.value || [];
        this.matTableValues = new MatTableDataSource(this.values);
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    add() {
        this.values.push({ name: '', value: '' });
    }
    remove(index) {
        this.values.splice(index, 1);
        this.save(true);
    }
    onBlur(value) {
        if (value.length) {
            this.save();
        }
    }
    save(remove) {
        const validValues = this.values.filter(i => i.name.length && i.value.length);
        if (remove || validValues.length) {
            this.cardViewUpdateService.update(this.property, validValues);
            this.property.value = validValues;
        }
    }
};
CardViewFixedKeyvaluepairsitemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", CardViewFixedKeyValuePairsItemModel)
], CardViewFixedKeyvaluepairsitemComponent.prototype, "property", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Boolean)
], CardViewFixedKeyvaluepairsitemComponent.prototype, "editable", void 0);
CardViewFixedKeyvaluepairsitemComponent = tslib_1.__decorate([
    Component({
        selector: 'card-view-keyvaluepair',
        template: "<div [attr.data-automation-id]=\"'card-key-value-pairs-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n\r\n    <div *ngIf=\"!isEditable()\" class=\"card-view__key-value-pairs__read-only\">\r\n        <mat-table #table [dataSource]=\"matTableValues\" class=\"mat-elevation-z8\">\r\n            <ng-container matColumnDef=\"name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.name}}</mat-cell>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"value\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.value}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"['name', 'value']\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: ['name', 'value'];\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n\r\n\r\n    <div class=\"card-view__key-value-pairs\" *ngIf=\"isEditable() && values && values.length\">\r\n        <div class=\"card-view__key-value-pairs__row\">\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</div>\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</div>\r\n        </div>\r\n\r\n        <div class=\"card-view__key-value-pairs__row\" *ngFor=\"let item of values; let i = index\">\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           [disabled]=\"true\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-name-input-' + i\"\r\n                           [(ngModel)]=\"values[i].name\">\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           (blur)=\"onBlur(item.value)\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-value-input-' + i\"\r\n                           [(ngModel)]=\"values[i].value\">\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [".card-view__key-value-pairs__col{display:inline-block;width:39%}.card-view__key-value-pairs__col .mat-form-field{width:100%}.card-view__key-value-pairs__read-only .mat-table{box-shadow:none}.card-view__key-value-pairs__read-only .mat-header-row,.card-view__key-value-pairs__read-only .mat-row{padding:0}"]
    }),
    tslib_1.__metadata("design:paramtypes", [CardViewUpdateService])
], CardViewFixedKeyvaluepairsitemComponent);
export { CardViewFixedKeyvaluepairsitemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWZpeGVkLWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzaXRlbS9jYXJkLXZpZXctZml4ZWQta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7R0FlRzs7QUFFSCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUsNkJBQTZCLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUMxRixPQUFPLEVBQUUsbUNBQW1DLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQVF2RyxJQUFhLHVDQUF1QyxHQUFwRCxNQUFhLHVDQUF1QztJQVdoRCxZQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUxoRSxhQUFRLEdBQVksS0FBSyxDQUFDO0lBS3lDLENBQUM7SUFFcEUsV0FBVztRQUNQLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELEdBQUc7UUFDQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFhO1FBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxNQUFNLENBQUMsS0FBSztRQUNSLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNkLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELElBQUksQ0FBQyxNQUFnQjtRQUNqQixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFN0UsSUFBSSxNQUFNLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQztDQUNKLENBQUE7O1lBbEM4QyxxQkFBcUI7O0FBUmhFO0lBREMsS0FBSyxFQUFFO3NDQUNFLG1DQUFtQzt5RUFBQztBQUc5QztJQURDLEtBQUssRUFBRTs7eUVBQ2tCO0FBTmpCLHVDQUF1QztJQU5uRCxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsd0JBQXdCO1FBQ2xDLG1sRkFBMkQ7O0tBRTlELENBQUM7NkNBYTZDLHFCQUFxQjtHQVh2RCx1Q0FBdUMsQ0E2Q25EO1NBN0NZLHVDQUF1QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxNiBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtVHlwZSwgQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdGaXhlZEtleVZhbHVlUGFpcnNJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3LWZpeGVkLWtleXZhbHVlcGFpcnMubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NhcmQtdmlldy1rZXl2YWx1ZXBhaXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1rZXl2YWx1ZXBhaXJzaXRlbS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3Rml4ZWRLZXl2YWx1ZXBhaXJzaXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHByb3BlcnR5OiBDYXJkVmlld0ZpeGVkS2V5VmFsdWVQYWlyc0l0ZW1Nb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZWRpdGFibGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICB2YWx1ZXM6IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1UeXBlW107XHJcbiAgICBtYXRUYWJsZVZhbHVlczogTWF0VGFibGVEYXRhU291cmNlPENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1UeXBlPjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlKSB7fVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCkge1xyXG4gICAgICAgIHRoaXMudmFsdWVzID0gdGhpcy5wcm9wZXJ0eS52YWx1ZSB8fCBbXTtcclxuICAgICAgICB0aGlzLm1hdFRhYmxlVmFsdWVzID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLnZhbHVlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnZhbHVlcy5wdXNoKHsgbmFtZTogJycsIHZhbHVlOiAnJyB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmUoaW5kZXg6IG51bWJlcik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudmFsdWVzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgdGhpcy5zYXZlKHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQmx1cih2YWx1ZSk6IHZvaWQge1xyXG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNhdmUocmVtb3ZlPzogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IHZhbGlkVmFsdWVzID0gdGhpcy52YWx1ZXMuZmlsdGVyKGkgPT4gaS5uYW1lLmxlbmd0aCAmJiBpLnZhbHVlLmxlbmd0aCk7XHJcblxyXG4gICAgICAgIGlmIChyZW1vdmUgfHwgdmFsaWRWYWx1ZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LCB2YWxpZFZhbHVlcyk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcGVydHkudmFsdWUgPSB2YWxpZFZhbHVlcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19