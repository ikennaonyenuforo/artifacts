import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { DefaultLocaleConfig, LOCALE_CONFIG } from './date-range-picker.config';
let LocaleService = class LocaleService {
    constructor(_config) {
        this._config = _config;
    }
    get config() {
        if (!this._config) {
            return DefaultLocaleConfig;
        }
        return Object.assign({}, DefaultLocaleConfig, this._config);
    }
};
LocaleService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
];
LocaleService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__param(0, Inject(LOCALE_CONFIG)),
    tslib_1.__metadata("design:paramtypes", [Object])
], LocaleService);
export { LocaleService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxlcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvbG9jYWxlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFnQixNQUFNLDRCQUE0QixDQUFDO0FBRzlGLElBQWEsYUFBYSxHQUExQixNQUFhLGFBQWE7SUFDekIsWUFBMkMsT0FBcUI7UUFBckIsWUFBTyxHQUFQLE9BQU8sQ0FBYztJQUNoRSxDQUFDO0lBRUQsSUFBSSxNQUFNO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDbEIsT0FBTyxtQkFBbUIsQ0FBQztTQUMzQjtRQUVELHlCQUFXLG1CQUFtQixFQUFLLElBQUksQ0FBQyxPQUFPLEVBQUM7SUFDakQsQ0FBQztDQUNELENBQUE7OzRDQVZhLE1BQU0sU0FBQyxhQUFhOztBQURyQixhQUFhO0lBRHpCLFVBQVUsRUFBRTtJQUVDLG1CQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTs7R0FEdEIsYUFBYSxDQVd6QjtTQVhZLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGVmYXVsdExvY2FsZUNvbmZpZywgTE9DQUxFX0NPTkZJRywgTG9jYWxlQ29uZmlnIH0gZnJvbSAnLi9kYXRlLXJhbmdlLXBpY2tlci5jb25maWcnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTG9jYWxlU2VydmljZSB7XHJcblx0Y29uc3RydWN0b3IoQEluamVjdChMT0NBTEVfQ09ORklHKSBwcml2YXRlIF9jb25maWc6IExvY2FsZUNvbmZpZykge1xyXG5cdH1cclxuXHJcblx0Z2V0IGNvbmZpZygpIHtcclxuXHRcdGlmICghdGhpcy5fY29uZmlnKSB7XHJcblx0XHRcdHJldHVybiBEZWZhdWx0TG9jYWxlQ29uZmlnO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiB7Li4uRGVmYXVsdExvY2FsZUNvbmZpZywgLi4udGhpcy5fY29uZmlnfVxyXG5cdH1cclxufVxyXG4iXX0=