import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab-animation';
let SpeedDialFabComponent = class SpeedDialFabComponent {
    constructor() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
        this.buttonState = [];
    }
    ngOnInit() {
        this.links.forEach(link => this.buttonState.push(link));
    }
    showItems() {
        this.fabTogglerState = 'active';
        this.buttons = this.buttonState;
    }
    hideItems() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
    }
    onToggleFab() {
        this.buttons.length ? this.hideItems() : this.showItems();
    }
};
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Array)
], SpeedDialFabComponent.prototype, "links", void 0);
SpeedDialFabComponent = tslib_1.__decorate([
    Component({
        selector: 'speed-dial',
        template: "<div class=\"fab-container\">\r\n    <button mat-fab class=\"fab-toggler\"\r\n            (click)=\"onToggleFab()\">\r\n        <mat-icon [@fabToggler]=\"{value: fabTogglerState}\">add</mat-icon>\r\n    </button>\r\n    <div [@speedDialStagger]=\"buttons.length\">\r\n        <ng-container *ngFor=\"let btn of buttons\">\r\n            <button mat-mini-fab\r\n                    *jhiHasAnyAuthority=\"btn.roles\"\r\n                    matTooltip=\"{{btn.tooltip}}\"\r\n                    [routerLink]=\"['.', btn.state, 'new']\"\r\n                    class=\"fab-secondary\"\r\n                    color=\"accent\">\r\n                <mat-icon>{{btn.icon}}</mat-icon>\r\n            </button>\r\n        </ng-container>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"fab-dismiss\"\r\n     *ngIf=\"fabTogglerState==='active'\"\r\n     (click)=\"onToggleFab()\">\r\n</div>\r\n",
        animations: speedDialFabAnimations,
        styles: [""]
    }),
    tslib_1.__metadata("design:paramtypes", [])
], SpeedDialFabComponent);
export { SpeedDialFabComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvc3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQVFwRSxJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQVk5QjtRQUpBLG9CQUFlLEdBQUcsVUFBVSxDQUFDO1FBQzdCLFlBQU8sR0FBVSxFQUFFLENBQUM7UUFDcEIsZ0JBQVcsR0FBVSxFQUFFLENBQUM7SUFJeEIsQ0FBQztJQWJELFFBQVE7UUFDSixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQWFELFNBQVM7UUFDTCxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDcEMsQ0FBQztJQUVELFNBQVM7UUFDTCxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztRQUNsQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTtJQUNyQixDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUM5RCxDQUFDO0NBQ0osQ0FBQTtBQXZCRztJQURDLEtBQUssRUFBRTs7b0RBQzhEO0FBTjdELHFCQUFxQjtJQU5qQyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsWUFBWTtRQUN0QixxM0JBQThDO1FBRTlDLFVBQVUsRUFBRSxzQkFBc0I7O0tBQ3JDLENBQUM7O0dBQ1cscUJBQXFCLENBNkJqQztTQTdCWSxxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgc3BlZWREaWFsRmFiQW5pbWF0aW9ucyB9IGZyb20gJy4vc3BlZWQtZGlhbC1mYWItYW5pbWF0aW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdzcGVlZC1kaWFsJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zcGVlZC1kaWFsLWZhYi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9zcGVlZC1kaWFsLWZhYi5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgYW5pbWF0aW9uczogc3BlZWREaWFsRmFiQW5pbWF0aW9uc1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU3BlZWREaWFsRmFiQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubGlua3MuZm9yRWFjaChsaW5rID0+IHRoaXMuYnV0dG9uU3RhdGUucHVzaChsaW5rKSk7XHJcbiAgICB9XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGxpbmtzOiB7IHN0YXRlOiBzdHJpbmcsIGljb246IHN0cmluZywgdG9vbHRpcDogc3RyaW5nLCByb2xlcz86IFtdIH1bXTtcclxuXHJcbiAgICBmYWJUb2dnbGVyU3RhdGUgPSAnaW5hY3RpdmUnO1xyXG4gICAgYnV0dG9uczogYW55W10gPSBbXTtcclxuICAgIGJ1dHRvblN0YXRlOiBhbnlbXSA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBzaG93SXRlbXMoKSB7XHJcbiAgICAgICAgdGhpcy5mYWJUb2dnbGVyU3RhdGUgPSAnYWN0aXZlJztcclxuICAgICAgICB0aGlzLmJ1dHRvbnMgPSB0aGlzLmJ1dHRvblN0YXRlO1xyXG4gICAgfVxyXG5cclxuICAgIGhpZGVJdGVtcygpIHtcclxuICAgICAgICB0aGlzLmZhYlRvZ2dsZXJTdGF0ZSA9ICdpbmFjdGl2ZSc7XHJcbiAgICAgICAgdGhpcy5idXR0b25zID0gW11cclxuICAgIH1cclxuXHJcbiAgICBvblRvZ2dsZUZhYigpIHtcclxuICAgICAgICB0aGlzLmJ1dHRvbnMubGVuZ3RoID8gdGhpcy5oaWRlSXRlbXMoKSA6IHRoaXMuc2hvd0l0ZW1zKCk7XHJcbiAgICB9XHJcbn1cclxuIl19