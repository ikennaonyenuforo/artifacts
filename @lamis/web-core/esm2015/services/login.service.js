import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AccountService } from '../core/auth/account.service';
import { AuthServerProvider } from '../core/auth/auth-jwt.service';
import * as i0 from "@angular/core";
import * as i1 from "../core/auth/account.service";
import * as i2 from "../core/auth/auth-jwt.service";
let LoginService = class LoginService {
    constructor(accountService, authServerProvider) {
        this.accountService = accountService;
        this.authServerProvider = authServerProvider;
    }
    login(credentials, callback) {
        const cb = callback || function () {
        };
        return new Promise((resolve, reject) => {
            this.authServerProvider.login(credentials).subscribe(data => {
                this.accountService.identity(true).then(account => {
                    resolve(data);
                });
                return cb();
            }, err => {
                this.logout();
                reject(err);
                return cb(err);
            });
        });
    }
    loginWithToken(jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    }
    logout() {
        this.authServerProvider.logout().subscribe();
        this.accountService.authenticate(null);
    }
};
LoginService.ctorParameters = () => [
    { type: AccountService },
    { type: AuthServerProvider }
];
LoginService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.ɵɵinject(i1.AccountService), i0.ɵɵinject(i2.AuthServerProvider)); }, token: LoginService, providedIn: "root" });
LoginService = tslib_1.__decorate([
    Injectable({ providedIn: 'root' }),
    tslib_1.__metadata("design:paramtypes", [AccountService, AuthServerProvider])
], LoginService);
export { LoginService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQzlELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtCQUErQixDQUFDOzs7O0FBSW5FLElBQWEsWUFBWSxHQUF6QixNQUFhLFlBQVk7SUFDeEIsWUFBb0IsY0FBOEIsRUFBVSxrQkFBc0M7UUFBOUUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVUsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtJQUNsRyxDQUFDO0lBRUQsS0FBSyxDQUFDLFdBQVcsRUFBRSxRQUFTO1FBQzNCLE1BQU0sRUFBRSxHQUFHLFFBQVEsSUFBSTtRQUN2QixDQUFDLENBQUM7UUFFRixPQUFPLElBQUksT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUNuRCxJQUFJLENBQUMsRUFBRTtnQkFDTixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ2pELE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZixDQUFDLENBQUMsQ0FBQztnQkFDSCxPQUFPLEVBQUUsRUFBRSxDQUFDO1lBQ2IsQ0FBQyxFQUNELEdBQUcsQ0FBQyxFQUFFO2dCQUNMLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDZCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ1osT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsQ0FBQyxDQUNELENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCxjQUFjLENBQUMsR0FBRyxFQUFFLFVBQVU7UUFDN0IsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQsTUFBTTtRQUNMLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN4QyxDQUFDO0NBQ0QsQ0FBQTs7WUFoQ29DLGNBQWM7WUFBOEIsa0JBQWtCOzs7QUFEdEYsWUFBWTtJQUR4QixVQUFVLENBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDLENBQUM7NkNBRUksY0FBYyxFQUE4QixrQkFBa0I7R0FEdEYsWUFBWSxDQWlDeEI7U0FqQ1ksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWNjb3VudFNlcnZpY2UgfSBmcm9tICcuLi9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aFNlcnZlclByb3ZpZGVyIH0gZnJvbSAnLi4vY29yZS9hdXRoL2F1dGgtand0LnNlcnZpY2UnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGFjY291bnRTZXJ2aWNlOiBBY2NvdW50U2VydmljZSwgcHJpdmF0ZSBhdXRoU2VydmVyUHJvdmlkZXI6IEF1dGhTZXJ2ZXJQcm92aWRlcikge1xyXG5cdH1cclxuXHJcblx0bG9naW4oY3JlZGVudGlhbHMsIGNhbGxiYWNrPykge1xyXG5cdFx0Y29uc3QgY2IgPSBjYWxsYmFjayB8fCBmdW5jdGlvbiAoKSB7XHJcblx0XHR9O1xyXG5cclxuXHRcdHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblx0XHRcdHRoaXMuYXV0aFNlcnZlclByb3ZpZGVyLmxvZ2luKGNyZWRlbnRpYWxzKS5zdWJzY3JpYmUoXHJcblx0XHRcdFx0ZGF0YSA9PiB7XHJcblx0XHRcdFx0XHR0aGlzLmFjY291bnRTZXJ2aWNlLmlkZW50aXR5KHRydWUpLnRoZW4oYWNjb3VudCA9PiB7XHJcblx0XHRcdFx0XHRcdHJlc29sdmUoZGF0YSk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdHJldHVybiBjYigpO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZXJyID0+IHtcclxuXHRcdFx0XHRcdHRoaXMubG9nb3V0KCk7XHJcblx0XHRcdFx0XHRyZWplY3QoZXJyKTtcclxuXHRcdFx0XHRcdHJldHVybiBjYihlcnIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0KTtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bG9naW5XaXRoVG9rZW4oand0LCByZW1lbWJlck1lKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5hdXRoU2VydmVyUHJvdmlkZXIubG9naW5XaXRoVG9rZW4oand0LCByZW1lbWJlck1lKTtcclxuXHR9XHJcblxyXG5cdGxvZ291dCgpIHtcclxuXHRcdHRoaXMuYXV0aFNlcnZlclByb3ZpZGVyLmxvZ291dCgpLnN1YnNjcmliZSgpO1xyXG5cdFx0dGhpcy5hY2NvdW50U2VydmljZS5hdXRoZW50aWNhdGUobnVsbCk7XHJcblx0fVxyXG59XHJcbiJdfQ==