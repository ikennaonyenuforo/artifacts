import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
function _window() {
    // return the global native browser window object
    return window;
}
let WindowRef = class WindowRef {
    get nativeWindow() {
        return _window();
    }
};
WindowRef.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function WindowRef_Factory() { return new WindowRef(); }, token: WindowRef, providedIn: "root" });
WindowRef = tslib_1.__decorate([
    Injectable({ providedIn: 'root' })
], WindowRef);
export { WindowRef };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2luZG93LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy93aW5kb3cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFFM0MsU0FBUyxPQUFPO0lBQ2YsaURBQWlEO0lBQ2pELE9BQU8sTUFBTSxDQUFDO0FBQ2YsQ0FBQztBQUdELElBQWEsU0FBUyxHQUF0QixNQUFhLFNBQVM7SUFDckIsSUFBSSxZQUFZO1FBQ2YsT0FBTyxPQUFPLEVBQUUsQ0FBQztJQUNsQixDQUFDO0NBQ0QsQ0FBQTs7QUFKWSxTQUFTO0lBRHJCLFVBQVUsQ0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsQ0FBQztHQUNwQixTQUFTLENBSXJCO1NBSlksU0FBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmZ1bmN0aW9uIF93aW5kb3coKTogYW55IHtcclxuXHQvLyByZXR1cm4gdGhlIGdsb2JhbCBuYXRpdmUgYnJvd3NlciB3aW5kb3cgb2JqZWN0XHJcblx0cmV0dXJuIHdpbmRvdztcclxufVxyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBXaW5kb3dSZWYge1xyXG5cdGdldCBuYXRpdmVXaW5kb3coKTogYW55IHtcclxuXHRcdHJldHVybiBfd2luZG93KCk7XHJcblx0fVxyXG59XHJcbiJdfQ==