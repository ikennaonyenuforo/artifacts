import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from './login.service';
import { JhiEventManager } from 'ng-jhipster';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
let LoginAuthenticationService = class LoginAuthenticationService {
    constructor(router, loginService, $localStorage, $sessionStorage, eventManager, stateStorageService, injector) {
        this.router = router;
        this.loginService = loginService;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
        this.eventManager = eventManager;
        this.stateStorageService = stateStorageService;
        this.injector = injector;
        /*this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
        this.stateStorageService = this.injector.get(StateStorageService);
        this.eventManager = this.injector.get(JhiEventManager);*/
    }
    setRedirect(value) {
    }
    isEcmLoggedIn() {
        return false;
    }
    isBpmLoggedIn() {
        return false;
    }
    isOauth() {
        return false;
    }
    getRedirect() {
        return null;
    }
    login(username, password, rememberMe = false) {
        this.loginService
            .login({
            username: username,
            password: password,
            rememberMe: rememberMe
        })
            .then((data) => {
            if (this.router.url === '/account/register' || (/^\/account\/activate\//.test(this.router.url)) ||
                (/^\/account\/reset\//.test(this.router.url))) {
                this.router.navigate(['']);
            }
            this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // // since login is successful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
                this.stateStorageService.storeUrl('');
                this.router.navigate([redirect]);
            }
            else {
                this.router.navigate(['/dashboard']);
            }
        });
        return of(true);
    }
};
LoginAuthenticationService.ctorParameters = () => [
    { type: Router },
    { type: LoginService },
    { type: LocalStorageService },
    { type: SessionStorageService },
    { type: JhiEventManager },
    { type: StateStorageService },
    { type: Injector }
];
LoginAuthenticationService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [Router,
        LoginService,
        LocalStorageService,
        SessionStorageService,
        JhiEventManager,
        StateStorageService,
        Injector])
], LoginAuthenticationService);
export { LoginAuthenticationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUd2RSxJQUFhLDBCQUEwQixHQUF2QyxNQUFhLDBCQUEwQjtJQUVuQyxZQUFvQixNQUFjLEVBQ2QsWUFBMEIsRUFDMUIsYUFBa0MsRUFDbEMsZUFBc0MsRUFDdEMsWUFBNkIsRUFDN0IsbUJBQXdDLEVBQ3hDLFFBQWtCO1FBTmxCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBcUI7UUFDbEMsb0JBQWUsR0FBZixlQUFlLENBQXVCO1FBQ3RDLGlCQUFZLEdBQVosWUFBWSxDQUFpQjtRQUM3Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hDLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEM7OztpRUFHeUQ7SUFDN0QsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFVO0lBRXRCLENBQUM7SUFFRCxhQUFhO1FBQ1QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELGFBQWE7UUFDVCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsT0FBTztRQUNILE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELEtBQUssQ0FBQyxRQUFnQixFQUFFLFFBQWdCLEVBQUUsYUFBc0IsS0FBSztRQUNqRSxJQUFJLENBQUMsWUFBWTthQUNaLEtBQUssQ0FBQztZQUNILFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFVBQVUsRUFBRSxVQUFVO1NBQ3pCLENBQUM7YUFDRCxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNYLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEtBQUssbUJBQW1CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDM0YsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDeEIsSUFBSSxFQUFFLHVCQUF1QjtnQkFDN0IsT0FBTyxFQUFFLGdDQUFnQzthQUM1QyxDQUFDLENBQUM7WUFDSCxpR0FBaUc7WUFDakcsbUZBQW1GO1lBQ25GLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNuRCxJQUFJLFFBQVEsRUFBRTtnQkFDVixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7YUFDcEM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2FBQ3hDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDUCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQixDQUFDO0NBQ0osQ0FBQTs7WUE3RCtCLE1BQU07WUFDQSxZQUFZO1lBQ1gsbUJBQW1CO1lBQ2pCLHFCQUFxQjtZQUN4QixlQUFlO1lBQ1IsbUJBQW1CO1lBQzlCLFFBQVE7O0FBUjdCLDBCQUEwQjtJQUR0QyxVQUFVLEVBQUU7NkNBR21CLE1BQU07UUFDQSxZQUFZO1FBQ1gsbUJBQW1CO1FBQ2pCLHFCQUFxQjtRQUN4QixlQUFlO1FBQ1IsbUJBQW1CO1FBQzlCLFFBQVE7R0FSN0IsMEJBQTBCLENBK0R0QztTQS9EWSwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9hdXRoL3N0YXRlLXN0b3JhZ2Uuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ2luU2VydmljZSB9IGZyb20gJy4vbG9naW4uc2VydmljZSc7XHJcbmltcG9ydCB7IEpoaUV2ZW50TWFuYWdlciB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIExvZ2luQXV0aGVudGljYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgJGxvY2FsU3RvcmFnZTogTG9jYWxTdG9yYWdlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgJHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGV2ZW50TWFuYWdlcjogSmhpRXZlbnRNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBzdGF0ZVN0b3JhZ2VTZXJ2aWNlOiBTdGF0ZVN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IpIHtcclxuICAgICAgICAvKnRoaXMuJGxvY2FsU3RvcmFnZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KExvY2FsU3RvcmFnZVNlcnZpY2UpO1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlID0gdGhpcy5pbmplY3Rvci5nZXQoU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlKTtcclxuICAgICAgICB0aGlzLnN0YXRlU3RvcmFnZVNlcnZpY2UgPSB0aGlzLmluamVjdG9yLmdldChTdGF0ZVN0b3JhZ2VTZXJ2aWNlKTtcclxuICAgICAgICB0aGlzLmV2ZW50TWFuYWdlciA9IHRoaXMuaW5qZWN0b3IuZ2V0KEpoaUV2ZW50TWFuYWdlcik7Ki9cclxuICAgIH1cclxuXHJcbiAgICBzZXRSZWRpcmVjdCh2YWx1ZTogYW55KXtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgaXNFY21Mb2dnZWRJbigpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNCcG1Mb2dnZWRJbigpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNPYXV0aCgpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVkaXJlY3QoKSB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZywgcmVtZW1iZXJNZTogYm9vbGVhbiA9IGZhbHNlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICB0aGlzLmxvZ2luU2VydmljZVxyXG4gICAgICAgICAgICAubG9naW4oe1xyXG4gICAgICAgICAgICAgICAgdXNlcm5hbWU6IHVzZXJuYW1lLFxyXG4gICAgICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkLFxyXG4gICAgICAgICAgICAgICAgcmVtZW1iZXJNZTogcmVtZW1iZXJNZVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMucm91dGVyLnVybCA9PT0gJy9hY2NvdW50L3JlZ2lzdGVyJyB8fCAoL15cXC9hY2NvdW50XFwvYWN0aXZhdGVcXC8vLnRlc3QodGhpcy5yb3V0ZXIudXJsKSkgfHxcclxuICAgICAgICAgICAgICAgICAgICAoL15cXC9hY2NvdW50XFwvcmVzZXRcXC8vLnRlc3QodGhpcy5yb3V0ZXIudXJsKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJyddKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRNYW5hZ2VyLmJyb2FkY2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2F1dGhlbnRpY2F0aW9uU3VjY2VzcycsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDogJ1NlbmRpbmcgQXV0aGVudGljYXRpb24gU3VjY2VzcydcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgLy8gLy8gcHJldmlvdXNTdGF0ZSB3YXMgc2V0IGluIHRoZSBhdXRoRXhwaXJlZEludGVyY2VwdG9yIGJlZm9yZSBiZWluZyByZWRpcmVjdGVkIHRvIGxvZ2luIG1vZGFsLlxyXG4gICAgICAgICAgICAgICAgLy8gLy8gc2luY2UgbG9naW4gaXMgc3VjY2Vzc2Z1bCwgZ28gdG8gc3RvcmVkIHByZXZpb3VzU3RhdGUgYW5kIGNsZWFyIHByZXZpb3VzU3RhdGVcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlZGlyZWN0ID0gdGhpcy5zdGF0ZVN0b3JhZ2VTZXJ2aWNlLmdldFVybCgpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlZGlyZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZVN0b3JhZ2VTZXJ2aWNlLnN0b3JlVXJsKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbcmVkaXJlY3RdKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvZGFzaGJvYXJkJ10pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gb2YodHJ1ZSk7XHJcbiAgICB9XHJcbn1cclxuIl19