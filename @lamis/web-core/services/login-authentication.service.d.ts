import { Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from './login.service';
import { JhiEventManager } from 'ng-jhipster';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
export declare class LoginAuthenticationService {
    private router;
    private loginService;
    private $localStorage;
    private $sessionStorage;
    private eventManager;
    private stateStorageService;
    private injector;
    constructor(router: Router, loginService: LoginService, $localStorage: LocalStorageService, $sessionStorage: SessionStorageService, eventManager: JhiEventManager, stateStorageService: StateStorageService, injector: Injector);
    setRedirect(value: any): void;
    isEcmLoggedIn(): boolean;
    isBpmLoggedIn(): boolean;
    isOauth(): boolean;
    getRedirect(): any;
    login(username: string, password: string, rememberMe?: boolean): Observable<any>;
}
