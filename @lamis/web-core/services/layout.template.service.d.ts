import { HttpClient } from '@angular/common/http';
import { ServerApiUrlConfig } from '../app.constants';
export declare class LayoutTemplateService {
    private http;
    private serverUrl;
    resourceUrl: string;
    constructor(http: HttpClient, serverUrl: ServerApiUrlConfig);
    getTemplate(templateId: string): import("rxjs").Observable<Object>;
}
