import { OnDestroy, OnInit } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
export declare class AlertComponent implements OnInit, OnDestroy {
    private notification;
    private alertService;
    alerts: any[];
    constructor(notification: NotificationService, alertService: JhiAlertService);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
