import { OnDestroy } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';
import { NotificationService } from '@alfresco/adf-core';
export declare class AlertErrorComponent implements OnDestroy {
    private notification;
    private eventManager;
    cleanHttpErrorListener: Subscription;
    constructor(notification: NotificationService, eventManager: JhiEventManager);
    ngOnDestroy(): void;
    addErrorAlert(message: any, key?: any, data?: any): void;
}
