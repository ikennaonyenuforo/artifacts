import { OnChanges } from '@angular/core';
import { CardViewHtmlTextItemModel } from '../../models/card-view-html-text-item.model';
export declare class CardViewHtmlTextItemComponent implements OnChanges {
    property: CardViewHtmlTextItemModel;
    constructor();
    ngOnChanges(): void;
}
