/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { MatTableDataSource } from '@angular/material';
import { OnChanges } from '@angular/core';
import { CardViewKeyValuePairsItemType, CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewFixedKeyValuePairsItemModel } from '../../models/card-view-fixed-keyvaluepairs.model';
export declare class CardViewFixedKeyvaluepairsitemComponent implements OnChanges {
    private cardViewUpdateService;
    property: CardViewFixedKeyValuePairsItemModel;
    editable: boolean;
    values: CardViewKeyValuePairsItemType[];
    matTableValues: MatTableDataSource<CardViewKeyValuePairsItemType>;
    constructor(cardViewUpdateService: CardViewUpdateService);
    ngOnChanges(): void;
    isEditable(): boolean;
    add(): void;
    remove(index: number): void;
    onBlur(value: any): void;
    save(remove?: boolean): void;
}
