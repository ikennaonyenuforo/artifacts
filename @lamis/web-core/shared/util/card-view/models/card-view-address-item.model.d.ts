import { CardViewBaseItemModel, CardViewItem, DynamicComponentModel } from '@alfresco/adf-core';
import { CardItemAddressItemProperties } from './card-item-address-item.properties';
export declare class CardViewAddressItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string;
    constructor(obj: CardItemAddressItemProperties);
    readonly displayValue: any;
    getValue(): string;
}
