import { CardViewBaseItemModel, CardViewItem, CardViewTextItemProperties, DynamicComponentModel } from '@alfresco/adf-core';
export declare class CardViewHtmlTextItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string;
    constructor(obj: CardViewTextItemProperties);
    readonly displayValue: any;
}
