import { CardViewBaseItemModel, CardViewItem, DynamicComponentModel } from '@alfresco/adf-core';
import { CardViewPhoneItemProperties } from './card-view-phone-item.properties';
export declare class CardViewPhoneItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string;
    constructor(obj: CardViewPhoneItemProperties);
    readonly displayValue: any;
    getValue(): string;
}
