import { CardItemTypeService } from '@alfresco/adf-core';
import { CardViewHtmlTextItemComponent } from './components/card-view-html-text-item/card-view-html-text-item.component';
import { CardViewNameItemComponent } from './components/card-view-name-item/card-view-name-item.component';
import { CardViewAddressItemComponent } from './components/card-view-address-item/card-view-address-item.component';
import { CardViewFixedKeyvaluepairsitemComponent } from './components/card-view-fixed-keyvaluepairsitem/card-view-fixed-keyvaluepairsitem.component';
import { CardViewPhoneItemComponent } from './components/card-view-phone-item/card-view-phone-item.component';
export declare function components(): (typeof CardViewHtmlTextItemComponent | typeof CardViewNameItemComponent | typeof CardViewAddressItemComponent | typeof CardViewFixedKeyvaluepairsitemComponent | typeof CardViewPhoneItemComponent)[];
export declare class CardViewModule {
    private cardItemTypeService;
    constructor(cardItemTypeService: CardItemTypeService);
}
