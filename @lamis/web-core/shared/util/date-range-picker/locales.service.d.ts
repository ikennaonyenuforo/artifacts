import { LocaleConfig } from './date-range-picker.config';
export declare class LocaleService {
    private _config;
    constructor(_config: LocaleConfig);
    readonly config: LocaleConfig;
}
