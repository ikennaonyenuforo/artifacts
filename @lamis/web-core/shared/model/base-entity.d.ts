export interface IBaseEntity {
    id?: number | string;
}
export interface IAggregate {
    field?: string;
    key?: string;
}
export declare class Aggregate implements IAggregate {
    field: string;
    key: any;
    constructor(field: string, key: any);
}
export declare function remove<T extends IBaseEntity>(array: T[], element: T): T[];
export declare function clear<T>(array: T[]): T[];
export declare function contains<T extends IBaseEntity>(array: T[], element: T): boolean;
export declare function entityCompare<T extends IBaseEntity>(e1: T, e2: T): boolean;
export declare function enumCompare(e1: any, e2: any): boolean;
export declare function replace<T extends IBaseEntity>(array: T[], element: T): T[];
