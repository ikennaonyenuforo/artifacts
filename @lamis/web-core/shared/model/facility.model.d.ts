import { IBaseEntity } from './base-entity';
import { ILGA } from './lga.model';
export declare enum FacilityType {
    DOCTOR_OFFICE = 0,
    PRIMARY_CARE = 1,
    CLINIC = 2,
    HOSPITAL = 3,
    SPECIALIZED = 4,
    NURSING_HOME = 5,
    HOSPICE = 6,
    RURAL = 7
}
export declare enum PublicLevel {
    PRIVATE = 0,
    PUBLIC = 1,
    MIXED = 2
}
export interface Facility extends IBaseEntity {
    id?: number;
    name?: string;
    code?: string;
    lga?: ILGA;
    type?: FacilityType;
    publicLevel?: PublicLevel;
    teaching?: boolean;
    extra?: string;
}
