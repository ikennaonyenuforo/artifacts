import { IBaseEntity } from './base-entity';
export interface IState extends IBaseEntity {
    id?: number;
    name?: string;
}
export declare class State implements IState {
    id?: number;
    name?: string;
    constructor(id?: number, name?: string);
}
