import { IBaseEntity } from './base-entity';
import { IState } from './state.model';
export interface ILGA extends IBaseEntity {
    id?: number;
    name?: string;
    state?: IState;
}
export declare class LGA implements ILGA {
    id?: number;
    name?: string;
    state?: IState;
    constructor(id?: number, name?: string, state?: IState);
}
