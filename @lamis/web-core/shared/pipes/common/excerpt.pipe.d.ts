import { PipeTransform } from "@angular/core";
export declare class ExcerptPipe implements PipeTransform {
    transform(text: string, limit?: number): string;
}
