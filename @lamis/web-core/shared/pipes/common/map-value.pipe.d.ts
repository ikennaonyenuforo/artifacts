import { PipeTransform } from "@angular/core";
export declare class MapValuesPipe implements PipeTransform {
    transform(value: any, args?: any[]): Object[];
}
