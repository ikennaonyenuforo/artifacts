import { PipeTransform } from "@angular/core";
export declare class RelativeTimePipe implements PipeTransform {
    transform(value: Date): string;
}
