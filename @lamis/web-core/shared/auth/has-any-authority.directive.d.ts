import { TemplateRef, ViewContainerRef } from '@angular/core';
import { AccountService } from '../../core/auth/account.service';
/**
 * @whatItDoes Conditionally includes an HTML element if current user has any
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *jhiHasAnyAuthority="'ROLE_ADMIN'">...</some-element>
 *
 *     <some-element *jhiHasAnyAuthority="['ROLE_ADMIN', 'ROLE_USER']">...</some-element>
 * ```
 */
export declare class HasAnyAuthorityDirective {
    private accountService;
    private templateRef;
    private viewContainerRef;
    private authorities;
    constructor(accountService: AccountService, templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef);
    jhiHasAnyAuthority: string | string[];
    private updateView;
}
