import { NotificationService } from '@alfresco/adf-core';
import { OnInit } from '@angular/core';
import { MatButton, MatProgressBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { IBaseEntity } from '../shared/model/base-entity';
export declare abstract class BaseEntityEditComponent<T extends IBaseEntity> implements OnInit {
    protected notification: NotificationService;
    protected route: ActivatedRoute;
    progressBar: MatProgressBar;
    submitButton: MatButton;
    entity: T;
    isSaving: boolean;
    error: boolean;
    constructor(notification: NotificationService, route: ActivatedRoute);
    ngOnInit(): void;
    previousState(): void;
    save(valid?: any): void;
    private subscribeToSaveResponse;
    private onSaveSuccess;
    private onSaveError;
    protected onError(errorMessage: string): void;
    entityCompare(s1: any, s2: any): boolean;
    abstract getService(): any;
    abstract createEntity(): T;
}
