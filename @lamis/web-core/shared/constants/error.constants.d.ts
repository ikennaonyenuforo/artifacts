export declare const PROBLEM_BASE_URL = "https://www.jhipster.tech/problem";
export declare const EMAIL_ALREADY_USED_TYPE: string;
export declare const LOGIN_ALREADY_USED_TYPE: string;
export declare const EMAIL_NOT_FOUND_TYPE: string;
