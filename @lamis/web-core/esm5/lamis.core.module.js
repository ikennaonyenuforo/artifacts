import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SERVER_API_URL_CONFIG } from './app.constants';
import { RxStompConfig } from './services/rx-stomp.config';
import { LamisSharedModule } from './shared/lamis-shared.module';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { LOCALE_CONFIG } from './shared/util/date-range-picker/date-range-picker.config';
import { LocaleService } from './shared/util/date-range-picker/locales.service';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
var LamisCoreModule = /** @class */ (function () {
    function LamisCoreModule() {
    }
    LamisCoreModule_1 = LamisCoreModule;
    LamisCoreModule.forRoot = function (serverApiUrlConfig, config) {
        return {
            ngModule: LamisCoreModule_1,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                { provide: LOCALE_CONFIG, useValue: config || {} },
                { provide: LocaleService, useClass: LocaleService, deps: [LOCALE_CONFIG] },
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    };
    var LamisCoreModule_1;
    LamisCoreModule = LamisCoreModule_1 = tslib_1.__decorate([
        NgModule({
            declarations: [],
            imports: [
                CommonModule,
                LamisSharedModule
            ],
            exports: [
                LamisSharedModule
            ],
            providers: []
        })
    ], LamisCoreModule);
    return LamisCoreModule;
}());
export { LamisCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMuY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJsYW1pcy5jb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUF1QixRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxxQkFBcUIsRUFBc0IsTUFBTSxpQkFBaUIsQ0FBQztBQUM1RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDakUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDdkYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3hGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxhQUFhLEVBQWdCLE1BQU0sMERBQTBELENBQUM7QUFDdkcsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBQ2hGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQWFwRztJQUFBO0lBMkJBLENBQUM7d0JBM0JZLGVBQWU7SUFDakIsdUJBQU8sR0FBZCxVQUFlLGtCQUFzQyxFQUFFLE1BQXFCO1FBQ3hFLE9BQU87WUFDSCxRQUFRLEVBQUUsaUJBQWU7WUFDekIsU0FBUyxFQUFFO2dCQUNQLHNCQUFzQjtnQkFDdEIsZUFBZTtnQkFDZix1QkFBdUI7Z0JBQ3ZCLHVCQUF1QjtnQkFDdkI7b0JBQ0ksT0FBTyxFQUFFLHFCQUFxQjtvQkFDOUIsUUFBUSxFQUFFLGtCQUFrQjtpQkFDL0I7Z0JBQ0QsRUFBQyxPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVEsRUFBRSxNQUFNLElBQUksRUFBRSxFQUFDO2dCQUNoRCxFQUFDLE9BQU8sRUFBRSxhQUFhLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxhQUFhLENBQUMsRUFBQztnQkFDeEU7b0JBQ0ksT0FBTyxFQUFFLHVCQUF1QjtvQkFDaEMsUUFBUSxFQUFFLGFBQWE7aUJBQzFCO2dCQUNEO29CQUNJLE9BQU8sRUFBRSxjQUFjO29CQUN2QixVQUFVLEVBQUUscUJBQXFCO29CQUNqQyxJQUFJLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztpQkFDbEM7YUFDSjtTQUNKLENBQUM7SUFDTixDQUFDOztJQTFCUSxlQUFlO1FBWDNCLFFBQVEsQ0FBQztZQUNOLFlBQVksRUFBRSxFQUFFO1lBQ2hCLE9BQU8sRUFBRTtnQkFDTCxZQUFZO2dCQUNaLGlCQUFpQjthQUNwQjtZQUNELE9BQU8sRUFBRTtnQkFDTCxpQkFBaUI7YUFDcEI7WUFDRCxTQUFTLEVBQUUsRUFBRTtTQUNoQixDQUFDO09BQ1csZUFBZSxDQTJCM0I7SUFBRCxzQkFBQztDQUFBLEFBM0JELElBMkJDO1NBM0JZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBTRVJWRVJfQVBJX1VSTF9DT05GSUcsIFNlcnZlckFwaVVybENvbmZpZyB9IGZyb20gJy4vYXBwLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IFJ4U3RvbXBDb25maWcgfSBmcm9tICcuL3NlcnZpY2VzL3J4LXN0b21wLmNvbmZpZyc7XHJcbmltcG9ydCB7IExhbWlzU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi9zaGFyZWQvbGFtaXMtc2hhcmVkLm1vZHVsZSc7XHJcbmltcG9ydCB7IEF1dGhFeHBpcmVkSW50ZXJjZXB0b3IgfSBmcm9tICcuL2Jsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLWV4cGlyZWQuaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBBdXRoSW50ZXJjZXB0b3IgfSBmcm9tICcuL2Jsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uSW50ZXJjZXB0b3IgfSBmcm9tICcuL2Jsb2Nrcy9pbnRlcmNlcHRvci9ub3RpZmljYXRpb24uaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBFcnJvckhhbmRsZXJJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2Vycm9yaGFuZGxlci5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IExPQ0FMRV9DT05GSUcsIExvY2FsZUNvbmZpZyB9IGZyb20gJy4vc2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvZGF0ZS1yYW5nZS1waWNrZXIuY29uZmlnJztcclxuaW1wb3J0IHsgTG9jYWxlU2VydmljZSB9IGZyb20gJy4vc2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvbG9jYWxlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZVJ4U3RvbXBDb25maWcsIFJ4U3RvbXBTZXJ2aWNlLCByeFN0b21wU2VydmljZUZhY3RvcnkgfSBmcm9tICdAc3RvbXAvbmcyLXN0b21wanMnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW10sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIExhbWlzU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIExhbWlzU2hhcmVkTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFtaXNDb3JlTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KHNlcnZlckFwaVVybENvbmZpZzogU2VydmVyQXBpVXJsQ29uZmlnLCBjb25maWc/OiBMb2NhbGVDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBuZ01vZHVsZTogTGFtaXNDb3JlTW9kdWxlLFxyXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICAgICAgICAgIEF1dGhFeHBpcmVkSW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgICAgICBBdXRoSW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgICAgICBFcnJvckhhbmRsZXJJbnRlcmNlcHRvcixcclxuICAgICAgICAgICAgICAgIE5vdGlmaWNhdGlvbkludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IFNFUlZFUl9BUElfVVJMX0NPTkZJRyxcclxuICAgICAgICAgICAgICAgICAgICB1c2VWYWx1ZTogc2VydmVyQXBpVXJsQ29uZmlnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge3Byb3ZpZGU6IExPQ0FMRV9DT05GSUcsIHVzZVZhbHVlOiBjb25maWcgfHwge319LFxyXG4gICAgICAgICAgICAgICAge3Byb3ZpZGU6IExvY2FsZVNlcnZpY2UsIHVzZUNsYXNzOiBMb2NhbGVTZXJ2aWNlLCBkZXBzOiBbTE9DQUxFX0NPTkZJR119LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IEluamVjdGFibGVSeFN0b21wQ29uZmlnLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZVZhbHVlOiBSeFN0b21wQ29uZmlnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3ZpZGU6IFJ4U3RvbXBTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhY3Rvcnk6IHJ4U3RvbXBTZXJ2aWNlRmFjdG9yeSxcclxuICAgICAgICAgICAgICAgICAgICBkZXBzOiBbSW5qZWN0YWJsZVJ4U3RvbXBDb25maWddXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==