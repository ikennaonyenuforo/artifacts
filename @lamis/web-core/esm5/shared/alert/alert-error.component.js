import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
var AlertErrorComponent = /** @class */ (function () {
    function AlertErrorComponent(notification, eventManager) {
        var _this = this;
        this.notification = notification;
        this.eventManager = eventManager;
        /* tslint:enable */
        this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', function (response) {
            var i;
            var httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    _this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    var arr = httpErrorResponse.headers.keys();
                    var errorHeader_1 = null;
                    var entityKey_1 = null;
                    arr.forEach(function (entry) {
                        if (entry.endsWith('app-error')) {
                            errorHeader_1 = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.endsWith('app-params')) {
                            entityKey_1 = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader_1) {
                        _this.addErrorAlert(errorHeader_1, errorHeader_1, { entityName: entityKey_1 });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        var fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            var fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            var convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            var fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                            _this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName: fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        _this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        _this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                case 404:
                    _this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        _this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        _this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    AlertErrorComponent.prototype.ngOnDestroy = function () {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
        }
    };
    AlertErrorComponent.prototype.addErrorAlert = function (message, key, data) {
        this.notification.openSnackMessage(message, 5000);
    };
    AlertErrorComponent.ctorParameters = function () { return [
        { type: NotificationService },
        { type: JhiEventManager }
    ]; };
    AlertErrorComponent = tslib_1.__decorate([
        Component({
            selector: 'alert-error',
            template: ""
        }),
        tslib_1.__metadata("design:paramtypes", [NotificationService, JhiEventManager])
    ], AlertErrorComponent);
    return AlertErrorComponent;
}());
export { AlertErrorComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtZXJyb3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRTlDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTXpEO0lBR0ksNkJBQW9CLFlBQWlDLEVBQVUsWUFBNkI7UUFBNUYsaUJBd0RDO1FBeERtQixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFDeEYsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxZQUFZLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxVQUFBLFFBQVE7WUFDMUUsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFNLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDM0MsUUFBUSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQzlCLDJDQUEyQztnQkFDM0MsS0FBSyxDQUFDO29CQUNGLEtBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztvQkFDekUsTUFBTTtnQkFFVixLQUFLLEdBQUc7b0JBQ0osSUFBTSxHQUFHLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUM3QyxJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLElBQUksV0FBUyxHQUFHLElBQUksQ0FBQztvQkFDckIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7d0JBQ2IsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUM3QixhQUFXLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdEQ7NkJBQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFOzRCQUNyQyxXQUFTLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDcEQ7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxhQUFXLEVBQUU7d0JBQ2IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFXLEVBQUUsYUFBVyxFQUFFLEVBQUMsVUFBVSxFQUFFLFdBQVMsRUFBQyxDQUFDLENBQUM7cUJBQ3pFO3lCQUFNLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO3dCQUM5RSxJQUFNLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO3dCQUN4RCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3JDLElBQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbEMsdUdBQXVHOzRCQUN2RyxJQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ2xFLElBQU0sU0FBUyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbkYsS0FBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLEdBQUcsR0FBRyxFQUFFLFFBQVEsR0FBRyxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUMsU0FBUyxXQUFBLEVBQUMsQ0FBQyxDQUFDO3lCQUN4RztxQkFDSjt5QkFBTSxJQUFJLGlCQUFpQixDQUFDLEtBQUssS0FBSyxFQUFFLElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTt3QkFDMUUsS0FBSSxDQUFDLGFBQWEsQ0FDZCxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUMvQixpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUMvQixpQkFBaUIsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUNqQyxDQUFDO3FCQUNMO3lCQUFNO3dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQy9DO29CQUNELE1BQU07Z0JBRVYsS0FBSyxHQUFHO29CQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLHFCQUFxQixDQUFDLENBQUM7b0JBQ3ZELE1BQU07Z0JBRVY7b0JBQ0ksSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7d0JBQ25FLEtBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUN2RDt5QkFBTTt3QkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQzthQUNSO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUNBQVcsR0FBWDtRQUNJLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssSUFBSSxFQUFFO1lBQ25GLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1NBQzFEO0lBQ0wsQ0FBQztJQUVELDJDQUFhLEdBQWIsVUFBYyxPQUFPLEVBQUUsR0FBSSxFQUFFLElBQUs7UUFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Z0JBbEVpQyxtQkFBbUI7Z0JBQXdCLGVBQWU7O0lBSG5GLG1CQUFtQjtRQUovQixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsYUFBYTtZQUN2QixRQUFRLEVBQUUsRUFBRTtTQUNmLENBQUM7aURBSW9DLG1CQUFtQixFQUF3QixlQUFlO09BSG5GLG1CQUFtQixDQXNFL0I7SUFBRCwwQkFBQztDQUFBLEFBdEVELElBc0VDO1NBdEVZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEpoaUV2ZW50TWFuYWdlciB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FsZXJ0LWVycm9yJyxcclxuICAgIHRlbXBsYXRlOiBgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWxlcnRFcnJvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XHJcbiAgICBjbGVhbkh0dHBFcnJvckxpc3RlbmVyOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBub3RpZmljYXRpb246IE5vdGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgZXZlbnRNYW5hZ2VyOiBKaGlFdmVudE1hbmFnZXIpIHtcclxuICAgICAgICAvKiB0c2xpbnQ6ZW5hYmxlICovXHJcbiAgICAgICAgdGhpcy5jbGVhbkh0dHBFcnJvckxpc3RlbmVyID0gZXZlbnRNYW5hZ2VyLnN1YnNjcmliZSgnYXBwLmh0dHBFcnJvcicsIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgbGV0IGk7XHJcbiAgICAgICAgICAgIGNvbnN0IGh0dHBFcnJvclJlc3BvbnNlID0gcmVzcG9uc2UuY29udGVudDtcclxuICAgICAgICAgICAgc3dpdGNoIChodHRwRXJyb3JSZXNwb25zZS5zdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgIC8vIGNvbm5lY3Rpb24gcmVmdXNlZCwgc2VydmVyIG5vdCByZWFjaGFibGVcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoJ1NlcnZlciBub3QgcmVhY2hhYmxlJywgJ2Vycm9yLnNlcnZlci5ub3QucmVhY2hhYmxlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgY2FzZSA0MDA6XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXJyID0gaHR0cEVycm9yUmVzcG9uc2UuaGVhZGVycy5rZXlzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGVycm9ySGVhZGVyID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZW50aXR5S2V5ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBhcnIuZm9yRWFjaChlbnRyeSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbnRyeS5lbmRzV2l0aCgnYXBwLWVycm9yJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9ySGVhZGVyID0gaHR0cEVycm9yUmVzcG9uc2UuaGVhZGVycy5nZXQoZW50cnkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVudHJ5LmVuZHNXaXRoKCdhcHAtcGFyYW1zJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudGl0eUtleSA9IGh0dHBFcnJvclJlc3BvbnNlLmhlYWRlcnMuZ2V0KGVudHJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvckhlYWRlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoZXJyb3JIZWFkZXIsIGVycm9ySGVhZGVyLCB7ZW50aXR5TmFtZTogZW50aXR5S2V5fSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChodHRwRXJyb3JSZXNwb25zZS5lcnJvciAhPT0gJycgJiYgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IuZmllbGRFcnJvcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRFcnJvcnMgPSBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5maWVsZEVycm9ycztcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGZpZWxkRXJyb3JzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZEVycm9yID0gZmllbGRFcnJvcnNbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBjb252ZXJ0ICdzb21ldGhpbmdbMTRdLm90aGVyWzRdLmlkJyB0byAnc29tZXRoaW5nW10ub3RoZXJbXS5pZCcgc28gdHJhbnNsYXRpb25zIGNhbiBiZSB3cml0dGVuIHRvIGl0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjb252ZXJ0ZWRGaWVsZCA9IGZpZWxkRXJyb3IuZmllbGQucmVwbGFjZSgvXFxbXFxkKlxcXS9nLCAnW10nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkTmFtZSA9IGNvbnZlcnRlZEZpZWxkLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgY29udmVydGVkRmllbGQuc2xpY2UoMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoJ0Vycm9yIG9uIGZpZWxkIFwiJyArIGZpZWxkTmFtZSArICdcIicsICdlcnJvci4nICsgZmllbGRFcnJvci5tZXNzYWdlLCB7ZmllbGROYW1lfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IucGFyYW1zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGh0dHBFcnJvclJlc3BvbnNlLmVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgY2FzZSA0MDQ6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdOb3QgZm91bmQnLCAnZXJyb3IudXJsLm5vdC5mb3VuZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIgIT09IHVuZGVmaW5lZCAmJiB0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5ldmVudE1hbmFnZXIuZGVzdHJveSh0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZGRFcnJvckFsZXJ0KG1lc3NhZ2UsIGtleT8sIGRhdGE/KSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb24ub3BlblNuYWNrTWVzc2FnZShtZXNzYWdlLCA1MDAwKTtcclxuICAgIH1cclxufVxyXG4iXX0=