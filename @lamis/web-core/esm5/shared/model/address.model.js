var Address = /** @class */ (function () {
    function Address(street1, street2, city, lga) {
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.lga = lga;
        this.street1 = street1 ? street1 : null;
        this.street2 = street2 ? street2 : null;
        this.city = city ? city : null;
        this.lga = lga ? lga : null;
    }
    return Address;
}());
export { Address };
var PersonName = /** @class */ (function () {
    function PersonName(title, firstName, middleName, surname) {
        this.title = title;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surname = surname;
    }
    return PersonName;
}());
export { PersonName };
var Phone = /** @class */ (function () {
    function Phone(phone1, phone2) {
        this.phone1 = phone1;
        this.phone2 = phone2;
    }
    return Phone;
}());
export { Phone };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkcmVzcy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9tb2RlbC9hZGRyZXNzLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVNBO0lBQ0ksaUJBQ1csT0FBZ0IsRUFDaEIsT0FBZ0IsRUFDaEIsSUFBYSxFQUNiLEdBQVU7UUFIVixZQUFPLEdBQVAsT0FBTyxDQUFTO1FBQ2hCLFlBQU8sR0FBUCxPQUFPLENBQVM7UUFDaEIsU0FBSSxHQUFKLElBQUksQ0FBUztRQUNiLFFBQUcsR0FBSCxHQUFHLENBQU87UUFFakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUFDTCxjQUFDO0FBQUQsQ0FBQyxBQVpELElBWUM7O0FBRUQ7SUFDSSxvQkFBb0IsS0FBYyxFQUFVLFNBQWtCLEVBQVUsVUFBbUIsRUFBVSxPQUFnQjtRQUFqRyxVQUFLLEdBQUwsS0FBSyxDQUFTO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBUztRQUFVLGVBQVUsR0FBVixVQUFVLENBQVM7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFTO0lBQUUsQ0FBQztJQUM1SCxpQkFBQztBQUFELENBQUMsQUFGRCxJQUVDOztBQUVEO0lBQ0ksZUFBb0IsTUFBZSxFQUFVLE1BQWU7UUFBeEMsV0FBTSxHQUFOLE1BQU0sQ0FBUztRQUFVLFdBQU0sR0FBTixNQUFNLENBQVM7SUFBRyxDQUFDO0lBQ3BFLFlBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElMR0EgfSBmcm9tICcuL2xnYS5tb2RlbCc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElBZGRyZXNzIHtcclxuICAgIHN0cmVldDE/OiBzdHJpbmc7XHJcbiAgICBzdHJlZXQyPzogc3RyaW5nO1xyXG4gICAgY2l0eT86IHN0cmluZztcclxuICAgIGxnYT86IElMR0E7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBBZGRyZXNzIGltcGxlbWVudHMgSUFkZHJlc3Mge1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIHN0cmVldDE/OiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIHN0cmVldDI/OiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIGNpdHk/OiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIGxnYT86IElMR0FcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMuc3RyZWV0MSA9IHN0cmVldDEgPyBzdHJlZXQxIDogbnVsbDtcclxuICAgICAgICB0aGlzLnN0cmVldDIgPSBzdHJlZXQyID8gc3RyZWV0MiA6IG51bGw7XHJcbiAgICAgICAgdGhpcy5jaXR5ID0gY2l0eSA/IGNpdHkgOiBudWxsO1xyXG4gICAgICAgIHRoaXMubGdhID0gbGdhID8gbGdhIDogbnVsbDtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFBlcnNvbk5hbWUge1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB0aXRsZT86IHN0cmluZywgcHJpdmF0ZSBmaXJzdE5hbWU/OiBzdHJpbmcsIHByaXZhdGUgbWlkZGxlTmFtZT86IHN0cmluZywgcHJpdmF0ZSBzdXJuYW1lPzogc3RyaW5nKXt9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBQaG9uZSB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBob25lMT86IHN0cmluZywgcHJpdmF0ZSBwaG9uZTI/OiBzdHJpbmcpIHt9XHJcbn1cclxuIl19