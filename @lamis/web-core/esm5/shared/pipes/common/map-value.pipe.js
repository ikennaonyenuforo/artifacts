import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
var MapValuesPipe = /** @class */ (function () {
    function MapValuesPipe() {
    }
    MapValuesPipe.prototype.transform = function (value, args) {
        var returnArray = [];
        value.forEach(function (entryVal, entryKey) {
            returnArray.push({
                key: entryKey,
                val: entryVal
            });
        });
        return returnArray;
    };
    MapValuesPipe = tslib_1.__decorate([
        Pipe({ name: 'mapValues' })
    ], MapValuesPipe);
    return MapValuesPipe;
}());
export { MapValuesPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLXZhbHVlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvcGlwZXMvY29tbW9uL21hcC12YWx1ZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUdwRDtJQUFBO0lBWUEsQ0FBQztJQVhHLGlDQUFTLEdBQVQsVUFBVSxLQUFVLEVBQUUsSUFBWTtRQUM5QixJQUFJLFdBQVcsR0FBVSxFQUFFLENBQUM7UUFDNUIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQWEsRUFBRSxRQUFhO1lBQ3ZDLFdBQVcsQ0FBQyxJQUFJLENBQUM7Z0JBQ2IsR0FBRyxFQUFFLFFBQVE7Z0JBQ2IsR0FBRyxFQUFFLFFBQVE7YUFDaEIsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBWFEsYUFBYTtRQUR6QixJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsV0FBVyxFQUFDLENBQUM7T0FDYixhQUFhLENBWXpCO0lBQUQsb0JBQUM7Q0FBQSxBQVpELElBWUM7U0FaWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5AUGlwZSh7bmFtZTogJ21hcFZhbHVlcyd9KVxyXG5leHBvcnQgY2xhc3MgTWFwVmFsdWVzUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGFyZ3M/OiBhbnlbXSk6IE9iamVjdFtdIHtcclxuICAgICAgICBsZXQgcmV0dXJuQXJyYXk6IGFueVtdID0gW107XHJcbiAgICAgICAgdmFsdWUuZm9yRWFjaCgoZW50cnlWYWw6IGFueSwgZW50cnlLZXk6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm5BcnJheS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGtleTogZW50cnlLZXksXHJcbiAgICAgICAgICAgICAgICB2YWw6IGVudHJ5VmFsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gcmV0dXJuQXJyYXk7XHJcbiAgICB9XHJcbn1cclxuIl19