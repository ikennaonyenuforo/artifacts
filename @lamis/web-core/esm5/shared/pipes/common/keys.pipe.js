import * as tslib_1 from "tslib";
import { Pipe } from "@angular/core";
var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var enumMember in value) {
            if (!isNaN(parseInt(enumMember, 10))) {
                keys.push({ key: enumMember, value: value[enumMember] });
            }
        }
        console.log('Keys', keys);
        return keys;
    };
    KeysPipe = tslib_1.__decorate([
        Pipe({ name: 'keys1' })
    ], KeysPipe);
    return KeysPipe;
}());
export { KeysPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5cy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3BpcGVzL2NvbW1vbi9rZXlzLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBR3BEO0lBQUE7SUFXQSxDQUFDO0lBVkcsNEJBQVMsR0FBVCxVQUFVLEtBQUssRUFBRSxJQUFjO1FBQzNCLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNkLEtBQUssSUFBSSxVQUFVLElBQUksS0FBSyxFQUFFO1lBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFO2dCQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFDLENBQUMsQ0FBQzthQUMxRDtTQUNKO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQVZRLFFBQVE7UUFEcEIsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLE9BQU8sRUFBQyxDQUFDO09BQ1QsUUFBUSxDQVdwQjtJQUFELGVBQUM7Q0FBQSxBQVhELElBV0M7U0FYWSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5AUGlwZSh7bmFtZTogJ2tleXMxJ30pXHJcbmV4cG9ydCBjbGFzcyBLZXlzUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gICAgdHJhbnNmb3JtKHZhbHVlLCBhcmdzOiBzdHJpbmdbXSk6IGFueSB7XHJcbiAgICAgICAgbGV0IGtleXMgPSBbXTtcclxuICAgICAgICBmb3IgKGxldCBlbnVtTWVtYmVyIGluIHZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmICghaXNOYU4ocGFyc2VJbnQoZW51bU1lbWJlciwgMTApKSkge1xyXG4gICAgICAgICAgICAgICAga2V5cy5wdXNoKHtrZXk6IGVudW1NZW1iZXIsIHZhbHVlOiB2YWx1ZVtlbnVtTWVtYmVyXX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnNvbGUubG9nKCdLZXlzJywga2V5cyk7XHJcbiAgICAgICAgcmV0dXJuIGtleXM7XHJcbiAgICB9XHJcbn1cclxuIl19