import * as tslib_1 from "tslib";
import { Component, Input, ViewEncapsulation } from '@angular/core';
import get from 'lodash-es/get';
import { LayoutTemplateService } from '../../services/layout.template.service';
import { CardViewBoolItemModel, CardViewDateItemModel, CardViewDatetimeItemModel, CardViewFloatItemModel, CardViewIntItemModel, CardViewTextItemModel } from '@alfresco/adf-core';
export var FieldType;
(function (FieldType) {
    FieldType["date"] = "date";
    FieldType["datetime"] = "datetime";
    FieldType["text"] = "text";
    FieldType["boolean"] = "boolean";
    FieldType["int"] = "int";
    FieldType["float"] = "float";
})(FieldType || (FieldType = {}));
var DetailsComponent = /** @class */ (function () {
    function DetailsComponent(templateService) {
        this.templateService = templateService;
    }
    DetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.templateService.getTemplate(this.template).subscribe(function (json) {
            _this.details = json.template;
        });
    };
    DetailsComponent.prototype.propertiesForDetail = function (detail) {
        var e_1, _a;
        var properties = [];
        try {
            for (var _b = tslib_1.__values(detail.fields), _c = _b.next(); !_c.done; _c = _b.next()) {
                var field = _c.value;
                var dataType = field.type;
                var item = void 0;
                switch (dataType) {
                    case FieldType.boolean:
                        item = new CardViewBoolItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label
                        });
                        break;
                    case FieldType.int:
                        item = new CardViewIntItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                        });
                        break;
                    case FieldType.float:
                        item = new CardViewFloatItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                        });
                        break;
                    case FieldType.date:
                        item = new CardViewDateItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                            format: 'dd MMM, yyyy'
                        });
                        break;
                    case FieldType.datetime:
                        item = new CardViewDatetimeItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                            format: 'dd MMM, yyyy HH:mm'
                        });
                        break;
                    default:
                        item = new CardViewTextItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                        });
                }
                properties.push(item);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return properties;
    };
    DetailsComponent.prototype.getValueForKey = function (key) {
        return get(this.model, key);
    };
    DetailsComponent.ctorParameters = function () { return [
        { type: LayoutTemplateService }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], DetailsComponent.prototype, "template", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], DetailsComponent.prototype, "model", void 0);
    DetailsComponent = tslib_1.__decorate([
        Component({
            selector: 'details-component',
            template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
            encapsulation: ViewEncapsulation.None,
            styles: [""]
        }),
        tslib_1.__metadata("design:paramtypes", [LayoutTemplateService])
    ], DetailsComponent);
    return DetailsComponent;
}());
export { DetailsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvY29tcG9uZW50L2RldGFpbHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM1RSxPQUFPLEdBQUcsTUFBTSxlQUFlLENBQUM7QUFDaEMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDL0UsT0FBTyxFQUNILHFCQUFxQixFQUNyQixxQkFBcUIsRUFDckIseUJBQXlCLEVBQ3pCLHNCQUFzQixFQUN0QixvQkFBb0IsRUFFcEIscUJBQXFCLEVBQ3hCLE1BQU0sb0JBQW9CLENBQUM7QUFjNUIsTUFBTSxDQUFOLElBQVksU0FPWDtBQVBELFdBQVksU0FBUztJQUNqQiwwQkFBYSxDQUFBO0lBQ2Isa0NBQXFCLENBQUE7SUFDckIsMEJBQWEsQ0FBQTtJQUNiLGdDQUFtQixDQUFBO0lBQ25CLHdCQUFXLENBQUE7SUFDWCw0QkFBZSxDQUFBO0FBQ25CLENBQUMsRUFQVyxTQUFTLEtBQVQsU0FBUyxRQU9wQjtBQVFEO0lBU0ksMEJBQW9CLGVBQXNDO1FBQXRDLG9CQUFlLEdBQWYsZUFBZSxDQUF1QjtJQUMxRCxDQUFDO0lBRUQsbUNBQVEsR0FBUjtRQUFBLGlCQUlDO1FBSEcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQVM7WUFDaEUsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLDhDQUFtQixHQUExQixVQUEyQixNQUFjOztRQUNyQyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUM7O1lBQ3BCLEtBQW9CLElBQUEsS0FBQSxpQkFBQSxNQUFNLENBQUMsTUFBTSxDQUFBLGdCQUFBLDRCQUFFO2dCQUE5QixJQUFNLEtBQUssV0FBQTtnQkFDWixJQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUM1QixJQUFJLElBQUksU0FBYyxDQUFDO2dCQUN2QixRQUFRLFFBQVEsRUFBRTtvQkFDZCxLQUFLLFNBQVMsQ0FBQyxPQUFPO3dCQUNsQixJQUFJLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQzs0QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs0QkFDckMsR0FBRyxFQUFFLEVBQUU7NEJBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3lCQUNyQixDQUFDLENBQUM7d0JBQ0gsTUFBTTtvQkFDVixLQUFLLFNBQVMsQ0FBQyxHQUFHO3dCQUNkLElBQUksR0FBRyxJQUFJLG9CQUFvQixDQUFDOzRCQUM1QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNyQyxHQUFHLEVBQUUsRUFBRTs0QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7eUJBQ3JCLENBQUMsQ0FBQzt3QkFDSCxNQUFNO29CQUNWLEtBQUssU0FBUyxDQUFDLEtBQUs7d0JBQ2hCLElBQUksR0FBRyxJQUFJLHNCQUFzQixDQUFDOzRCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNyQyxHQUFHLEVBQUUsRUFBRTs0QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7eUJBQ3JCLENBQUMsQ0FBQzt3QkFDSCxNQUFNO29CQUNWLEtBQUssU0FBUyxDQUFDLElBQUk7d0JBQ2YsSUFBSSxHQUFHLElBQUkscUJBQXFCLENBQUM7NEJBQzdCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7NEJBQ3JDLEdBQUcsRUFBRSxFQUFFOzRCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSzs0QkFDbEIsTUFBTSxFQUFFLGNBQWM7eUJBQ3pCLENBQUMsQ0FBQzt3QkFDSCxNQUFNO29CQUNWLEtBQUssU0FBUyxDQUFDLFFBQVE7d0JBQ25CLElBQUksR0FBRyxJQUFJLHlCQUF5QixDQUFDOzRCQUNqQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNyQyxHQUFHLEVBQUUsRUFBRTs0QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7NEJBQ2xCLE1BQU0sRUFBRSxvQkFBb0I7eUJBQy9CLENBQUMsQ0FBQzt3QkFDSCxNQUFNO29CQUNWO3dCQUNJLElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDOzRCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNyQyxHQUFHLEVBQUUsRUFBRTs0QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7eUJBQ3JCLENBQUMsQ0FBQztpQkFDVjtnQkFDRCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3pCOzs7Ozs7Ozs7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUN0QixDQUFDO0lBRU8seUNBQWMsR0FBdEIsVUFBdUIsR0FBVztRQUM5QixPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7O2dCQWxFb0MscUJBQXFCOztJQVAxRDtRQURDLEtBQUssRUFBRTs7c0RBQ1M7SUFHakI7UUFEQyxLQUFLLEVBQUU7O21EQUNHO0lBTEYsZ0JBQWdCO1FBTjVCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxtQkFBbUI7WUFDN0IsbWdCQUF1QztZQUV2QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7U0FDeEMsQ0FBQztpREFVdUMscUJBQXFCO09BVGpELGdCQUFnQixDQTRFNUI7SUFBRCx1QkFBQztDQUFBLEFBNUVELElBNEVDO1NBNUVZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IGdldCBmcm9tICdsb2Rhc2gtZXMvZ2V0JztcclxuaW1wb3J0IHsgTGF5b3V0VGVtcGxhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnRlbXBsYXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQge1xyXG4gICAgQ2FyZFZpZXdCb29sSXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdEYXRlSXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdEYXRldGltZUl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3RmxvYXRJdGVtTW9kZWwsXHJcbiAgICBDYXJkVmlld0ludEl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3SXRlbSxcclxuICAgIENhcmRWaWV3VGV4dEl0ZW1Nb2RlbFxyXG59IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERldGFpbCB7XHJcbiAgICBoZWFkZXI/OiBzdHJpbmc7XHJcbiAgICBoZWFkZXJDbGFzcz86IHN0cmluZztcclxuICAgIGZpZWxkczogRmllbGRbXTtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBGaWVsZCB7XHJcbiAgICB0eXBlOiBGaWVsZFR5cGUsXHJcbiAgICBrZXk6IHN0cmluZ1xyXG4gICAgbGFiZWw6IHN0cmluZ1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBGaWVsZFR5cGUge1xyXG4gICAgZGF0ZSA9ICdkYXRlJyxcclxuICAgIGRhdGV0aW1lID0gJ2RhdGV0aW1lJyxcclxuICAgIHRleHQgPSAndGV4dCcsXHJcbiAgICBib29sZWFuID0gJ2Jvb2xlYW4nLFxyXG4gICAgaW50ID0gJ2ludCcsXHJcbiAgICBmbG9hdCA9ICdmbG9hdCdcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2RldGFpbHMtY29tcG9uZW50JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kZXRhaWxzLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2RldGFpbHMtY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIERldGFpbHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQElucHV0KClcclxuICAgIHRlbXBsYXRlOiBzdHJpbmc7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIG1vZGVsOiBhbnk7XHJcblxyXG4gICAgZGV0YWlsczogRGV0YWlsW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB0ZW1wbGF0ZVNlcnZpY2U6IExheW91dFRlbXBsYXRlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudGVtcGxhdGVTZXJ2aWNlLmdldFRlbXBsYXRlKHRoaXMudGVtcGxhdGUpLnN1YnNjcmliZSgoanNvbjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZGV0YWlscyA9IGpzb24udGVtcGxhdGU7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHByb3BlcnRpZXNGb3JEZXRhaWwoZGV0YWlsOiBEZXRhaWwpOiBBcnJheTxDYXJkVmlld0l0ZW0+IHtcclxuICAgICAgICBsZXQgcHJvcGVydGllcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgZGV0YWlsLmZpZWxkcykge1xyXG4gICAgICAgICAgICBjb25zdCBkYXRhVHlwZSA9IGZpZWxkLnR5cGU7XHJcbiAgICAgICAgICAgIGxldCBpdGVtOiBDYXJkVmlld0l0ZW07XHJcbiAgICAgICAgICAgIHN3aXRjaCAoZGF0YVR5cGUpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmJvb2xlYW46XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0Jvb2xJdGVtTW9kZWwoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5nZXRWYWx1ZUZvcktleShmaWVsZC5rZXkpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogZmllbGQubGFiZWxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmludDpcclxuICAgICAgICAgICAgICAgICAgICBpdGVtID0gbmV3IENhcmRWaWV3SW50SXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGaWVsZFR5cGUuZmxvYXQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0Zsb2F0SXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGaWVsZFR5cGUuZGF0ZTpcclxuICAgICAgICAgICAgICAgICAgICBpdGVtID0gbmV3IENhcmRWaWV3RGF0ZUl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiAnZGQgTU1NLCB5eXl5J1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGaWVsZFR5cGUuZGF0ZXRpbWU6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0RhdGV0aW1lSXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtYXQ6ICdkZCBNTU0sIHl5eXkgSEg6bW0nXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdUZXh0SXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHByb3BlcnRpZXMucHVzaChpdGVtKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHByb3BlcnRpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRWYWx1ZUZvcktleShrZXk6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIGdldCh0aGlzLm1vZGVsLCBrZXkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==