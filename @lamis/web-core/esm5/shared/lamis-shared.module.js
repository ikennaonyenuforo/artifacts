import * as tslib_1 from "tslib";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { SharedCommonModule } from './shared-common.module';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { SpeedDialFabComponent } from './util/speed-dial-fab.component';
import { CardViewModule } from './util/card-view/card-view.module';
import { DateRangePicker } from './util/date-range-picker/date-range-picker';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatDatepickerModule, MatIconModule, MatSelectModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { DateRangePickerDirective } from './util/date-range-picker/date-range-picker.directive';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JsonFormModule } from './json-form/json-form.module';
var LamisSharedModule = /** @class */ (function () {
    function LamisSharedModule() {
    }
    LamisSharedModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MatButtonModule,
                MatSelectModule,
                MatIconModule,
                MatTooltipModule,
                RouterModule,
                SharedCommonModule,
                CardViewModule,
                FlexLayoutModule,
                MatDatepickerModule,
                MatCardModule,
                JsonFormModule
            ],
            declarations: [
                HasAnyAuthorityDirective,
                SpeedDialFabComponent,
                DateRangePicker,
                DateRangePickerDirective
            ],
            entryComponents: [
                DateRangePicker
            ],
            exports: [
                JsonFormModule,
                SharedCommonModule,
                HasAnyAuthorityDirective,
                SpeedDialFabComponent,
                CardViewModule,
                DateRangePicker,
                DateRangePickerDirective
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], LamisSharedModule);
    return LamisSharedModule;
}());
export { LamisSharedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMtc2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9sYW1pcy1zaGFyZWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNuRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDN0UsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQ04sZUFBZSxFQUNmLGFBQWEsRUFDYixtQkFBbUIsRUFDbkIsYUFBYSxFQUNiLGVBQWUsRUFDZixnQkFBZ0IsRUFDaEIsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDaEcsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBdUM5RDtJQUFBO0lBQ0EsQ0FBQztJQURZLGlCQUFpQjtRQXJDN0IsUUFBUSxDQUFDO1lBQ1QsT0FBTyxFQUFFO2dCQUNSLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxtQkFBbUI7Z0JBQ25CLGVBQWU7Z0JBQ2YsZUFBZTtnQkFDZixhQUFhO2dCQUNiLGdCQUFnQjtnQkFDaEIsWUFBWTtnQkFDWixrQkFBa0I7Z0JBQ2xCLGNBQWM7Z0JBQ2QsZ0JBQWdCO2dCQUNoQixtQkFBbUI7Z0JBQ25CLGFBQWE7Z0JBQ2IsY0FBYzthQUNkO1lBQ0QsWUFBWSxFQUFFO2dCQUNiLHdCQUF3QjtnQkFDeEIscUJBQXFCO2dCQUNyQixlQUFlO2dCQUNmLHdCQUF3QjthQUN4QjtZQUNELGVBQWUsRUFBRTtnQkFDaEIsZUFBZTthQUNmO1lBQ0QsT0FBTyxFQUFFO2dCQUNSLGNBQWM7Z0JBQ2Qsa0JBQWtCO2dCQUNsQix3QkFBd0I7Z0JBQ3hCLHFCQUFxQjtnQkFDckIsY0FBYztnQkFDZCxlQUFlO2dCQUNmLHdCQUF3QjthQUN4QjtZQUNELE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO1NBQ2pDLENBQUM7T0FDVyxpQkFBaUIsQ0FDN0I7SUFBRCx3QkFBQztDQUFBLEFBREQsSUFDQztTQURZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENVU1RPTV9FTEVNRU5UU19TQ0hFTUEsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNoYXJlZENvbW1vbk1vZHVsZSB9IGZyb20gJy4vc2hhcmVkLWNvbW1vbi5tb2R1bGUnO1xyXG5pbXBvcnQgeyBIYXNBbnlBdXRob3JpdHlEaXJlY3RpdmUgfSBmcm9tICcuL2F1dGgvaGFzLWFueS1hdXRob3JpdHkuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU3BlZWREaWFsRmFiQ29tcG9uZW50IH0gZnJvbSAnLi91dGlsL3NwZWVkLWRpYWwtZmFiLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3TW9kdWxlIH0gZnJvbSAnLi91dGlsL2NhcmQtdmlldy9jYXJkLXZpZXcubW9kdWxlJztcclxuaW1wb3J0IHsgRGF0ZVJhbmdlUGlja2VyIH0gZnJvbSAnLi91dGlsL2RhdGUtcmFuZ2UtcGlja2VyL2RhdGUtcmFuZ2UtcGlja2VyJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7XHJcblx0TWF0QnV0dG9uTW9kdWxlLFxyXG5cdE1hdENhcmRNb2R1bGUsXHJcblx0TWF0RGF0ZXBpY2tlck1vZHVsZSxcclxuXHRNYXRJY29uTW9kdWxlLFxyXG5cdE1hdFNlbGVjdE1vZHVsZSxcclxuXHRNYXRUb29sdGlwTW9kdWxlXHJcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBEYXRlUmFuZ2VQaWNrZXJEaXJlY3RpdmUgfSBmcm9tICcuL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvZGF0ZS1yYW5nZS1waWNrZXIuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgRmxleExheW91dE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgSnNvbkZvcm1Nb2R1bGUgfSBmcm9tICcuL2pzb24tZm9ybS9qc29uLWZvcm0ubW9kdWxlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcblx0aW1wb3J0czogW1xyXG5cdFx0Q29tbW9uTW9kdWxlLFxyXG5cdFx0Rm9ybXNNb2R1bGUsXHJcblx0XHRSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG5cdFx0TWF0QnV0dG9uTW9kdWxlLFxyXG5cdFx0TWF0U2VsZWN0TW9kdWxlLFxyXG5cdFx0TWF0SWNvbk1vZHVsZSxcclxuXHRcdE1hdFRvb2x0aXBNb2R1bGUsXHJcblx0XHRSb3V0ZXJNb2R1bGUsXHJcblx0XHRTaGFyZWRDb21tb25Nb2R1bGUsXHJcblx0XHRDYXJkVmlld01vZHVsZSxcclxuXHRcdEZsZXhMYXlvdXRNb2R1bGUsXHJcblx0XHRNYXREYXRlcGlja2VyTW9kdWxlLFxyXG5cdFx0TWF0Q2FyZE1vZHVsZSxcclxuXHRcdEpzb25Gb3JtTW9kdWxlXHJcblx0XSxcclxuXHRkZWNsYXJhdGlvbnM6IFtcclxuXHRcdEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSxcclxuXHRcdFNwZWVkRGlhbEZhYkNvbXBvbmVudCxcclxuXHRcdERhdGVSYW5nZVBpY2tlcixcclxuXHRcdERhdGVSYW5nZVBpY2tlckRpcmVjdGl2ZVxyXG5cdF0sXHJcblx0ZW50cnlDb21wb25lbnRzOiBbXHJcblx0XHREYXRlUmFuZ2VQaWNrZXJcclxuXHRdLFxyXG5cdGV4cG9ydHM6IFtcclxuXHRcdEpzb25Gb3JtTW9kdWxlLFxyXG5cdFx0U2hhcmVkQ29tbW9uTW9kdWxlLFxyXG5cdFx0SGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlLFxyXG5cdFx0U3BlZWREaWFsRmFiQ29tcG9uZW50LFxyXG5cdFx0Q2FyZFZpZXdNb2R1bGUsXHJcblx0XHREYXRlUmFuZ2VQaWNrZXIsXHJcblx0XHREYXRlUmFuZ2VQaWNrZXJEaXJlY3RpdmVcclxuXHRdLFxyXG5cdHNjaGVtYXM6IFtDVVNUT01fRUxFTUVOVFNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFtaXNTaGFyZWRNb2R1bGUge1xyXG59XHJcbiJdfQ==