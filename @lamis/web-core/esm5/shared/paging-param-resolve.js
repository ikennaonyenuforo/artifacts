import * as tslib_1 from "tslib";
import { JhiPaginationUtil } from 'ng-jhipster';
import { Injectable, Injector } from '@angular/core';
import * as i0 from "@angular/core";
var PagingParamsResolve = /** @class */ (function () {
    function PagingParamsResolve(injector) {
        this.injector = injector;
        this.paginationUtil = this.injector.get(JhiPaginationUtil);
    }
    PagingParamsResolve.prototype.resolve = function (route, state) {
        var page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        var query = route.queryParams['query'] ? route.queryParams['query'] : '';
        var filter = route.queryParams['filter'] ? route.queryParams['filter'] : '';
        var sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: +page,
            query: query,
            filter: filter,
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    };
    PagingParamsResolve.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    PagingParamsResolve.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PagingParamsResolve_Factory() { return new PagingParamsResolve(i0.ɵɵinject(i0.INJECTOR)); }, token: PagingParamsResolve, providedIn: "root" });
    PagingParamsResolve = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Injector])
    ], PagingParamsResolve);
    return PagingParamsResolve;
}());
export { PagingParamsResolve };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5nLXBhcmFtLXJlc29sdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvcGFnaW5nLXBhcmFtLXJlc29sdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRCxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLckQ7SUFHSSw2QkFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHFDQUFPLEdBQVAsVUFBUSxLQUE2QixFQUFFLEtBQTBCO1FBQzdELElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztRQUN6RSxJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsSUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzlFLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUM5RSxPQUFPO1lBQ0gsSUFBSSxFQUFFLENBQUMsSUFBSTtZQUNYLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLE1BQU07WUFDZCxTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ25ELFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7U0FDdEQsQ0FBQztJQUNOLENBQUM7O2dCQWhCNkIsUUFBUTs7O0lBSDdCLG1CQUFtQjtRQUgvQixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO2lEQUlnQyxRQUFRO09BSDdCLG1CQUFtQixDQW9CL0I7OEJBM0JEO0NBMkJDLEFBcEJELElBb0JDO1NBcEJZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJlc29sdmUsIFJvdXRlclN0YXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBKaGlQYWdpbmF0aW9uVXRpbCB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0b3IgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFnaW5nUGFyYW1zUmVzb2x2ZSBpbXBsZW1lbnRzIFJlc29sdmU8YW55PiB7XHJcbiAgICBwYWdpbmF0aW9uVXRpbDogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yKSB7XHJcbiAgICAgICAgdGhpcy5wYWdpbmF0aW9uVXRpbCA9IHRoaXMuaW5qZWN0b3IuZ2V0KEpoaVBhZ2luYXRpb25VdGlsKTtcclxuICAgIH1cclxuXHJcbiAgICByZXNvbHZlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCkge1xyXG4gICAgICAgIGNvbnN0IHBhZ2UgPSByb3V0ZS5xdWVyeVBhcmFtc1sncGFnZSddID8gcm91dGUucXVlcnlQYXJhbXNbJ3BhZ2UnXSA6ICcxJztcclxuICAgICAgICBjb25zdCBxdWVyeSA9IHJvdXRlLnF1ZXJ5UGFyYW1zWydxdWVyeSddID8gcm91dGUucXVlcnlQYXJhbXNbJ3F1ZXJ5J10gOiAnJztcclxuICAgICAgICBjb25zdCBmaWx0ZXIgPSByb3V0ZS5xdWVyeVBhcmFtc1snZmlsdGVyJ10gPyByb3V0ZS5xdWVyeVBhcmFtc1snZmlsdGVyJ10gOiAnJztcclxuICAgICAgICBjb25zdCBzb3J0ID0gcm91dGUucXVlcnlQYXJhbXNbJ3NvcnQnXSA/IHJvdXRlLnF1ZXJ5UGFyYW1zWydzb3J0J10gOiAnaWQsYXNjJztcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBwYWdlOiArcGFnZSxcclxuICAgICAgICAgICAgcXVlcnk6IHF1ZXJ5LFxyXG4gICAgICAgICAgICBmaWx0ZXI6IGZpbHRlcixcclxuICAgICAgICAgICAgcHJlZGljYXRlOiB0aGlzLnBhZ2luYXRpb25VdGlsLnBhcnNlUHJlZGljYXRlKHNvcnQpLFxyXG4gICAgICAgICAgICBhc2NlbmRpbmc6IHRoaXMucGFnaW5hdGlvblV0aWwucGFyc2VBc2NlbmRpbmcoc29ydClcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==