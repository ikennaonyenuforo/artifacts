var CardViewItemNameValidator = /** @class */ (function () {
    function CardViewItemNameValidator() {
        this.message = '';
    }
    CardViewItemNameValidator.prototype.isValid = function (value) {
        var nameError = false;
        var surnameError = false;
        if (!value.firstName || value.firstName.length < 3) {
            nameError = true;
        }
        if (!value.surname || value.surname.length < 3) {
            surnameError = true;
        }
        if (nameError && surnameError) {
            this.message = 'Both first name and surname are required';
        }
        else if (nameError) {
            this.message = 'First name is required';
        }
        else if (surnameError) {
            this.message = 'Surname is required';
        }
        return !(nameError || surnameError);
    };
    return CardViewItemNameValidator;
}());
export { CardViewItemNameValidator };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tbmFtZS52YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvdmFsaWRhdG9ycy9jYXJkLXZpZXctaXRlbS1uYW1lLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtJQUFBO1FBQ0ksWUFBTyxHQUFHLEVBQUUsQ0FBQztJQXFCakIsQ0FBQztJQW5CRywyQ0FBTyxHQUFQLFVBQVEsS0FBVTtRQUNkLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2hELFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDNUMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUNELElBQUksU0FBUyxJQUFJLFlBQVksRUFBRTtZQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLDBDQUEwQyxDQUFDO1NBQzdEO2FBQU0sSUFBSSxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyx3QkFBd0IsQ0FBQztTQUMzQzthQUFNLElBQUksWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcscUJBQXFCLENBQUE7U0FDdkM7UUFDRCxPQUFPLENBQUMsQ0FBQyxTQUFTLElBQUksWUFBWSxDQUFDLENBQUM7SUFDeEMsQ0FBQztJQUVMLGdDQUFDO0FBQUQsQ0FBQyxBQXRCRCxJQXNCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENhcmRWaWV3SXRlbVZhbGlkYXRvciB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdJdGVtTmFtZVZhbGlkYXRvciBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbVZhbGlkYXRvciB7XHJcbiAgICBtZXNzYWdlID0gJyc7XHJcblxyXG4gICAgaXNWYWxpZCh2YWx1ZTogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IG5hbWVFcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBzdXJuYW1lRXJyb3IgPSBmYWxzZTtcclxuICAgICAgICBpZiAoIXZhbHVlLmZpcnN0TmFtZSB8fCB2YWx1ZS5maXJzdE5hbWUubGVuZ3RoIDwgMykge1xyXG4gICAgICAgICAgICBuYW1lRXJyb3IgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXZhbHVlLnN1cm5hbWUgfHwgdmFsdWUuc3VybmFtZS5sZW5ndGggPCAzKSB7XHJcbiAgICAgICAgICAgIHN1cm5hbWVFcnJvciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChuYW1lRXJyb3IgJiYgc3VybmFtZUVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdCb3RoIGZpcnN0IG5hbWUgYW5kIHN1cm5hbWUgYXJlIHJlcXVpcmVkJztcclxuICAgICAgICB9IGVsc2UgaWYgKG5hbWVFcnJvcikge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnRmlyc3QgbmFtZSBpcyByZXF1aXJlZCc7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzdXJuYW1lRXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ1N1cm5hbWUgaXMgcmVxdWlyZWQnXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAhKG5hbWVFcnJvciB8fCBzdXJuYW1lRXJyb3IpO1xyXG4gICAgfVxyXG5cclxufSJdfQ==