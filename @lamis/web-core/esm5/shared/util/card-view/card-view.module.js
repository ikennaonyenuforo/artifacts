import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CardItemTypeService, CoreModule } from '@alfresco/adf-core';
import { CardViewHtmlTextItemComponent } from './components/card-view-html-text-item/card-view-html-text-item.component';
import { CardViewNameItemComponent } from './components/card-view-name-item/card-view-name-item.component';
import { CardViewAddressItemComponent } from './components/card-view-address-item/card-view-address-item.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CardViewFixedKeyvaluepairsitemComponent } from './components/card-view-fixed-keyvaluepairsitem/card-view-fixed-keyvaluepairsitem.component';
import { CardViewPhoneItemComponent } from './components/card-view-phone-item/card-view-phone-item.component';
export function components() {
    return [
        CardViewHtmlTextItemComponent,
        CardViewNameItemComponent,
        CardViewAddressItemComponent,
        CardViewFixedKeyvaluepairsitemComponent,
        CardViewPhoneItemComponent
    ];
}
var CardViewModule = /** @class */ (function () {
    function CardViewModule(cardItemTypeService) {
        this.cardItemTypeService = cardItemTypeService;
        cardItemTypeService.setComponentTypeResolver('html-text', function () { return CardViewHtmlTextItemComponent; });
        cardItemTypeService.setComponentTypeResolver('phone', function () { return CardViewPhoneItemComponent; });
        cardItemTypeService.setComponentTypeResolver('name', function () { return CardViewNameItemComponent; });
        cardItemTypeService.setComponentTypeResolver('address', function () { return CardViewAddressItemComponent; });
        cardItemTypeService.setComponentTypeResolver('fixedkeyvaluepairs', function () { return CardViewFixedKeyvaluepairsitemComponent; });
    }
    CardViewModule.ctorParameters = function () { return [
        { type: CardItemTypeService }
    ]; };
    CardViewModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CoreModule,
                CommonModule,
                FormsModule,
                FlexLayoutModule
            ],
            declarations: components(),
            exports: components(),
            entryComponents: components(),
            providers: [
                CardItemTypeService
            ]
        }),
        tslib_1.__metadata("design:paramtypes", [CardItemTypeService])
    ], CardViewModule);
    return CardViewModule;
}());
export { CardViewModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jYXJkLXZpZXcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxVQUFVLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSwwRUFBMEUsQ0FBQztBQUN6SCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnRUFBZ0UsQ0FBQztBQUMzRyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxzRUFBc0UsQ0FBQztBQUNwSCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx1Q0FBdUMsRUFBRSxNQUFNLDRGQUE0RixDQUFDO0FBQ3JKLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBRTlHLE1BQU0sVUFBVSxVQUFVO0lBQ3RCLE9BQU87UUFDSCw2QkFBNkI7UUFDN0IseUJBQXlCO1FBQ3pCLDRCQUE0QjtRQUM1Qix1Q0FBdUM7UUFDdkMsMEJBQTBCO0tBQzdCLENBQUE7QUFDTCxDQUFDO0FBZ0JEO0lBQ0ksd0JBQW9CLG1CQUF3QztRQUF4Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO1FBQ3hELG1CQUFtQixDQUFDLHdCQUF3QixDQUFDLFdBQVcsRUFBRSxjQUFNLE9BQUEsNkJBQTZCLEVBQTdCLENBQTZCLENBQUMsQ0FBQztRQUMvRixtQkFBbUIsQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLEVBQUUsY0FBTSxPQUFBLDBCQUEwQixFQUExQixDQUEwQixDQUFDLENBQUM7UUFDeEYsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFLGNBQU0sT0FBQSx5QkFBeUIsRUFBekIsQ0FBeUIsQ0FBQyxDQUFDO1FBQ3RGLG1CQUFtQixDQUFDLHdCQUF3QixDQUFDLFNBQVMsRUFBRSxjQUFNLE9BQUEsNEJBQTRCLEVBQTVCLENBQTRCLENBQUMsQ0FBQztRQUM1RixtQkFBbUIsQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0IsRUFBRSxjQUFNLE9BQUEsdUNBQXVDLEVBQXZDLENBQXVDLENBQUMsQ0FBQztJQUN0SCxDQUFDOztnQkFOd0MsbUJBQW1COztJQURuRCxjQUFjO1FBZDFCLFFBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRTtnQkFDTCxVQUFVO2dCQUNWLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxnQkFBZ0I7YUFDbkI7WUFDRCxZQUFZLEVBQUUsVUFBVSxFQUFFO1lBQzFCLE9BQU8sRUFBRSxVQUFVLEVBQUU7WUFDckIsZUFBZSxFQUFFLFVBQVUsRUFBRTtZQUM3QixTQUFTLEVBQUU7Z0JBQ1AsbUJBQW1CO2FBQ3RCO1NBQ0osQ0FBQztpREFFMkMsbUJBQW1CO09BRG5ELGNBQWMsQ0FRMUI7SUFBRCxxQkFBQztDQUFBLEFBUkQsSUFRQztTQVJZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkSXRlbVR5cGVTZXJ2aWNlLCBDb3JlTW9kdWxlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdIdG1sVGV4dEl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtL2NhcmQtdmlldy1odG1sLXRleHQtaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld05hbWVJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhcmQtdmlldy1uYW1lLWl0ZW0vY2FyZC12aWV3LW5hbWUtaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0FkZHJlc3NJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhcmQtdmlldy1hZGRyZXNzLWl0ZW0vY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGbGV4TGF5b3V0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdGaXhlZEtleXZhbHVlcGFpcnNpdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzaXRlbS9jYXJkLXZpZXctZml4ZWQta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZFZpZXdQaG9uZUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3LXBob25lLWl0ZW0vY2FyZC12aWV3LXBob25lLWl0ZW0uY29tcG9uZW50JztcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBjb21wb25lbnRzKCkge1xyXG4gICAgcmV0dXJuIFtcclxuICAgICAgICBDYXJkVmlld0h0bWxUZXh0SXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld05hbWVJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3QWRkcmVzc0l0ZW1Db21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdGaXhlZEtleXZhbHVlcGFpcnNpdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3UGhvbmVJdGVtQ29tcG9uZW50XHJcbiAgICBdXHJcbn1cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29yZU1vZHVsZSxcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgRmxleExheW91dE1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogY29tcG9uZW50cygpLFxyXG4gICAgZXhwb3J0czogY29tcG9uZW50cygpLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBjb21wb25lbnRzKCksXHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBDYXJkSXRlbVR5cGVTZXJ2aWNlXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld01vZHVsZSB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRJdGVtVHlwZVNlcnZpY2U6IENhcmRJdGVtVHlwZVNlcnZpY2UpIHtcclxuICAgICAgICBjYXJkSXRlbVR5cGVTZXJ2aWNlLnNldENvbXBvbmVudFR5cGVSZXNvbHZlcignaHRtbC10ZXh0JywgKCkgPT4gQ2FyZFZpZXdIdG1sVGV4dEl0ZW1Db21wb25lbnQpO1xyXG4gICAgICAgIGNhcmRJdGVtVHlwZVNlcnZpY2Uuc2V0Q29tcG9uZW50VHlwZVJlc29sdmVyKCdwaG9uZScsICgpID0+IENhcmRWaWV3UGhvbmVJdGVtQ29tcG9uZW50KTtcclxuICAgICAgICBjYXJkSXRlbVR5cGVTZXJ2aWNlLnNldENvbXBvbmVudFR5cGVSZXNvbHZlcignbmFtZScsICgpID0+IENhcmRWaWV3TmFtZUl0ZW1Db21wb25lbnQpO1xyXG4gICAgICAgIGNhcmRJdGVtVHlwZVNlcnZpY2Uuc2V0Q29tcG9uZW50VHlwZVJlc29sdmVyKCdhZGRyZXNzJywgKCkgPT4gQ2FyZFZpZXdBZGRyZXNzSXRlbUNvbXBvbmVudCk7XHJcbiAgICAgICAgY2FyZEl0ZW1UeXBlU2VydmljZS5zZXRDb21wb25lbnRUeXBlUmVzb2x2ZXIoJ2ZpeGVka2V5dmFsdWVwYWlycycsICgpID0+IENhcmRWaWV3Rml4ZWRLZXl2YWx1ZXBhaXJzaXRlbUNvbXBvbmVudCk7XHJcbiAgICB9XHJcbn1cclxuIl19