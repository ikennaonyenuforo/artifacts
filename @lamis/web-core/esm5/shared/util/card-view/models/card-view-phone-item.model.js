import * as tslib_1 from "tslib";
import { CardViewBaseItemModel } from '@alfresco/adf-core';
var CardViewPhoneItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewPhoneItemModel, _super);
    function CardViewPhoneItemModel(obj) {
        var _this = _super.call(this, obj) || this;
        _this.type = 'phone';
        return _this;
    }
    Object.defineProperty(CardViewPhoneItemModel.prototype, "displayValue", {
        get: function () {
            if (this.isEmpty()) {
                return this.default;
            }
            else {
                return this.getValue();
            }
        },
        enumerable: true,
        configurable: true
    });
    CardViewPhoneItemModel.prototype.getValue = function () {
        return ((!!this.value.phone1 ? this.value.phone1 : '') + "\n        " + (!!this.value.phone2 ? ', ' + this.value.phone2 : '')).trim();
    };
    return CardViewPhoneItemModel;
}(CardViewBaseItemModel));
export { CardViewPhoneItemModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXBob25lLWl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1waG9uZS1pdGVtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUscUJBQXFCLEVBQXVDLE1BQU0sb0JBQW9CLENBQUM7QUFHaEc7SUFBNEMsa0RBQXFCO0lBRzdELGdDQUFZLEdBQWdDO1FBQTVDLFlBQ0ksa0JBQU0sR0FBRyxDQUFDLFNBQ2I7UUFKRCxVQUFJLEdBQVcsT0FBTyxDQUFDOztJQUl2QixDQUFDO0lBRUQsc0JBQUksZ0RBQVk7YUFBaEI7WUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQzFCO1FBQ0wsQ0FBQzs7O09BQUE7SUFFRCx5Q0FBUSxHQUFSO1FBQ0ksT0FBTyxDQUFBLENBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQSxDQUFDLENBQUMsRUFBRSxvQkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUEsQ0FBQyxDQUFDLEVBQUUsQ0FBRSxDQUFBLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbEUsQ0FBQztJQUNMLDZCQUFDO0FBQUQsQ0FBQyxBQW5CRCxDQUE0QyxxQkFBcUIsR0FtQmhFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsLCBDYXJkVmlld0l0ZW0sIER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3UGhvbmVJdGVtUHJvcGVydGllcyB9IGZyb20gJy4vY2FyZC12aWV3LXBob25lLWl0ZW0ucHJvcGVydGllcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdQaG9uZUl0ZW1Nb2RlbCBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICdwaG9uZSc7XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqOiBDYXJkVmlld1Bob25lSXRlbVByb3BlcnRpZXMpIHtcclxuICAgICAgICBzdXBlcihvYmopO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkaXNwbGF5VmFsdWUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNFbXB0eSgpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIGAkeyEhdGhpcy52YWx1ZS5waG9uZTEgPyB0aGlzLnZhbHVlLnBob25lMTogJyd9XHJcbiAgICAgICAgJHshIXRoaXMudmFsdWUucGhvbmUyID8gJywgJyArIHRoaXMudmFsdWUucGhvbmUyOiAnJ31gLnRyaW0oKTtcclxuICAgIH1cclxufSJdfQ==