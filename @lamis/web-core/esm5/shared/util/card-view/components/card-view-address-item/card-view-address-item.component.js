import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewAddressItemModel } from '../../models/card-view-address-item.model';
import { Address } from '../../../../model/address.model';
import { LgaService } from '../../../../../services/lga.service';
import { StateService } from '../../../../../services/state.service';
var CardViewAddressItemComponent = /** @class */ (function () {
    function CardViewAddressItemComponent(cardViewUpdateService, stateService, lgaService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.stateService = stateService;
        this.lgaService = lgaService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    CardViewAddressItemComponent.prototype.ngOnChanges = function () {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
    };
    CardViewAddressItemComponent.prototype.ngOnInit = function () {
    };
    CardViewAddressItemComponent.prototype.onChange = function (event) {
        var _this = this;
        this.lgaService.findByState(event.value.id).subscribe(function (res) { return _this.lgas = res.body; });
    };
    CardViewAddressItemComponent.prototype.showProperty = function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    CardViewAddressItemComponent.prototype.isEditable = function () {
        return this.editable && this.property.editable;
    };
    CardViewAddressItemComponent.prototype.isClickable = function () {
        return this.property.clickable;
    };
    CardViewAddressItemComponent.prototype.hasIcon = function () {
        return !!this.property.icon;
    };
    CardViewAddressItemComponent.prototype.hasErrors = function () {
        return this.errorMessages && this.errorMessages.length;
    };
    CardViewAddressItemComponent.prototype.setEditMode = function (editStatus) {
        var _this = this;
        if (editStatus) {
            this.stateService.getStates().subscribe(function (res) { return _this.states = res.body; });
        }
        this.inEdit = editStatus;
        setTimeout(function () {
            if (_this.cityInput) {
                _this.cityInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.street1Input) {
                _this.street1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.street2Input) {
                _this.street2Input.nativeElement.click();
            }
        }, 0);
    };
    CardViewAddressItemComponent.prototype.reset = function () {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
        this.setEditMode(false);
    };
    CardViewAddressItemComponent.prototype.update = function () {
        if (this.property.isValid(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga))) {
            this.cardViewUpdateService.update(this.property, new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
            this.property.value = new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
        }
    };
    Object.defineProperty(CardViewAddressItemComponent.prototype, "displayValue", {
        get: function () {
            return this.property.displayValue;
        },
        enumerable: true,
        configurable: true
    });
    CardViewAddressItemComponent.prototype.clicked = function () {
        this.cardViewUpdateService.clicked(this.property);
    };
    CardViewAddressItemComponent.prototype.entityCompare = function (s1, s2) {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    };
    CardViewAddressItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService },
        { type: StateService },
        { type: LgaService }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", CardViewAddressItemModel)
    ], CardViewAddressItemComponent.prototype, "property", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], CardViewAddressItemComponent.prototype, "editable", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], CardViewAddressItemComponent.prototype, "displayEmpty", void 0);
    tslib_1.__decorate([
        ViewChild('cityInput', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewAddressItemComponent.prototype, "cityInput", void 0);
    tslib_1.__decorate([
        ViewChild('street1Input', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewAddressItemComponent.prototype, "street1Input", void 0);
    tslib_1.__decorate([
        ViewChild('street2Input', { static: true }),
        tslib_1.__metadata("design:type", Object)
    ], CardViewAddressItemComponent.prototype, "street2Input", void 0);
    CardViewAddressItemComponent = tslib_1.__decorate([
        Component({
            selector: 'card-view-address-item',
            template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #street1Input\r\n                               matInput\r\n                               [placeholder]=\"'Street Line 1'\"\r\n                               [(ngModel)]=\"editedStreet1\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #street2Input\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'Street Line 2'\"\r\n                               [(ngModel)]=\"editedStreet2\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #cityInput\r\n                               matInput\r\n                               [placeholder]=\"'City'\"\r\n                               [(ngModel)]=\"editedCity\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field>\r\n                        <mat-select (selectionChange)=\"onChange($event)\"\r\n                                    placeholder=\"State\"\r\n                                    [(ngModel)]=\"state\"\r\n                                    [compareWith]=\"entityCompare\"\r\n                                    data-automation-class=\"select-box\">\r\n                            <mat-option *ngFor=\"let state of states \" [value]=\"state\">\r\n                                {{ state.name }}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field>\r\n                        <mat-select [(ngModel)]=\"editedLga\"\r\n                                    placeholder=\"LGA\"\r\n                                    [compareWith]=\"entityCompare\"\r\n                                    data-automation-class=\"select-box\">\r\n                            <mat-option *ngFor=\"let lga of lgas \" [value]=\"lga\">\r\n                                {{ lga.name }}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
        }),
        tslib_1.__metadata("design:paramtypes", [CardViewUpdateService,
            StateService,
            LgaService])
    ], CardViewAddressItemComponent);
    return CardViewAddressItemComponent;
}());
export { CardViewAddressItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctYWRkcmVzcy1pdGVtL2NhcmQtdmlldy1hZGRyZXNzLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQzNELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRXJGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUMxRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDakUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBUXJFO0lBOEJJLHNDQUFvQixxQkFBNEMsRUFDNUMsWUFBMEIsRUFDMUIsVUFBc0I7UUFGdEIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUM1QyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBMUIxQyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBRzFCLGlCQUFZLEdBQVksSUFBSSxDQUFDO1FBYzdCLFdBQU0sR0FBWSxLQUFLLENBQUM7SUFVeEIsQ0FBQztJQUVELGtEQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMzQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1NBQzlDO0lBQ0wsQ0FBQztJQUVELCtDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsK0NBQVEsR0FBUixVQUFTLEtBQXNCO1FBQS9CLGlCQUVDO1FBREcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxLQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQXBCLENBQW9CLENBQUMsQ0FBQztJQUN2RixDQUFDO0lBRUQsbURBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDekQsQ0FBQztJQUVELGlEQUFVLEdBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELGtEQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFRCw4Q0FBTyxHQUFQO1FBQ0ksT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUVELGdEQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFDM0QsQ0FBQztJQUVELGtEQUFXLEdBQVgsVUFBWSxVQUFtQjtRQUEvQixpQkFvQkM7UUFuQkcsSUFBSSxVQUFVLEVBQUU7WUFDWixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEtBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLElBQUksRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO1NBQzFFO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7UUFDekIsVUFBVSxDQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNoQixLQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN4QztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLFVBQVUsQ0FBQztZQUNQLElBQUksS0FBSSxDQUFDLFlBQVksRUFBRTtnQkFDbkIsS0FBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDM0M7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixVQUFVLENBQUM7WUFDUCxJQUFJLEtBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ25CLEtBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzNDO1FBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQUVELDRDQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMzQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsNkNBQU0sR0FBTjtRQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7WUFDN0csSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUMzQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMxRixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDM0csSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1NBQ2hKO0lBQ0wsQ0FBQztJQUVELHNCQUFJLHNEQUFZO2FBQWhCO1lBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztRQUN0QyxDQUFDOzs7T0FBQTtJQUVELDhDQUFPLEdBQVA7UUFDSSxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsb0RBQWEsR0FBYixVQUFjLEVBQU8sRUFBRSxFQUFPO1FBQzFCLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDO0lBQ2pELENBQUM7O2dCQWhHMEMscUJBQXFCO2dCQUM5QixZQUFZO2dCQUNkLFVBQVU7O0lBN0IxQztRQURDLEtBQUssRUFBRTswQ0FDRSx3QkFBd0I7a0VBQUM7SUFHbkM7UUFEQyxLQUFLLEVBQUU7O2tFQUNrQjtJQUcxQjtRQURDLEtBQUssRUFBRTs7c0VBQ3FCO0lBRzdCO1FBREMsU0FBUyxDQUFDLFdBQVcsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzs7bUVBQ2hCO0lBR3ZCO1FBREMsU0FBUyxDQUFDLGNBQWMsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzs7c0VBQ2hCO0lBRzFCO1FBREMsU0FBUyxDQUFDLGNBQWMsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzs7c0VBQ2hCO0lBbEJqQiw0QkFBNEI7UUFKeEMsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLHdCQUF3QjtZQUNsQyxnN01BQXNEO1NBQ3pELENBQUM7aURBK0I2QyxxQkFBcUI7WUFDOUIsWUFBWTtZQUNkLFVBQVU7T0FoQ2pDLDRCQUE0QixDQStIeEM7SUFBRCxtQ0FBQztDQUFBLEFBL0hELElBK0hDO1NBL0hZLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdBZGRyZXNzSXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1hZGRyZXNzLWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBNYXRTZWxlY3RDaGFuZ2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IEFkZHJlc3MgfSBmcm9tICcuLi8uLi8uLi8uLi9tb2RlbC9hZGRyZXNzLm1vZGVsJztcclxuaW1wb3J0IHsgTGdhU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL3NlcnZpY2VzL2xnYS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vc2VydmljZXMvc3RhdGUuc2VydmljZSc7XHJcbmltcG9ydCB7IElMR0EgfSBmcm9tICcuLi8uLi8uLi8uLi9tb2RlbC9sZ2EubW9kZWwnO1xyXG5pbXBvcnQgeyBJU3RhdGUgfSBmcm9tICcuLi8uLi8uLi8uLi9tb2RlbC9zdGF0ZS5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY2FyZC12aWV3LWFkZHJlc3MtaXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5jb21wb25lbnQuaHRtbCcsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0FkZHJlc3NJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkluaXQge1xyXG4gICAgc3RhdGU6IElTdGF0ZTtcclxuICAgIEBJbnB1dCgpXHJcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdBZGRyZXNzSXRlbU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBlZGl0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNwbGF5RW1wdHk6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ2NpdHlJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSBjaXR5SW5wdXQ6IGFueTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdzdHJlZXQxSW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgc3RyZWV0MUlucHV0OiBhbnk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnc3RyZWV0MklucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIHN0cmVldDJJbnB1dDogYW55O1xyXG5cclxuICAgIHN0YXRlczogSVN0YXRlW107XHJcbiAgICBsZ2FzOiBJTEdBW107XHJcblxyXG4gICAgaW5FZGl0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBlZGl0ZWRTdHJlZXQxOiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRTdHJlZXQyOiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRDaXR5OiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRMZ2E6IElMR0E7XHJcbiAgICBlcnJvck1lc3NhZ2VzOiBzdHJpbmdbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBzdGF0ZVNlcnZpY2U6IFN0YXRlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbGdhU2VydmljZTogTGdhU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZWRpdGVkU3RyZWV0MSA9IHRoaXMucHJvcGVydHkudmFsdWUuc3RyZWV0MTtcclxuICAgICAgICB0aGlzLmVkaXRlZFN0cmVldDIgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnN0cmVldDI7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRDaXR5ID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5jaXR5O1xyXG4gICAgICAgIHRoaXMuZWRpdGVkTGdhID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5sZ2E7XHJcbiAgICAgICAgaWYgKHRoaXMuZWRpdGVkTGdhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLmxnYS5zdGF0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgb25DaGFuZ2UoZXZlbnQ6IE1hdFNlbGVjdENoYW5nZSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubGdhU2VydmljZS5maW5kQnlTdGF0ZShldmVudC52YWx1ZS5pZCkuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLmxnYXMgPSByZXMuYm9keSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd1Byb3BlcnR5KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlFbXB0eSB8fCAhdGhpcy5wcm9wZXJ0eS5pc0VtcHR5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzQ2xpY2thYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BlcnR5LmNsaWNrYWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNJY29uKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAhIXRoaXMucHJvcGVydHkuaWNvbjtcclxuICAgIH1cclxuXHJcbiAgICBoYXNFcnJvcnMoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lcnJvck1lc3NhZ2VzICYmIHRoaXMuZXJyb3JNZXNzYWdlcy5sZW5ndGg7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0RWRpdE1vZGUoZWRpdFN0YXR1czogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIGlmIChlZGl0U3RhdHVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGVTZXJ2aWNlLmdldFN0YXRlcygpLnN1YnNjcmliZShyZXMgPT4gdGhpcy5zdGF0ZXMgPSByZXMuYm9keSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaW5FZGl0ID0gZWRpdFN0YXR1cztcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuY2l0eUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNpdHlJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RyZWV0MUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0cmVldDFJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RyZWV0MklucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0cmVldDJJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVkaXRlZFN0cmVldDEgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnN0cmVldDE7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRTdHJlZXQyID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5zdHJlZXQyO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkQ2l0eSA9IHRoaXMucHJvcGVydHkudmFsdWUuY2l0eTtcclxuICAgICAgICB0aGlzLmVkaXRlZExnYSA9IHRoaXMucHJvcGVydHkudmFsdWUubGdhO1xyXG4gICAgICAgIGlmICh0aGlzLmVkaXRlZExnYSkge1xyXG4gICAgICAgICAgICB0aGlzLnN0YXRlID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5sZ2Euc3RhdGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0RWRpdE1vZGUoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5wcm9wZXJ0eS5pc1ZhbGlkKG5ldyBBZGRyZXNzKHRoaXMuZWRpdGVkU3RyZWV0MSwgdGhpcy5lZGl0ZWRTdHJlZXQyLCB0aGlzLmVkaXRlZENpdHksIHRoaXMuZWRpdGVkTGdhKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UudXBkYXRlKHRoaXMucHJvcGVydHksXHJcbiAgICAgICAgICAgICAgICBuZXcgQWRkcmVzcyh0aGlzLmVkaXRlZFN0cmVldDEsIHRoaXMuZWRpdGVkU3RyZWV0MiwgdGhpcy5lZGl0ZWRDaXR5LCB0aGlzLmVkaXRlZExnYSkpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BlcnR5LnZhbHVlID0gbmV3IEFkZHJlc3ModGhpcy5lZGl0ZWRTdHJlZXQxLCB0aGlzLmVkaXRlZFN0cmVldDIsIHRoaXMuZWRpdGVkQ2l0eSwgdGhpcy5lZGl0ZWRMZ2EpO1xyXG4gICAgICAgICAgICB0aGlzLnNldEVkaXRNb2RlKGZhbHNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yTWVzc2FnZXMgPSB0aGlzLnByb3BlcnR5LmdldFZhbGlkYXRpb25FcnJvcnMobmV3IEFkZHJlc3ModGhpcy5lZGl0ZWRTdHJlZXQxLCB0aGlzLmVkaXRlZFN0cmVldDIsIHRoaXMuZWRpdGVkQ2l0eSwgdGhpcy5lZGl0ZWRMZ2EpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wZXJ0eS5kaXNwbGF5VmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgY2xpY2tlZCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS5jbGlja2VkKHRoaXMucHJvcGVydHkpO1xyXG4gICAgfVxyXG5cclxuICAgIGVudGl0eUNvbXBhcmUoczE6IGFueSwgczI6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBzMSAmJiBzMiA/IHMxLmlkID09IHMyLmlkIDogczEgPT09IHMyO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==