import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { CardViewHtmlTextItemModel } from '../../models/card-view-html-text-item.model';
var CardViewHtmlTextItemComponent = /** @class */ (function () {
    function CardViewHtmlTextItemComponent() {
    }
    CardViewHtmlTextItemComponent.prototype.ngOnChanges = function () {
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", CardViewHtmlTextItemModel)
    ], CardViewHtmlTextItemComponent.prototype, "property", void 0);
    CardViewHtmlTextItemComponent = tslib_1.__decorate([
        Component({
            selector: 'tradcard-view-html-text',
            template: "<div class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <span>\r\n        <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n            <span [innerHtml]=\"property.displayValue\"></span>\r\n        </span>\r\n    </span>\r\n</div>\r\n",
            styles: [""]
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], CardViewHtmlTextItemComponent);
    return CardViewHtmlTextItemComponent;
}());
export { CardViewHtmlTextItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1odG1sLXRleHQtaXRlbS9jYXJkLXZpZXctaHRtbC10ZXh0LWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQU94RjtJQUlJO0lBQ0EsQ0FBQztJQUVELG1EQUFXLEdBQVg7SUFDQSxDQUFDO0lBTkQ7UUFEQyxLQUFLLEVBQUU7MENBQ0UseUJBQXlCO21FQUFDO0lBRjNCLDZCQUE2QjtRQUx6QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUseUJBQXlCO1lBQ25DLGlWQUF3RDs7U0FFM0QsQ0FBQzs7T0FDVyw2QkFBNkIsQ0FVekM7SUFBRCxvQ0FBQztDQUFBLEFBVkQsSUFVQztTQVZZLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0h0bWxUZXh0SXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1odG1sLXRleHQtaXRlbS5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAndHJhZGNhcmQtdmlldy1odG1sLXRleHQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1odG1sLXRleHQtaXRlbS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jYXJkLXZpZXctaHRtbC10ZXh0LWl0ZW0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdIdG1sVGV4dEl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG4gICAgQElucHV0KClcclxuICAgIHByb3BlcnR5OiBDYXJkVmlld0h0bWxUZXh0SXRlbU1vZGVsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCk6IHZvaWQge1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=