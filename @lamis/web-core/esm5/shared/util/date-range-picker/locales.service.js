import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { DefaultLocaleConfig, LOCALE_CONFIG } from './date-range-picker.config';
var LocaleService = /** @class */ (function () {
    function LocaleService(_config) {
        this._config = _config;
    }
    Object.defineProperty(LocaleService.prototype, "config", {
        get: function () {
            if (!this._config) {
                return DefaultLocaleConfig;
            }
            return tslib_1.__assign({}, DefaultLocaleConfig, this._config);
        },
        enumerable: true,
        configurable: true
    });
    LocaleService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
    ]; };
    LocaleService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__param(0, Inject(LOCALE_CONFIG)),
        tslib_1.__metadata("design:paramtypes", [Object])
    ], LocaleService);
    return LocaleService;
}());
export { LocaleService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxlcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvbG9jYWxlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFnQixNQUFNLDRCQUE0QixDQUFDO0FBRzlGO0lBQ0MsdUJBQTJDLE9BQXFCO1FBQXJCLFlBQU8sR0FBUCxPQUFPLENBQWM7SUFDaEUsQ0FBQztJQUVELHNCQUFJLGlDQUFNO2FBQVY7WUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDbEIsT0FBTyxtQkFBbUIsQ0FBQzthQUMzQjtZQUVELDRCQUFXLG1CQUFtQixFQUFLLElBQUksQ0FBQyxPQUFPLEVBQUM7UUFDakQsQ0FBQzs7O09BQUE7O2dEQVRZLE1BQU0sU0FBQyxhQUFhOztJQURyQixhQUFhO1FBRHpCLFVBQVUsRUFBRTtRQUVDLG1CQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTs7T0FEdEIsYUFBYSxDQVd6QjtJQUFELG9CQUFDO0NBQUEsQUFYRCxJQVdDO1NBWFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEZWZhdWx0TG9jYWxlQ29uZmlnLCBMT0NBTEVfQ09ORklHLCBMb2NhbGVDb25maWcgfSBmcm9tICcuL2RhdGUtcmFuZ2UtcGlja2VyLmNvbmZpZyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBMb2NhbGVTZXJ2aWNlIHtcclxuXHRjb25zdHJ1Y3RvcihASW5qZWN0KExPQ0FMRV9DT05GSUcpIHByaXZhdGUgX2NvbmZpZzogTG9jYWxlQ29uZmlnKSB7XHJcblx0fVxyXG5cclxuXHRnZXQgY29uZmlnKCkge1xyXG5cdFx0aWYgKCF0aGlzLl9jb25maWcpIHtcclxuXHRcdFx0cmV0dXJuIERlZmF1bHRMb2NhbGVDb25maWc7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHsuLi5EZWZhdWx0TG9jYWxlQ29uZmlnLCAuLi50aGlzLl9jb25maWd9XHJcblx0fVxyXG59XHJcbiJdfQ==