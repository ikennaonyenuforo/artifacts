import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab-animation';
var SpeedDialFabComponent = /** @class */ (function () {
    function SpeedDialFabComponent() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
        this.buttonState = [];
    }
    SpeedDialFabComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.links.forEach(function (link) { return _this.buttonState.push(link); });
    };
    SpeedDialFabComponent.prototype.showItems = function () {
        this.fabTogglerState = 'active';
        this.buttons = this.buttonState;
    };
    SpeedDialFabComponent.prototype.hideItems = function () {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
    };
    SpeedDialFabComponent.prototype.onToggleFab = function () {
        this.buttons.length ? this.hideItems() : this.showItems();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], SpeedDialFabComponent.prototype, "links", void 0);
    SpeedDialFabComponent = tslib_1.__decorate([
        Component({
            selector: 'speed-dial',
            template: "<div class=\"fab-container\">\r\n    <button mat-fab class=\"fab-toggler\"\r\n            (click)=\"onToggleFab()\">\r\n        <mat-icon [@fabToggler]=\"{value: fabTogglerState}\">add</mat-icon>\r\n    </button>\r\n    <div [@speedDialStagger]=\"buttons.length\">\r\n        <ng-container *ngFor=\"let btn of buttons\">\r\n            <button mat-mini-fab\r\n                    *jhiHasAnyAuthority=\"btn.roles\"\r\n                    matTooltip=\"{{btn.tooltip}}\"\r\n                    [routerLink]=\"['.', btn.state, 'new']\"\r\n                    class=\"fab-secondary\"\r\n                    color=\"accent\">\r\n                <mat-icon>{{btn.icon}}</mat-icon>\r\n            </button>\r\n        </ng-container>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"fab-dismiss\"\r\n     *ngIf=\"fabTogglerState==='active'\"\r\n     (click)=\"onToggleFab()\">\r\n</div>\r\n",
            animations: speedDialFabAnimations,
            styles: [""]
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SpeedDialFabComponent);
    return SpeedDialFabComponent;
}());
export { SpeedDialFabComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvc3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQVFwRTtJQVlJO1FBSkEsb0JBQWUsR0FBRyxVQUFVLENBQUM7UUFDN0IsWUFBTyxHQUFVLEVBQUUsQ0FBQztRQUNwQixnQkFBVyxHQUFVLEVBQUUsQ0FBQztJQUl4QixDQUFDO0lBYkQsd0NBQVEsR0FBUjtRQUFBLGlCQUVDO1FBREcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFhRCx5Q0FBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3BDLENBQUM7SUFFRCx5Q0FBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUM7UUFDbEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUE7SUFDckIsQ0FBQztJQUVELDJDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDOUQsQ0FBQztJQXRCRDtRQURDLEtBQUssRUFBRTs7d0RBQzhEO0lBTjdELHFCQUFxQjtRQU5qQyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsWUFBWTtZQUN0QixxM0JBQThDO1lBRTlDLFVBQVUsRUFBRSxzQkFBc0I7O1NBQ3JDLENBQUM7O09BQ1cscUJBQXFCLENBNkJqQztJQUFELDRCQUFDO0NBQUEsQUE3QkQsSUE2QkM7U0E3QlkscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IHNwZWVkRGlhbEZhYkFuaW1hdGlvbnMgfSBmcm9tICcuL3NwZWVkLWRpYWwtZmFiLWFuaW1hdGlvbic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnc3BlZWQtZGlhbCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc3BlZWQtZGlhbC1mYWIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vc3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGFuaW1hdGlvbnM6IHNwZWVkRGlhbEZhYkFuaW1hdGlvbnNcclxufSlcclxuZXhwb3J0IGNsYXNzIFNwZWVkRGlhbEZhYkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmxpbmtzLmZvckVhY2gobGluayA9PiB0aGlzLmJ1dHRvblN0YXRlLnB1c2gobGluaykpO1xyXG4gICAgfVxyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBsaW5rczogeyBzdGF0ZTogc3RyaW5nLCBpY29uOiBzdHJpbmcsIHRvb2x0aXA6IHN0cmluZywgcm9sZXM/OiBbXSB9W107XHJcblxyXG4gICAgZmFiVG9nZ2xlclN0YXRlID0gJ2luYWN0aXZlJztcclxuICAgIGJ1dHRvbnM6IGFueVtdID0gW107XHJcbiAgICBidXR0b25TdGF0ZTogYW55W10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0l0ZW1zKCkge1xyXG4gICAgICAgIHRoaXMuZmFiVG9nZ2xlclN0YXRlID0gJ2FjdGl2ZSc7XHJcbiAgICAgICAgdGhpcy5idXR0b25zID0gdGhpcy5idXR0b25TdGF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBoaWRlSXRlbXMoKSB7XHJcbiAgICAgICAgdGhpcy5mYWJUb2dnbGVyU3RhdGUgPSAnaW5hY3RpdmUnO1xyXG4gICAgICAgIHRoaXMuYnV0dG9ucyA9IFtdXHJcbiAgICB9XHJcblxyXG4gICAgb25Ub2dnbGVGYWIoKSB7XHJcbiAgICAgICAgdGhpcy5idXR0b25zLmxlbmd0aCA/IHRoaXMuaGlkZUl0ZW1zKCkgOiB0aGlzLnNob3dJdGVtcygpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==