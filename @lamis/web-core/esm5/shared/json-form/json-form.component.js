import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { LayoutTemplateService } from '../../services/layout.template.service';
import { FormioComponent } from 'angular-material-formio';
var JsonFormComponent = /** @class */ (function () {
    function JsonFormComponent(templateService) {
        this.templateService = templateService;
        this.dataEvent = new EventEmitter(true);
        this.form = {};
        this.isValid = false;
    }
    JsonFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.templateService.getTemplate(this.template).subscribe(function (json) {
            _this.form = json.template;
        });
    };
    JsonFormComponent.prototype.reset = function () {
        this.model = this._defaultValue;
    };
    JsonFormComponent.prototype.change = function (event) {
        this.isValid = event.isValid;
        if (this.isValid) {
            this.dataEvent.emit(event.data);
        }
    };
    JsonFormComponent.prototype.ngOnChanges = function (changes) {
        if (changes['model']) {
            this.model = changes['model'];
            this._defaultValue = changes['model'];
            this.formio.submission = this.model;
        }
    };
    JsonFormComponent.ctorParameters = function () { return [
        { type: LayoutTemplateService }
    ]; };
    tslib_1.__decorate([
        ViewChild(FormioComponent, { static: true }),
        tslib_1.__metadata("design:type", FormioComponent)
    ], JsonFormComponent.prototype, "formio", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], JsonFormComponent.prototype, "template", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], JsonFormComponent.prototype, "model", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], JsonFormComponent.prototype, "dataEvent", void 0);
    JsonFormComponent = tslib_1.__decorate([
        Component({
            selector: 'json-form',
            template: "\n        <mat-formio [form]=\"form\"\n                    [submission]=\"model\"\n                    [options]=\"\"\n                    (change)=\"change($event)\">\n        </mat-formio>\n    "
        }),
        tslib_1.__metadata("design:paramtypes", [LayoutTemplateService])
    ], JsonFormComponent);
    return JsonFormComponent;
}());
export { JsonFormComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQWlCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMvRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFZMUQ7SUFhSSwyQkFBb0IsZUFBc0M7UUFBdEMsb0JBQWUsR0FBZixlQUFlLENBQXVCO1FBTDFELGNBQVMsR0FBc0IsSUFBSSxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEQsU0FBSSxHQUFRLEVBQUUsQ0FBQztRQUNmLFlBQU8sR0FBRyxLQUFLLENBQUM7SUFJaEIsQ0FBQztJQUVELG9DQUFRLEdBQVI7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFTO1lBQ2hFLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQ3BDLENBQUM7SUFFRCxrQ0FBTSxHQUFOLFVBQU8sS0FBVTtRQUNiLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUM3QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkM7SUFDTCxDQUFDO0lBRUQsdUNBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQzlCLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFDO1lBQ2pCLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDdkM7SUFDTCxDQUFDOztnQkExQm9DLHFCQUFxQjs7SUFYMUQ7UUFEQyxTQUFTLENBQUMsZUFBZSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOzBDQUNuQyxlQUFlO3FEQUFDO0lBRXhCO1FBREMsS0FBSyxFQUFFOzt1REFDUztJQUVqQjtRQURDLEtBQUssRUFBRTs7b0RBQ0c7SUFFWDtRQURDLE1BQU0sRUFBRTswQ0FDRSxZQUFZO3dEQUErQjtJQVI3QyxpQkFBaUI7UUFWN0IsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFdBQVc7WUFDckIsUUFBUSxFQUFFLHNNQU1UO1NBQ0osQ0FBQztpREFjdUMscUJBQXFCO09BYmpELGlCQUFpQixDQXdDN0I7SUFBRCx3QkFBQztDQUFBLEFBeENELElBd0NDO1NBeENZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQsIE91dHB1dCwgU2ltcGxlQ2hhbmdlcywgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IExheW91dFRlbXBsYXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xheW91dC50ZW1wbGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybWlvQ29tcG9uZW50IH0gZnJvbSAnYW5ndWxhci1tYXRlcmlhbC1mb3JtaW8nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2pzb24tZm9ybScsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxtYXQtZm9ybWlvIFtmb3JtXT1cImZvcm1cIlxyXG4gICAgICAgICAgICAgICAgICAgIFtzdWJtaXNzaW9uXT1cIm1vZGVsXCJcclxuICAgICAgICAgICAgICAgICAgICBbb3B0aW9uc109XCJcIlxyXG4gICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwiY2hhbmdlKCRldmVudClcIj5cclxuICAgICAgICA8L21hdC1mb3JtaW8+XHJcbiAgICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBKc29uRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuICAgIEBWaWV3Q2hpbGQoRm9ybWlvQ29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSlcclxuICAgIGZvcm1pbzogRm9ybWlvQ29tcG9uZW50O1xyXG4gICAgQElucHV0KClcclxuICAgIHRlbXBsYXRlOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgbW9kZWw6IGFueTtcclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZGF0YUV2ZW50OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIodHJ1ZSk7XHJcbiAgICBmb3JtOiBhbnkgPSB7fTtcclxuICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgIHByaXZhdGUgX2RlZmF1bHRWYWx1ZTogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdGVtcGxhdGVTZXJ2aWNlOiBMYXlvdXRUZW1wbGF0ZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnRlbXBsYXRlU2VydmljZS5nZXRUZW1wbGF0ZSh0aGlzLnRlbXBsYXRlKS5zdWJzY3JpYmUoKGpzb246IGFueSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0gPSBqc29uLnRlbXBsYXRlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMubW9kZWwgPSB0aGlzLl9kZWZhdWx0VmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlKGV2ZW50OiBhbnkpIHtcclxuICAgICAgICB0aGlzLmlzVmFsaWQgPSBldmVudC5pc1ZhbGlkO1xyXG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQpIHtcclxuICAgICAgICAgICAgdGhpcy5kYXRhRXZlbnQuZW1pdChldmVudC5kYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgICAgIGlmIChjaGFuZ2VzWydtb2RlbCddKXtcclxuICAgICAgICAgICAgdGhpcy5tb2RlbCA9IGNoYW5nZXNbJ21vZGVsJ107XHJcbiAgICAgICAgICAgIHRoaXMuX2RlZmF1bHRWYWx1ZSA9IGNoYW5nZXNbJ21vZGVsJ107XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybWlvLnN1Ym1pc3Npb24gPSB0aGlzLm1vZGVsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=