import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { MatFormioModule } from 'angular-material-formio';
import { JsonFormComponent } from './json-form.component';
var JsonFormModule = /** @class */ (function () {
    function JsonFormModule() {
    }
    JsonFormModule = tslib_1.__decorate([
        NgModule({
            imports: [MatFormioModule],
            declarations: [JsonFormComponent],
            exports: [JsonFormComponent, MatFormioModule],
            entryComponents: [JsonFormComponent]
        })
    ], JsonFormModule);
    return JsonFormModule;
}());
export { JsonFormModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFRMUQ7SUFBQTtJQUVBLENBQUM7SUFGWSxjQUFjO1FBTjFCLFFBQVEsQ0FBQztZQUNOLE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQztZQUMxQixZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztZQUNqQyxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxlQUFlLENBQUM7WUFDN0MsZUFBZSxFQUFFLENBQUMsaUJBQWlCLENBQUM7U0FDdkMsQ0FBQztPQUNXLGNBQWMsQ0FFMUI7SUFBRCxxQkFBQztDQUFBLEFBRkQsSUFFQztTQUZZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRGb3JtaW9Nb2R1bGUgfSBmcm9tICdhbmd1bGFyLW1hdGVyaWFsLWZvcm1pbyc7XHJcbmltcG9ydCB7IEpzb25Gb3JtQ29tcG9uZW50IH0gZnJvbSAnLi9qc29uLWZvcm0uY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbTWF0Rm9ybWlvTW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0pzb25Gb3JtQ29tcG9uZW50XSxcclxuICAgIGV4cG9ydHM6IFtKc29uRm9ybUNvbXBvbmVudCwgTWF0Rm9ybWlvTW9kdWxlXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW0pzb25Gb3JtQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSnNvbkZvcm1Nb2R1bGUge1xyXG5cclxufVxyXG4iXX0=