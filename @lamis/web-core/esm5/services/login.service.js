import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AccountService } from '../core/auth/account.service';
import { AuthServerProvider } from '../core/auth/auth-jwt.service';
import * as i0 from "@angular/core";
import * as i1 from "../core/auth/account.service";
import * as i2 from "../core/auth/auth-jwt.service";
var LoginService = /** @class */ (function () {
    function LoginService(accountService, authServerProvider) {
        this.accountService = accountService;
        this.authServerProvider = authServerProvider;
    }
    LoginService.prototype.login = function (credentials, callback) {
        var _this = this;
        var cb = callback || function () {
        };
        return new Promise(function (resolve, reject) {
            _this.authServerProvider.login(credentials).subscribe(function (data) {
                _this.accountService.identity(true).then(function (account) {
                    resolve(data);
                });
                return cb();
            }, function (err) {
                _this.logout();
                reject(err);
                return cb(err);
            });
        });
    };
    LoginService.prototype.loginWithToken = function (jwt, rememberMe) {
        return this.authServerProvider.loginWithToken(jwt, rememberMe);
    };
    LoginService.prototype.logout = function () {
        this.authServerProvider.logout().subscribe();
        this.accountService.authenticate(null);
    };
    LoginService.ctorParameters = function () { return [
        { type: AccountService },
        { type: AuthServerProvider }
    ]; };
    LoginService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.ɵɵinject(i1.AccountService), i0.ɵɵinject(i2.AuthServerProvider)); }, token: LoginService, providedIn: "root" });
    LoginService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [AccountService, AuthServerProvider])
    ], LoginService);
    return LoginService;
}());
export { LoginService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQzlELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLCtCQUErQixDQUFDOzs7O0FBSW5FO0lBQ0Msc0JBQW9CLGNBQThCLEVBQVUsa0JBQXNDO1FBQTlFLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7SUFDbEcsQ0FBQztJQUVELDRCQUFLLEdBQUwsVUFBTSxXQUFXLEVBQUUsUUFBUztRQUE1QixpQkFtQkM7UUFsQkEsSUFBTSxFQUFFLEdBQUcsUUFBUSxJQUFJO1FBQ3ZCLENBQUMsQ0FBQztRQUVGLE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUNsQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FDbkQsVUFBQSxJQUFJO2dCQUNILEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87b0JBQzlDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZixDQUFDLENBQUMsQ0FBQztnQkFDSCxPQUFPLEVBQUUsRUFBRSxDQUFDO1lBQ2IsQ0FBQyxFQUNELFVBQUEsR0FBRztnQkFDRixLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ2QsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hCLENBQUMsQ0FDRCxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQscUNBQWMsR0FBZCxVQUFlLEdBQUcsRUFBRSxVQUFVO1FBQzdCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVELDZCQUFNLEdBQU47UUFDQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7Z0JBL0JtQyxjQUFjO2dCQUE4QixrQkFBa0I7OztJQUR0RixZQUFZO1FBRHhCLFVBQVUsQ0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUMsQ0FBQztpREFFSSxjQUFjLEVBQThCLGtCQUFrQjtPQUR0RixZQUFZLENBaUN4Qjt1QkF2Q0Q7Q0F1Q0MsQUFqQ0QsSUFpQ0M7U0FqQ1ksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWNjb3VudFNlcnZpY2UgfSBmcm9tICcuLi9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aFNlcnZlclByb3ZpZGVyIH0gZnJvbSAnLi4vY29yZS9hdXRoL2F1dGgtand0LnNlcnZpY2UnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGFjY291bnRTZXJ2aWNlOiBBY2NvdW50U2VydmljZSwgcHJpdmF0ZSBhdXRoU2VydmVyUHJvdmlkZXI6IEF1dGhTZXJ2ZXJQcm92aWRlcikge1xyXG5cdH1cclxuXHJcblx0bG9naW4oY3JlZGVudGlhbHMsIGNhbGxiYWNrPykge1xyXG5cdFx0Y29uc3QgY2IgPSBjYWxsYmFjayB8fCBmdW5jdGlvbiAoKSB7XHJcblx0XHR9O1xyXG5cclxuXHRcdHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblx0XHRcdHRoaXMuYXV0aFNlcnZlclByb3ZpZGVyLmxvZ2luKGNyZWRlbnRpYWxzKS5zdWJzY3JpYmUoXHJcblx0XHRcdFx0ZGF0YSA9PiB7XHJcblx0XHRcdFx0XHR0aGlzLmFjY291bnRTZXJ2aWNlLmlkZW50aXR5KHRydWUpLnRoZW4oYWNjb3VudCA9PiB7XHJcblx0XHRcdFx0XHRcdHJlc29sdmUoZGF0YSk7XHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdHJldHVybiBjYigpO1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0ZXJyID0+IHtcclxuXHRcdFx0XHRcdHRoaXMubG9nb3V0KCk7XHJcblx0XHRcdFx0XHRyZWplY3QoZXJyKTtcclxuXHRcdFx0XHRcdHJldHVybiBjYihlcnIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0KTtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0bG9naW5XaXRoVG9rZW4oand0LCByZW1lbWJlck1lKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5hdXRoU2VydmVyUHJvdmlkZXIubG9naW5XaXRoVG9rZW4oand0LCByZW1lbWJlck1lKTtcclxuXHR9XHJcblxyXG5cdGxvZ291dCgpIHtcclxuXHRcdHRoaXMuYXV0aFNlcnZlclByb3ZpZGVyLmxvZ291dCgpLnN1YnNjcmliZSgpO1xyXG5cdFx0dGhpcy5hY2NvdW50U2VydmljZS5hdXRoZW50aWNhdGUobnVsbCk7XHJcblx0fVxyXG59XHJcbiJdfQ==