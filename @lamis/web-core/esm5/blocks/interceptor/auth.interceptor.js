import * as tslib_1 from "tslib";
import { Inject, Injectable, Injector } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(injector, serverUrl) {
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    AuthInterceptor.prototype.intercept = function (request, next) {
        if (!request || !request.url || (/^http/.test(request.url) && !(this.serverUrl.SERVER_API_URL && request.url.startsWith(this.serverUrl.SERVER_API_URL)))) {
            return next.handle(request);
        }
        var token = this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        return next.handle(request);
    };
    AuthInterceptor.ctorParameters = function () { return [
        { type: Injector },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
    ]; };
    AuthInterceptor = tslib_1.__decorate([
        Injectable(),
        tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG)),
        tslib_1.__metadata("design:paramtypes", [Injector, Object])
    ], AuthInterceptor);
    return AuthInterceptor;
}());
export { AuthInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImJsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFN0QsT0FBTyxFQUFFLG1CQUFtQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sV0FBVyxDQUFDO0FBRXZFLE9BQU8sRUFBRSxxQkFBcUIsRUFBc0IsTUFBTSxxQkFBcUIsQ0FBQztBQUloRjtJQUlDLHlCQUFvQixRQUFrQixFQUF5QyxTQUE2QjtRQUF4RixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQXlDLGNBQVMsR0FBVCxTQUFTLENBQW9CO1FBQzNHLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDakUsQ0FBQztJQUVELG1DQUFTLEdBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBRXJELElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3pKLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM1QjtRQUVELElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUMvRyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUU7WUFDWixPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDdkIsVUFBVSxFQUFFO29CQUNYLGFBQWEsRUFBRSxTQUFTLEdBQUcsS0FBSztpQkFDaEM7YUFDRCxDQUFDLENBQUM7U0FDSDtRQUNELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM3QixDQUFDOztnQkFwQjZCLFFBQVE7Z0RBQUcsTUFBTSxTQUFDLHFCQUFxQjs7SUFKekQsZUFBZTtRQUQzQixVQUFVLEVBQUU7UUFLNkIsbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7aURBQXhDLFFBQVE7T0FKMUIsZUFBZSxDQXlCM0I7SUFBRCxzQkFBQztDQUFBLEFBekJELElBeUJDO1NBekJZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuaW1wb3J0IHsgSHR0cEV2ZW50LCBIdHRwSGFuZGxlciwgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVxdWVzdCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkxfQ09ORklHLCBTZXJ2ZXJBcGlVcmxDb25maWcgfSBmcm9tICcuLi8uLi9hcHAuY29uc3RhbnRzJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBdXRoSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG5cdHByaXZhdGUgJGxvY2FsU3RvcmFnZTogTG9jYWxTdG9yYWdlU2VydmljZTtcclxuXHRwcml2YXRlICRzZXNzaW9uU3RvcmFnZTogU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGluamVjdG9yOiBJbmplY3RvciwgQEluamVjdChTRVJWRVJfQVBJX1VSTF9DT05GSUcpIHByaXZhdGUgc2VydmVyVXJsOiBTZXJ2ZXJBcGlVcmxDb25maWcpIHtcclxuXHRcdHRoaXMuJGxvY2FsU3RvcmFnZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KExvY2FsU3RvcmFnZVNlcnZpY2UpO1xyXG5cdFx0dGhpcy4kc2Vzc2lvblN0b3JhZ2UgPSB0aGlzLmluamVjdG9yLmdldChTZXNzaW9uU3RvcmFnZVNlcnZpY2UpO1xyXG5cdH1cclxuXHJcblx0aW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG5cclxuXHRcdGlmICghcmVxdWVzdCB8fCAhcmVxdWVzdC51cmwgfHwgKC9eaHR0cC8udGVzdChyZXF1ZXN0LnVybCkgJiYgISh0aGlzLnNlcnZlclVybC5TRVJWRVJfQVBJX1VSTCAmJiByZXF1ZXN0LnVybC5zdGFydHNXaXRoKHRoaXMuc2VydmVyVXJsLlNFUlZFUl9BUElfVVJMKSkpKSB7XHJcblx0XHRcdHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcclxuXHRcdH1cclxuXHJcblx0XHRjb25zdCB0b2tlbiA9IHRoaXMuJGxvY2FsU3RvcmFnZS5nZXQoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKSB8fCB0aGlzLiRzZXNzaW9uU3RvcmFnZS5nZXQoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKTtcclxuXHRcdGlmICghIXRva2VuKSB7XHJcblx0XHRcdHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcclxuXHRcdFx0XHRzZXRIZWFkZXJzOiB7XHJcblx0XHRcdFx0XHRBdXRob3JpemF0aW9uOiAnQmVhcmVyICcgKyB0b2tlblxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0XHRyZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XHJcblx0fVxyXG59XHJcbiJdfQ==