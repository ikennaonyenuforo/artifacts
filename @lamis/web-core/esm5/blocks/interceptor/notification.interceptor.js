import * as tslib_1 from "tslib";
import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NotificationService } from '@alfresco/adf-core';
var NotificationInterceptor = /** @class */ (function () {
    function NotificationInterceptor(notificationService) {
        this.notificationService = notificationService;
    }
    NotificationInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(tap(function (event) {
            if (event instanceof HttpResponse) {
                var arr = event.headers.keys();
                var alert_1 = null;
                arr.forEach(function (entry) {
                    if (entry.toLowerCase().endsWith('app-alert')) {
                        alert_1 = event.headers.get(entry);
                    }
                });
                if (alert_1) {
                    if (typeof alert_1 === 'string') {
                        _this.notificationService.openSnackMessage(alert_1);
                    }
                }
            }
        }, function (err) { }));
    };
    NotificationInterceptor.ctorParameters = function () { return [
        { type: NotificationService }
    ]; };
    NotificationInterceptor = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [NotificationService])
    ], NotificationInterceptor);
    return NotificationInterceptor;
}());
export { NotificationInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiYmxvY2tzL2ludGVyY2VwdG9yL25vdGlmaWNhdGlvbi5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUF3RCxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUMxRyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUd6RDtJQUNJLGlDQUFvQixtQkFBd0M7UUFBeEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUFHLENBQUM7SUFFaEUsMkNBQVMsR0FBVCxVQUFVLE9BQXlCLEVBQUUsSUFBaUI7UUFBdEQsaUJBc0JDO1FBckJHLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQzVCLEdBQUcsQ0FDQyxVQUFDLEtBQXFCO1lBQ2xCLElBQUksS0FBSyxZQUFZLFlBQVksRUFBRTtnQkFDL0IsSUFBTSxHQUFHLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDakMsSUFBSSxPQUFLLEdBQUcsSUFBSSxDQUFDO2dCQUNqQixHQUFHLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztvQkFDYixJQUFJLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUU7d0JBQzNDLE9BQUssR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDcEM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxPQUFLLEVBQUU7b0JBQ1AsSUFBSSxPQUFPLE9BQUssS0FBSyxRQUFRLEVBQUU7d0JBQzNCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFLLENBQUMsQ0FBQztxQkFDcEQ7aUJBQ0o7YUFDSjtRQUNMLENBQUMsRUFDRCxVQUFDLEdBQVEsSUFBTSxDQUFDLENBQ25CLENBQ0osQ0FBQztJQUNOLENBQUM7O2dCQXhCd0MsbUJBQW1COztJQURuRCx1QkFBdUI7UUFEbkMsVUFBVSxFQUFFO2lEQUVnQyxtQkFBbUI7T0FEbkQsdUJBQXVCLENBMEJuQztJQUFELDhCQUFDO0NBQUEsQUExQkQsSUEwQkM7U0ExQlksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cEV2ZW50LCBIdHRwSGFuZGxlciwgSHR0cEludGVyY2VwdG9yLCBIdHRwUmVxdWVzdCwgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbkludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbm90aWZpY2F0aW9uU2VydmljZTogTm90aWZpY2F0aW9uU2VydmljZSkge31cclxuXHJcbiAgICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICAgICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpLnBpcGUoXHJcbiAgICAgICAgICAgIHRhcChcclxuICAgICAgICAgICAgICAgIChldmVudDogSHR0cEV2ZW50PGFueT4pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnQgaW5zdGFuY2VvZiBIdHRwUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgYXJyID0gZXZlbnQuaGVhZGVycy5rZXlzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBhbGVydCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFyci5mb3JFYWNoKGVudHJ5ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbnRyeS50b0xvd2VyQ2FzZSgpLmVuZHNXaXRoKCdhcHAtYWxlcnQnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0ID0gZXZlbnQuaGVhZGVycy5nZXQoZW50cnkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFsZXJ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGFsZXJ0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5vcGVuU25hY2tNZXNzYWdlKGFsZXJ0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyOiBhbnkpID0+IHt9XHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==