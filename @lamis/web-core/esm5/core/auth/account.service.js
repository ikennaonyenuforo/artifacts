import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../app.constants";
var AccountService = /** @class */ (function () {
    function AccountService(http, apiUrlConfig) {
        this.http = http;
        this.apiUrlConfig = apiUrlConfig;
        this.authenticated = false;
        this.authenticationState = new Subject();
    }
    AccountService.prototype.fetch = function () {
        return this.http.get(this.apiUrlConfig.SERVER_API_URL + 'api/account', { observe: 'response' });
    };
    AccountService.prototype.save = function (account) {
        return this.http.post(this.apiUrlConfig.SERVER_API_URL + 'api/account', account, { observe: 'response' });
    };
    AccountService.prototype.authenticate = function (identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    };
    AccountService.prototype.hasAnyAuthority = function (authorities) {
        if (authorities === undefined || authorities.length === 0) {
            return true;
        }
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }
        for (var i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.includes(authorities[i])) {
                return true;
            }
        }
        return false;
    };
    AccountService.prototype.hasAuthority = function (authority) {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }
        return this.identity().then(function (id) {
            return Promise.resolve(id.authorities && id.authorities.includes(authority));
        }, function () {
            return Promise.resolve(false);
        });
    };
    AccountService.prototype.identity = function (force) {
        var _this = this;
        if (force) {
            this.userIdentity = undefined;
        }
        // check and see if we have retrieved the userIdentity data from the server.
        // if we have, reuse it by immediately resolving
        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }
        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.fetch()
            .toPromise()
            .then(function (response) {
            var account = response.body;
            if (account) {
                _this.userIdentity = account;
                _this.authenticated = true;
            }
            else {
                _this.userIdentity = null;
                _this.authenticated = false;
            }
            _this.authenticationState.next(_this.userIdentity);
            return _this.userIdentity;
        })
            .catch(function (err) {
            _this.userIdentity = null;
            _this.authenticated = false;
            _this.authenticationState.next(_this.userIdentity);
            return null;
        });
    };
    AccountService.prototype.isAuthenticated = function () {
        return this.authenticated;
    };
    AccountService.prototype.isIdentityResolved = function () {
        return this.userIdentity !== undefined;
    };
    AccountService.prototype.getAuthenticationState = function () {
        return this.authenticationState.asObservable();
    };
    AccountService.prototype.getImageUrl = function () {
        return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
    };
    AccountService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
    ]; };
    AccountService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AccountService_Factory() { return new AccountService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: AccountService, providedIn: "root" });
    AccountService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG)),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Object])
    ], AccountService);
    return AccountService;
}());
export { AccountService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiY29yZS9hdXRoL2FjY291bnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNoRSxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxxQkFBcUIsRUFBc0IsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQU1oRjtJQUtDLHdCQUFvQixJQUFnQixFQUF5QyxZQUFnQztRQUF6RixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQXlDLGlCQUFZLEdBQVosWUFBWSxDQUFvQjtRQUhyRyxrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0Qix3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO0lBR2pELENBQUM7SUFFRCw4QkFBSyxHQUFMO1FBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsR0FBRyxhQUFhLEVBQUUsRUFBQyxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUN4RyxDQUFDO0lBRUQsNkJBQUksR0FBSixVQUFLLE9BQVk7UUFDaEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsR0FBRyxhQUFhLEVBQUUsT0FBTyxFQUFFLEVBQUMsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQUM7SUFDekcsQ0FBQztJQUVELHFDQUFZLEdBQVosVUFBYSxRQUFRO1FBQ3BCLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO1FBQzdCLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxLQUFLLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQsd0NBQWUsR0FBZixVQUFnQixXQUFxQjtRQUNwQyxJQUFJLFdBQVcsS0FBSyxTQUFTLElBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDMUQsT0FBTyxJQUFJLENBQUE7U0FDWDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFO1lBQ2hGLE9BQU8sS0FBSyxDQUFDO1NBQ2I7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDM0QsT0FBTyxJQUFJLENBQUM7YUFDWjtTQUNEO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZCxDQUFDO0lBRUQscUNBQVksR0FBWixVQUFhLFNBQWlCO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3hCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjtRQUVELE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksQ0FDMUIsVUFBQSxFQUFFO1lBQ0QsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUM5RSxDQUFDLEVBQ0Q7WUFDQyxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUNELENBQUM7SUFDSCxDQUFDO0lBRUQsaUNBQVEsR0FBUixVQUFTLEtBQWU7UUFBeEIsaUJBZ0NDO1FBL0JBLElBQUksS0FBSyxFQUFFO1lBQ1YsSUFBSSxDQUFDLFlBQVksR0FBRyxTQUFTLENBQUM7U0FDOUI7UUFFRCw0RUFBNEU7UUFDNUUsZ0RBQWdEO1FBQ2hELElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN0QixPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQzFDO1FBRUQsZ0dBQWdHO1FBQ2hHLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRTthQUNqQixTQUFTLEVBQUU7YUFDWCxJQUFJLENBQUMsVUFBQSxRQUFRO1lBQ2IsSUFBTSxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUM5QixJQUFJLE9BQU8sRUFBRTtnQkFDWixLQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQztnQkFDNUIsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7YUFDMUI7aUJBQU07Z0JBQ04sS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO2FBQzNCO1lBQ0QsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDakQsT0FBTyxLQUFJLENBQUMsWUFBWSxDQUFDO1FBQzFCLENBQUMsQ0FBQzthQUNELEtBQUssQ0FBQyxVQUFBLEdBQUc7WUFDVCxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixLQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztZQUMzQixLQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNqRCxPQUFPLElBQUksQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHdDQUFlLEdBQWY7UUFDQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDM0IsQ0FBQztJQUVELDJDQUFrQixHQUFsQjtRQUNDLE9BQU8sSUFBSSxDQUFDLFlBQVksS0FBSyxTQUFTLENBQUM7SUFDeEMsQ0FBQztJQUVELCtDQUFzQixHQUF0QjtRQUNDLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ2hELENBQUM7SUFFRCxvQ0FBVyxHQUFYO1FBQ0MsT0FBTyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN0RSxDQUFDOztnQkFqR3lCLFVBQVU7Z0RBQUcsTUFBTSxTQUFDLHFCQUFxQjs7O0lBTHZELGNBQWM7UUFIMUIsVUFBVSxDQUFDO1lBQ1gsVUFBVSxFQUFFLE1BQU07U0FDbEIsQ0FBQztRQU1zQyxtQkFBQSxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQTtpREFBMUMsVUFBVTtPQUx4QixjQUFjLENBdUcxQjt5QkFoSEQ7Q0FnSEMsQUF2R0QsSUF1R0M7U0F2R1ksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkxfQ09ORklHLCBTZXJ2ZXJBcGlVcmxDb25maWcgfSBmcm9tICcuLi8uLi9hcHAuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQWNjb3VudCB9IGZyb20gJy4uL3VzZXIvYWNjb3VudC5tb2RlbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcblx0cHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBY2NvdW50U2VydmljZSB7XHJcblx0cHJpdmF0ZSB1c2VySWRlbnRpdHk6IGFueTtcclxuXHRwcml2YXRlIGF1dGhlbnRpY2F0ZWQgPSBmYWxzZTtcclxuXHRwcml2YXRlIGF1dGhlbnRpY2F0aW9uU3RhdGUgPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCwgQEluamVjdChTRVJWRVJfQVBJX1VSTF9DT05GSUcpIHByaXZhdGUgYXBpVXJsQ29uZmlnOiBTZXJ2ZXJBcGlVcmxDb25maWcpIHtcclxuXHR9XHJcblxyXG5cdGZldGNoKCk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPEFjY291bnQ+PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxBY2NvdW50Pih0aGlzLmFwaVVybENvbmZpZy5TRVJWRVJfQVBJX1VSTCArICdhcGkvYWNjb3VudCcsIHtvYnNlcnZlOiAncmVzcG9uc2UnfSk7XHJcblx0fVxyXG5cclxuXHRzYXZlKGFjY291bnQ6IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueT4+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmFwaVVybENvbmZpZy5TRVJWRVJfQVBJX1VSTCArICdhcGkvYWNjb3VudCcsIGFjY291bnQsIHtvYnNlcnZlOiAncmVzcG9uc2UnfSk7XHJcblx0fVxyXG5cclxuXHRhdXRoZW50aWNhdGUoaWRlbnRpdHkpIHtcclxuXHRcdHRoaXMudXNlcklkZW50aXR5ID0gaWRlbnRpdHk7XHJcblx0XHR0aGlzLmF1dGhlbnRpY2F0ZWQgPSBpZGVudGl0eSAhPT0gbnVsbDtcclxuXHRcdHRoaXMuYXV0aGVudGljYXRpb25TdGF0ZS5uZXh0KHRoaXMudXNlcklkZW50aXR5KTtcclxuXHR9XHJcblxyXG5cdGhhc0FueUF1dGhvcml0eShhdXRob3JpdGllczogc3RyaW5nW10pOiBib29sZWFuIHtcclxuXHRcdGlmIChhdXRob3JpdGllcyA9PT0gdW5kZWZpbmVkIHx8IGF1dGhvcml0aWVzLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRyZXR1cm4gdHJ1ZVxyXG5cdFx0fVxyXG5cdFx0aWYgKCF0aGlzLmF1dGhlbnRpY2F0ZWQgfHwgIXRoaXMudXNlcklkZW50aXR5IHx8ICF0aGlzLnVzZXJJZGVudGl0eS5hdXRob3JpdGllcykge1xyXG5cdFx0XHRyZXR1cm4gZmFsc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCBhdXRob3JpdGllcy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRpZiAodGhpcy51c2VySWRlbnRpdHkuYXV0aG9yaXRpZXMuaW5jbHVkZXMoYXV0aG9yaXRpZXNbaV0pKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHRoYXNBdXRob3JpdHkoYXV0aG9yaXR5OiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuXHRcdGlmICghdGhpcy5hdXRoZW50aWNhdGVkKSB7XHJcblx0XHRcdHJldHVybiBQcm9taXNlLnJlc29sdmUoZmFsc2UpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiB0aGlzLmlkZW50aXR5KCkudGhlbihcclxuXHRcdFx0aWQgPT4ge1xyXG5cdFx0XHRcdHJldHVybiBQcm9taXNlLnJlc29sdmUoaWQuYXV0aG9yaXRpZXMgJiYgaWQuYXV0aG9yaXRpZXMuaW5jbHVkZXMoYXV0aG9yaXR5KSk7XHJcblx0XHRcdH0sXHJcblx0XHRcdCgpID0+IHtcclxuXHRcdFx0XHRyZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuXHRcdFx0fVxyXG5cdFx0KTtcclxuXHR9XHJcblxyXG5cdGlkZW50aXR5KGZvcmNlPzogYm9vbGVhbik6IFByb21pc2U8YW55PiB7XHJcblx0XHRpZiAoZm9yY2UpIHtcclxuXHRcdFx0dGhpcy51c2VySWRlbnRpdHkgPSB1bmRlZmluZWQ7XHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gY2hlY2sgYW5kIHNlZSBpZiB3ZSBoYXZlIHJldHJpZXZlZCB0aGUgdXNlcklkZW50aXR5IGRhdGEgZnJvbSB0aGUgc2VydmVyLlxyXG5cdFx0Ly8gaWYgd2UgaGF2ZSwgcmV1c2UgaXQgYnkgaW1tZWRpYXRlbHkgcmVzb2x2aW5nXHJcblx0XHRpZiAodGhpcy51c2VySWRlbnRpdHkpIHtcclxuXHRcdFx0cmV0dXJuIFByb21pc2UucmVzb2x2ZSh0aGlzLnVzZXJJZGVudGl0eSk7XHJcblx0XHR9XHJcblxyXG5cdFx0Ly8gcmV0cmlldmUgdGhlIHVzZXJJZGVudGl0eSBkYXRhIGZyb20gdGhlIHNlcnZlciwgdXBkYXRlIHRoZSBpZGVudGl0eSBvYmplY3QsIGFuZCB0aGVuIHJlc29sdmUuXHJcblx0XHRyZXR1cm4gdGhpcy5mZXRjaCgpXHJcblx0XHRcdC50b1Byb21pc2UoKVxyXG5cdFx0XHQudGhlbihyZXNwb25zZSA9PiB7XHJcblx0XHRcdFx0Y29uc3QgYWNjb3VudCA9IHJlc3BvbnNlLmJvZHk7XHJcblx0XHRcdFx0aWYgKGFjY291bnQpIHtcclxuXHRcdFx0XHRcdHRoaXMudXNlcklkZW50aXR5ID0gYWNjb3VudDtcclxuXHRcdFx0XHRcdHRoaXMuYXV0aGVudGljYXRlZCA9IHRydWU7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHRoaXMudXNlcklkZW50aXR5ID0gbnVsbDtcclxuXHRcdFx0XHRcdHRoaXMuYXV0aGVudGljYXRlZCA9IGZhbHNlO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHR0aGlzLmF1dGhlbnRpY2F0aW9uU3RhdGUubmV4dCh0aGlzLnVzZXJJZGVudGl0eSk7XHJcblx0XHRcdFx0cmV0dXJuIHRoaXMudXNlcklkZW50aXR5O1xyXG5cdFx0XHR9KVxyXG5cdFx0XHQuY2F0Y2goZXJyID0+IHtcclxuXHRcdFx0XHR0aGlzLnVzZXJJZGVudGl0eSA9IG51bGw7XHJcblx0XHRcdFx0dGhpcy5hdXRoZW50aWNhdGVkID0gZmFsc2U7XHJcblx0XHRcdFx0dGhpcy5hdXRoZW50aWNhdGlvblN0YXRlLm5leHQodGhpcy51c2VySWRlbnRpdHkpO1xyXG5cdFx0XHRcdHJldHVybiBudWxsO1xyXG5cdFx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGlzQXV0aGVudGljYXRlZCgpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiB0aGlzLmF1dGhlbnRpY2F0ZWQ7XHJcblx0fVxyXG5cclxuXHRpc0lkZW50aXR5UmVzb2x2ZWQoKTogYm9vbGVhbiB7XHJcblx0XHRyZXR1cm4gdGhpcy51c2VySWRlbnRpdHkgIT09IHVuZGVmaW5lZDtcclxuXHR9XHJcblxyXG5cdGdldEF1dGhlbnRpY2F0aW9uU3RhdGUoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHJldHVybiB0aGlzLmF1dGhlbnRpY2F0aW9uU3RhdGUuYXNPYnNlcnZhYmxlKCk7XHJcblx0fVxyXG5cclxuXHRnZXRJbWFnZVVybCgpOiBzdHJpbmcge1xyXG5cdFx0cmV0dXJuIHRoaXMuaXNJZGVudGl0eVJlc29sdmVkKCkgPyB0aGlzLnVzZXJJZGVudGl0eS5pbWFnZVVybCA6IG51bGw7XHJcblx0fVxyXG59XHJcbiJdfQ==