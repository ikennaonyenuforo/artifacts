import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AccountService } from './account.service';
import { StateStorageService } from './state-storage.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./account.service";
import * as i3 from "./state-storage.service";
var UserRouteAccessService = /** @class */ (function () {
    function UserRouteAccessService(router, accountService, stateStorageService) {
        this.router = router;
        this.accountService = accountService;
        this.stateStorageService = stateStorageService;
    }
    UserRouteAccessService.prototype.canActivate = function (route, state) {
        var authorities = route.data['authorities'];
        // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.
        return this.checkLogin(authorities, state.url);
    };
    UserRouteAccessService.prototype.checkLogin = function (authorities, url) {
        var _this = this;
        return this.accountService.identity().then(function (account) {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account) {
                var hasAnyAuthority = _this.accountService.hasAnyAuthority(authorities);
                if (hasAnyAuthority) {
                    return true;
                }
                if (account.login !== 'anonymoususer') {
                    _this.router.navigate(['sessions/accessdenied']);
                }
                return false;
            }
            _this.stateStorageService.storeUrl(url);
            _this.router.navigate(['sessions/login']);
            return false;
        });
    };
    UserRouteAccessService.ctorParameters = function () { return [
        { type: Router },
        { type: AccountService },
        { type: StateStorageService }
    ]; };
    UserRouteAccessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UserRouteAccessService_Factory() { return new UserRouteAccessService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.AccountService), i0.ɵɵinject(i3.StateStorageService)); }, token: UserRouteAccessService, providedIn: "root" });
    UserRouteAccessService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__metadata("design:paramtypes", [Router,
            AccountService,
            StateStorageService])
    ], UserRouteAccessService);
    return UserRouteAccessService;
}());
export { UserRouteAccessService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1yb3V0ZS1hY2Nlc3Mtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvYXV0aC91c2VyLXJvdXRlLWFjY2Vzcy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFbkcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDOzs7OztBQUc5RDtJQUNJLGdDQUNZLE1BQWMsRUFDZCxjQUE4QixFQUM5QixtQkFBd0M7UUFGeEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQ2pELENBQUM7SUFFSiw0Q0FBVyxHQUFYLFVBQVksS0FBNkIsRUFBRSxLQUEwQjtRQUNqRSxJQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLDZGQUE2RjtRQUM3RixnRkFBZ0Y7UUFDaEYsdUNBQXVDO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCwyQ0FBVSxHQUFWLFVBQVcsV0FBcUIsRUFBRSxHQUFXO1FBQTdDLGlCQXFCQztRQXBCRyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUM5QyxJQUFJLENBQUMsV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUMxQyxPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsSUFBTSxlQUFlLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3pFLElBQUksZUFBZSxFQUFFO29CQUNqQixPQUFPLElBQUksQ0FBQztpQkFDZjtnQkFDRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssZUFBZSxFQUFFO29CQUNuQyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztpQkFDbkQ7Z0JBQ0QsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFFRCxLQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBbENtQixNQUFNO2dCQUNFLGNBQWM7Z0JBQ1QsbUJBQW1COzs7SUFKM0Msc0JBQXNCO1FBRGxDLFVBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQztpREFHWCxNQUFNO1lBQ0UsY0FBYztZQUNULG1CQUFtQjtPQUozQyxzQkFBc0IsQ0FxQ2xDO2lDQTVDRDtDQTRDQyxBQXJDRCxJQXFDQztTQXJDWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIENhbkFjdGl2YXRlLCBSb3V0ZXIsIFJvdXRlclN0YXRlU25hcHNob3QgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgQWNjb3VudFNlcnZpY2UgfSBmcm9tICcuL2FjY291bnQuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0YXRlU3RvcmFnZVNlcnZpY2UgfSBmcm9tICcuL3N0YXRlLXN0b3JhZ2Uuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46ICdyb290JyB9KVxyXG5leHBvcnQgY2xhc3MgVXNlclJvdXRlQWNjZXNzU2VydmljZSBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBzdGF0ZVN0b3JhZ2VTZXJ2aWNlOiBTdGF0ZVN0b3JhZ2VTZXJ2aWNlXHJcbiAgICApIHt9XHJcblxyXG4gICAgY2FuQWN0aXZhdGUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGNvbnN0IGF1dGhvcml0aWVzID0gcm91dGUuZGF0YVsnYXV0aG9yaXRpZXMnXTtcclxuICAgICAgICAvLyBXZSBuZWVkIHRvIGNhbGwgdGhlIGNoZWNrTG9naW4gLyBhbmQgc28gdGhlIGFjY291bnRTZXJ2aWNlLmlkZW50aXR5KCkgZnVuY3Rpb24sIHRvIGVuc3VyZSxcclxuICAgICAgICAvLyB0aGF0IHRoZSBjbGllbnQgaGFzIGEgcHJpbmNpcGFsIHRvbywgaWYgdGhleSBhbHJlYWR5IGxvZ2dlZCBpbiBieSB0aGUgc2VydmVyLlxyXG4gICAgICAgIC8vIFRoaXMgY291bGQgaGFwcGVuIG9uIGEgcGFnZSByZWZyZXNoLlxyXG4gICAgICAgIHJldHVybiB0aGlzLmNoZWNrTG9naW4oYXV0aG9yaXRpZXMsIHN0YXRlLnVybCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tMb2dpbihhdXRob3JpdGllczogc3RyaW5nW10sIHVybDogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWNjb3VudFNlcnZpY2UuaWRlbnRpdHkoKS50aGVuKGFjY291bnQgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIWF1dGhvcml0aWVzIHx8IGF1dGhvcml0aWVzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChhY2NvdW50KSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoYXNBbnlBdXRob3JpdHkgPSB0aGlzLmFjY291bnRTZXJ2aWNlLmhhc0FueUF1dGhvcml0eShhdXRob3JpdGllcyk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaGFzQW55QXV0aG9yaXR5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoYWNjb3VudC5sb2dpbiAhPT0gJ2Fub255bW91c3VzZXInKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydzZXNzaW9ucy9hY2Nlc3NkZW5pZWQnXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGVTdG9yYWdlU2VydmljZS5zdG9yZVVybCh1cmwpO1xyXG4gICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ3Nlc3Npb25zL2xvZ2luJ10pO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19