import * as tslib_1 from "tslib";
import { LOCALE_ID, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
var CoreModule = /** @class */ (function () {
    function CoreModule() {
        //registerLocaleData(locale);
    }
    CoreModule = tslib_1.__decorate([
        NgModule({
            imports: [HttpClientModule],
            exports: [],
            declarations: [],
            providers: [
                Title,
                {
                    provide: LOCALE_ID,
                    useValue: 'en'
                },
                DatePipe
            ]
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], CoreModule);
    return CoreModule;
}());
export { CoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJjb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBZWxEO0lBQ0k7UUFDSSw2QkFBNkI7SUFDakMsQ0FBQztJQUhRLFVBQVU7UUFidEIsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7WUFDM0IsT0FBTyxFQUFFLEVBQUU7WUFDWCxZQUFZLEVBQUUsRUFBRTtZQUNoQixTQUFTLEVBQUU7Z0JBQ1AsS0FBSztnQkFDTDtvQkFDSSxPQUFPLEVBQUUsU0FBUztvQkFDbEIsUUFBUSxFQUFFLElBQUk7aUJBQ2pCO2dCQUNELFFBQVE7YUFDWDtTQUNKLENBQUM7O09BQ1csVUFBVSxDQUl0QjtJQUFELGlCQUFDO0NBQUEsQUFKRCxJQUlDO1NBSlksVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IExPQ0FMRV9JRCwgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBUaXRsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtIdHRwQ2xpZW50TW9kdWxlXSxcclxuICAgIGV4cG9ydHM6IFtdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIFRpdGxlLFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcHJvdmlkZTogTE9DQUxFX0lELFxyXG4gICAgICAgICAgICB1c2VWYWx1ZTogJ2VuJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgRGF0ZVBpcGVcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVNb2R1bGUge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgLy9yZWdpc3RlckxvY2FsZURhdGEobG9jYWxlKTtcclxuICAgIH1cclxufVxyXG4iXX0=