var Account = /** @class */ (function () {
    function Account(activated, authorities, email, firstName, langKey, lastName, login, imageUrl) {
        this.activated = activated;
        this.authorities = authorities;
        this.email = email;
        this.firstName = firstName;
        this.langKey = langKey;
        this.lastName = lastName;
        this.login = login;
        this.imageUrl = imageUrl;
    }
    return Account;
}());
export { Account };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvdXNlci9hY2NvdW50Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQ1csU0FBa0IsRUFDbEIsV0FBcUIsRUFDckIsS0FBYSxFQUNiLFNBQWlCLEVBQ2pCLE9BQWUsRUFDZixRQUFnQixFQUNoQixLQUFhLEVBQ2IsUUFBZ0I7UUFQaEIsY0FBUyxHQUFULFNBQVMsQ0FBUztRQUNsQixnQkFBVyxHQUFYLFdBQVcsQ0FBVTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2IsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUNqQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2IsYUFBUSxHQUFSLFFBQVEsQ0FBUTtJQUN4QixDQUFDO0lBQ1IsY0FBQztBQUFELENBQUMsQUFYRCxJQVdDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIEFjY291bnQge1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGFjdGl2YXRlZDogYm9vbGVhbixcclxuICAgICAgICBwdWJsaWMgYXV0aG9yaXRpZXM6IHN0cmluZ1tdLFxyXG4gICAgICAgIHB1YmxpYyBlbWFpbDogc3RyaW5nLFxyXG4gICAgICAgIHB1YmxpYyBmaXJzdE5hbWU6IHN0cmluZyxcclxuICAgICAgICBwdWJsaWMgbGFuZ0tleTogc3RyaW5nLFxyXG4gICAgICAgIHB1YmxpYyBsYXN0TmFtZTogc3RyaW5nLFxyXG4gICAgICAgIHB1YmxpYyBsb2dpbjogc3RyaW5nLFxyXG4gICAgICAgIHB1YmxpYyBpbWFnZVVybDogc3RyaW5nXHJcbiAgICApIHt9XHJcbn1cclxuIl19